import sys
import os
import numpy as np
import mahotas as mh
import tifffile as tiff
from imgmisc import binary_extract_largest
from imgmisc import get_resolution
from imgmisc import to_uint8
from imgmisc import listdir
from imgmisc import mkdir
from imgmisc import fill_beneath
from scipy.ndimage import zoom
from scipy.ndimage import gaussian_filter
from skimage.exposure import equalize_adapthist
from skimage.segmentation import morphological_chan_vese
from phenotastic.mesh import fill_contour
from multiprocessing import Pool
import czifile as cz
import argparse
# import pyvista as pv

# fname = files[5]
# fin = fname

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--iterations', type=int, default=25)
parser.add_argument('--smoothing', type=int, default=1)
parser.add_argument('--masking', type=float, default=1)
parser.add_argument('--clahe_window', type=int, nargs=3, default=None)
parser.add_argument('--clahe_clip_limit', type=float, default=None)
parser.add_argument('--gaussian_sigma', type=int, nargs=3, default=[1,1,1])
parser.add_argument('--gaussian_iterations', type=int, default=5)
parser.add_argument('--target_resolution', type=float, nargs=3, default=[.5, .5, .5])
parser.add_argument('--lambda1', type=float, default=1)
parser.add_argument('--lambda2', type=float, default=1)
parser.add_argument('--clip_threshold', type=float, default=0.01)
parser.add_argument('--fill_slices', action='store_true')
parser.add_argument('--segmented', action='store_true')
parser.add_argument('--naive', action='store_true')
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--suffix', type=str, default='-stackreg')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    files = args.input
mkdir(args.output)
# print(files)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
print(files)
def run(fname):
    if args.verbose:
        print(f'Now running {fname}')
    try:
        data = tiff.imread(fname)
    except:
        try:
            data = cz.czifile(fname)
        except:
            raise Exception(f'Input file type for {fname} not supported.')
    data = np.squeeze(data)
    if data.ndim < 3:
        raise Exception('Too few channels')
    if data.ndim == 4 and args.channel is not None:
        data = data[:, args.channel]
    if args.resolution is None:
        resolution = np.asarray(get_resolution(fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution
    
    if os.path.isfile(f'{args.output}/{bname(fname)}_contour.tif'):
        print(f'Skipping {bname(fname)}...')
        return

    
    data = zoom(data, resolution / np.array(args.target_resolution), order=3 if
            not args.segmented else 0)
    if args.segmented:
        contour = data > 0
        contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
        contour = fill_beneath(contour)
        contour = binary_extract_largest(contour)
        contour = contour.astype('bool')
        
        tiff.imwrite(f'{args.output}/{bname(fname)}_contour.tif',
                    contour.astype(np.uint8), imagej=True, metadata={'spacing':
                        args.target_resolution[0], 'unit': 'um'},
                    resolution=(1./args.target_resolution[1],
                        1./args.target_resolution[1]))
        return


    if args.clahe_window is None:
        clahe_window = (np.array(data.shape) + 4) // 8
    data = equalize_adapthist(data, clahe_window, args.clip_threshold)

    for ii in range(args.gaussian_iterations):
        data = gaussian_filter(data, sigma=args.gaussian_sigma)
    data = to_uint8(data, False)
    mask = to_uint8(data, False) > args.masking * mh.otsu(to_uint8(data, False))

    if args.naive:
        if args.verbose:
            print(f'Running naive contouring for {fname}')
        contour = mask
    else:
        if args.verbose:
            print(f'Running morphological chan-vese for {fname}')
        contour = morphological_chan_vese(data, iterations=args.iterations,
                                          init_level_set=mask,
                                          smoothing=args.smoothing, lambda1=args.lambda1, lambda2=args.lambda2)
    contour = fill_contour(contour, fill_xy=True, fill_zx_zy=False)
    contour = fill_beneath(contour)
    contour = binary_extract_largest(contour)
    contour = contour.astype('bool')
    
    tiff.imwrite(f'{args.output}/{bname(fname)}_contour.tif',
                contour.astype(np.uint8), imagej=True, metadata={'spacing': args.target_resolution[0], 'unit': 'um'}, resolution=(1./args.target_resolution[1], 1./args.target_resolution[1]))
    
    return

p = Pool(args.parallel_workers)
p.map(run, files)
p.close()
p.join()

## Henrik's scripts
This is my collection of various convenience scripts. Many of them are fairly ad hoc, but are used in several different projects, so I am collecting them here for simulataneous maintenance. 

Do feel free to raise and Issue or Pull Request if anything is unclear or you make any modifications.

## Naming convention and script usage

In all scripts, I advise you (and sometimes you may well be required to) use the following file name format:

`<YYMMDD/date>-<reporters>-<genotype>-plant_<plant_id>-<time>h.<lsm/lif/tif/tiff>`

e.g.

```bash
220311-PIN1GFP_pUBQ10dsRED-csi1-plant_3-48h.tif or
230601-PI-WT-plant_15-0h.lsm
```
You can read detailed instructions on how my scripts work by running

```bash
python autocrop.py --help

usage: autocrop.py [-h] [--output OUTPUT] [--background BACKGROUND] [--offset [OFFSET ...]]

               	[--factor FACTOR] [--n N] [--channel CHANNEL]

               	[--resolution RESOLUTION RESOLUTION RESOLUTION] [--suffix SUFFIX] [--binary]

               	[--verbose] [--skip_done] [--split_channels] [--wiener] [--stackreg]

               	[--parallel_workers PARALLEL_WORKERS]

               	input [input ...]

This script allows the user to automatically crop 3D image stacks by setting a threshold intensity value/count that needs to be surpassed. The algorithm then crops the image to a bounding box defined by that constraint (optionally modified by an offset).

positional arguments:

  input             	Input files or directory. Required argument.

optional arguments:

  -h, --help        	show this help message and exit

  --output OUTPUT   	Output directory. Defaults to the current directory.
[...]
```

## **Cell segmentation**
1. **Crop the input and separate the channels**. Cropping can be particularly useful for avoiding memory issues for larger files.

`python autocrop.py --n 100 --split_channels --output cropped *.tif`



2. **Segment** the cells using PlantSeg.

`python run_pseg.py cropped/*C1*.tif --output segmented --mirror_padding 16 32 32 --patch 32 64 64 --stride 16 32 32 --seg_prob_thres .3 --seg_minsize 200 --num_workers 3 --seg_tif --verbose --skip_done --parallel_workers 1 --pseg_bin /home/henrikahl/.local/anaconda3/envs/plant-seg/bin/plantseg`



3. **Refine** the segmentation. This removes spurious cells and re-labels the background (defined as the biggest object) to have label 0. \

`python refine.py segmented --output segmented_refined`

4. **Plot** for visualisation.

`python plot-segmentations.py segmented_refined --output figures/segmentations`

<<< [alt_text](images/image1.png "image_tooltip") >>>
<<< [alt_text](images/image1.png "image_tooltip") >>>

## **Cell geometry quantification**
1. Generate *cell segmentation*.
2. Generate the cell geometry data:

`python generate_cell_data.py segmented_refined/*.tif --output cell_data`

## Registration – mashing together many meristems
1. Generate *cell segmentation*.
2. Generate the *cell geometry data*.
3. Generate registrations:
`python generate_registrations.py segmented_refined/*.tif --output registrations`

4. Generate the registered cell data DataFrame:

`python generate_registered_cell_data.py  --seg_input segmented_refined --data_input cell_data --reg_input registrations --output registered_cell_data.csv`

## Single cell tracking
    NB: Ensure that all files follow the naming convention in A.

1. Generate *cell segmentation*.
2. Generate tracking data:

`python tracking.py --seg_input segmented_refined --sig_input cropped/*C1*.tif --output tracking`

4. Generate cell age data:

`python generate_cell_age_data.py --seg_input segmented_refined --tracking_input tracking --output cell_age_data.csv`

## Fluorescence quantification per cell
1. Generate *cell segmentation*.
2. Extract the cell signal data:

`python ~/projects/generate_cell_signals.py --seg_input segmented_refined --cha_input cropped --output signal_data`

## Contour Generation
Two recommended alternatives:

1. Generate a contour using naive (Otsu) thresholding (fast):

`python ~/projects/contour.py --naive cropped/*C1*.lsm --output contours --masking 0.2`

2. Generate a contour using the Morphosnakes algorithm (slow):

`python contour.py cropped/*C1*.lsm --target_resolution 0.5 0.5 0.5 --output contours --masking 0.5 --iterations 20 --fill_slices`

## Mesh generation
1. Perform *contour generation*.
2. Generate surfaces:
`python surface.py contours --output meshes --smooth_iter 200 --downscaling 0.035`

## Flower organ segmentation

`python flower_segmentation.py --segmented --minmax_iterations 10 --smooth_iter 200 --downscaling 0.01 --upscaling 2 --mean_iterations 20 --bottom_crop 5 --output flower_segmentation`

## Deconvolution
`python deconvolve.py cropped/*C1*.tif 514 575 --iters 12 --normalise --pinhole_diameter 33.074 --output deconvolved`

## Cellygonisation
1. Generate segmentation: **B**.
2. Cellygonize!

`python cellygonize.py segmented_refined --output cellygon_meshes`

## Phyllotaxis quantification
1. Generate contour or segmentation: **B or G**
2. Run the manual interface:

`python manual_calculate_phyllotaxis.py segmented_refined/*.tif --segmented --output phyllotaxis_data --font_size 30`

3. Hover with the mouse over the center of the meristem, press “**p**” to make a selection. This will define the apex coordinate from which the angles will be calculated (in the XY plane). Any errors, press “**u**” to undo the last point.
4. Go around the meristem and similarly press “**p**” for each of the organs, starting out from the smallest.
5. Press “**c**” if the phyllotaxis order is clockwise arranged, or “**v**” if it is counter-clockwise ordered.
6. Press “**q**” to finish and go to the next plant.
7. The result should look something like this:

<<< [alt_text](images/image1.png "image_tooltip") >>>

8. Depending on the quality of the data, it may well be a good idea to not select the center point of the flowe organs, but rather the boundary between organ and inflorescence meristem.
9. For generating some basic plots, run:

`python ~/projects/plot_phyllotaxis_statistics.py phyllotaxis_data --output figures`

16. **PIN membrane quantification**
17. *Segment* the cells
18. (May need to shift PIN)
19. Run:
 `python quantify.py --seg_input segmented_refined/*.tif --pin_input cropped/*C2*.tif --wall_depth 1 --output pin_quantification`

## Simulate auxin patterns
1. Segment cells and *quantify PIN1*.
2. Simulate (and plot):
`python simulate.py --init <init_file> --neigh <neigh_file> --end_state_dir end_state --smooth_iterations 1 --figure_dir figures/auxin_simulations`

## Visualisation
1. **Max/Sum projections / side cuts**
`python project.py cropped/*.tif --sides 1 1 1 --fct sum --threshold 0.95 --output figures/projections`

<<< [alt_text](images/image1.png "image_tooltip") >>>

2. **Segmentations**
`python plot-segmentations segmented_refined/*.tif --output figures/segmentations`

<<< [alt_text](images/image1.png "image_tooltip") >>>

## Organ geometry quantification
## CZ 3D curvature calculation
## Nuclear segmentation using PyCostanza


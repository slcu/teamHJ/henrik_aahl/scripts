#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import re
import os
import ants
import numpy as np
import pandas as pd
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import get_resolution
from scipy.ndimage import rotate
from skimage.measure import regionprops

import argparse
from imgmisc import parse_input_files

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('--seg_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--data_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--reg_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output name', default=f'{os.path.curdir}/registered_cell_data.csv')
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-cropped')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)
args = parser.parse_args()

seg_files = parse_input_files(args.seg_input, include=['.tif', '.tiff'])
data_files = parse_input_files(args.data_input, include=['.csv'])
trans_files = parse_input_files(args.reg_input, include=['.mat'])
mkdir(os.path.abspath(os.path.dirname(args.output)))

assert(len(seg_files) == len(data_files))

def collate_registered_cell_data(seg_files, cell_data_files, trans_files):
    
    # Find meristem that's best fitted by the other meristems
    fixed = [re.findall('f_(\d+)', ff)[0] for ff in trans_files]
    moving = [re.findall('m_(\d+)', ff)[0] for ff in trans_files]
    flips = [re.findall('flip_(\D+)-', ff)[0] for ff in trans_files]
    rots = [float(re.findall('rot_(\d+\.\d+)', ff)[0]) for ff in trans_files]
    mi = [float(re.findall('mi_(\d+\.\d+)', ff)[0]) for ff in trans_files]
    
    data = pd.DataFrame(
        data={'fixed': fixed, 'moving': moving, 'flips': flips, 'rots': rots, 'mi': mi})
    best_fixed = data.groupby(['fixed', 'moving']).max().groupby(
        'fixed').sum()['mi'].idxmax()
    best_matches = data.iloc[data.loc[data['fixed'] == best_fixed].groupby('moving')[
        'mi'].idxmax()]
    best_idx = best_matches.index

    # Read fixed file
    ffile = [sf for sf in seg_files if best_fixed + '-' in sf][0]
    fres = get_resolution(ffile)
    fdata = tiff.imread(ffile).astype(float)
    fdata = ants.from_numpy(fdata, spacing=fres)

    regp = pd.read_csv(
        [ff for ff in cell_data_files if best_fixed + '-' in ff][0], sep='\t')
    dfs = [regp]
    for ff, idx in enumerate(best_idx):
        tf = ants.read_transform(trans_files[idx])
        mfile = [sf for sf in seg_files if best_matches['moving']
                 [idx] + '-' in sf][0]
        mres = get_resolution(mfile)

        # Read in the moving image and apply the corresponding 'best' transform
        mdata = tiff.imread(mfile)
        mdata = mdata[:, ::-1,
                      :] if data.iloc[idx]['flips'] == 'flip' else mdata
        mdata = rotate(mdata, data.iloc[idx]['rots'], axes=(
            1, 2), reshape=False, order=0).astype(float)
        mdata = ants.from_numpy(mdata, spacing=mres)
        mdata = tf.apply_to_image(mdata, interpolation='nearestneighbor')
        mdata = mdata.numpy()
        mdata = mdata.astype(np.uint16)

        # Compute the new centroids for the transformed data
        regp = regionprops(mdata)
        post_centroids = np.array([list(rp.centroid) for rp in regp]) * mres
        tf_labels = [rr.label for rr in regp]

        # Read in the cell geometry data and assign the new centroids to each label
        mfile = [sf for sf in cell_data_files if best_matches['moving']
                 [idx] + '-' in sf][0]
        df = pd.read_csv(mfile, sep='\t')
        df = df.loc[df['label'].isin(tf_labels)]
        for ii, lab in enumerate(tf_labels):
            df.loc[df['label'] == lab, ['z', 'y', 'x']] = post_centroids[ii]
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    return df

df = collate_registered_cell_data(seg_files, data_files, trans_files)
df.to_csv(f'{args.output}', index=False, sep='\t')

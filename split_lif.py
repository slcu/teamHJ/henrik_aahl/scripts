#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 11:09:54 2023

@author: henrikahl
"""

import os
import argparse
import numpy as np
import tifffile as tiff
import mahotas as mh
import czifile as cz
from imgmisc import cut
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import autocrop
from imgmisc import get_resolution
from pystackreg import StackReg
from readlif.reader import LifFile

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--offset', type=int, default=[10, 5, 5], nargs='*')
parser.add_argument('--factor', type=float, default=.9)
parser.add_argument('--n', type=int, default=10)
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='')
parser.add_argument('--binary', action='store_true')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--split_channels', action='store_true')
parser.add_argument('--wiener', action='store_true')
parser.add_argument('--stackreg', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi', '.lif'])
else:
    files = args.input
mkdir(args.output)

for fname in files:
    new = LifFile(fname)
    img_list = [i for i in new.get_iter_image()]
    for iter_, img in enumerate(img_list):
        
        cz_stack = np.array([[np.array(img.get_frame(z=zz, t=0, c=c_iter, m=0)) for zz in range(img.nz)] for c_iter in range(img.channels)])
        cz_stack = cz_stack.reshape((-1,) + tuple(cz_stack.shape[2:]))
        cz_stack = cz_stack.reshape((4, -1,) + tuple(cz_stack.shape[-2:]), order='F')
        data = np.swapaxes(cz_stack , 0, 1)
        resolution = np.array([1 / img.scale[2], 1/ img.scale[1], 1/img.scale[0]])
        
        if args.split_channels:
            if data.ndim < 3:
                raise Exception(f'Trying to split channels, but input is of shape {data.shape}.' + 
                                ' Channel splitting requires input in format ZCYX.')
            elif data.ndim == 3: 
                data = np.expand_dims(data, 1)
            for cc in range(data.shape[1]):
                iname = os.path.basename(os.path.splitext(fname)[0]) + f'-img{iter_}' + f'-C{cc+1}' + f'{args.suffix}.tif'
                iname = f'{args.output}/{iname}'
                tiff.imwrite(iname, data=np.expand_dims(data[:, cc], 1), resolution=1/resolution[1:], imagej=True, 
                    metadata={'spacing':resolution[0], 'unit':'um'})
        else:
            iname = os.path.basename(os.path.splitext(fname)[0]) + f'-img{iter_}' +  f'{args.suffix}.tif'
            iname = f'{args.output}/{iname}'
            tiff.imwrite(iname, data=np.expand_dims(data, 1) if data.ndim == 3 else data, resolution=1/resolution[1:], imagej=True, 
                        metadata={'spacing':resolution[0], 'unit':'um'})
        del data
    


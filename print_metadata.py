#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 14:13:51 2020

@author: henrikahl
"""
import os
import numpy as np
import argparse
import czifile as cz
import tifffile as tiff
from imgmisc import listdir
from imgmisc import mkdir
from imgmisc import get_resolution
from multiprocessing.pool import ThreadPool

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--parallel_workers', type=int, default=1)

# Optional argument
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--resolution', action='store_true')
args = parser.parse_args()
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi', '.lif'])
else:
    files = args.input
mkdir(args.output)
# print(files)

def run(fname):
    
    file_ = tiff.TiffFile(fname)
    if '.tif' in fname:
        basename = bname(fname)
        resolution = get_resolution(fname)
        print(f'{basename} - Resolution: {round(resolution[0], 4)} ' + 
                                       f'{round(resolution[1], 4)} ' + 
                                       f'{round(resolution[2], 4)}\t')



    if '.lsm' in fname:
        meta = file_.lsm_metadata
        
        print(f'--------Metadata file {bname(fname)}--------')
        print(f'Resolution: {round(meta["VoxelSizeZ"]*1e6, 4)} {round(meta["VoxelSizeY"]*1e6,4)} {round(meta["VoxelSizeX"]*1e6,4)}\t')
        if args.resolution:
            return
        
        for ii, _ in enumerate(meta['ChannelWavelength']):
            print(f'\tChannel {ii+1}: {round(meta["ChannelWavelength"][ii][0] * 1e9)}-{round(meta["ChannelWavelength"][ii][1] * 1e9)}; mean: {round(np.mean(meta["ChannelWavelength"][ii]) * 1e9)}')

        lasers = []
        for ii, track in enumerate(meta['ScanInformation']['Tracks']):
            for jj in track['IlluminationChannels']:
                lasers.append(jj['Wavelength'])
        lasers = np.sort(np.unique(lasers))
        print('Lasers: ' + " ".join([str(round(ll)) for ll in lasers]))

        
        pinholes = []
        for ii, track in enumerate(meta['ScanInformation']['Tracks']):
            for jj in track['DetectionChannels']:
                pinholes.append(jj['PinholeDiameter'])
        pinholes = np.sort(pinholes)
        print('Pinhole diameters: ' + " ".join([str(round(ll, 4)) for ll in pinholes]))
    # pinholes = np.sort(np.unique(pinholes))
            
    # try:
    #     data = tiff.imread(fname)
    # except:
    #     try:
    #         data = cz.czifile(fname)
    #     except:
    #         try:
    #             import read_lif as lif
    #             reader = lif.Reader(fname)
    #             series = reader.getSeries()
    #             chosen = series[0]
    #             data = []
    #             for nchan in range(len(chosen.getChannels())):
    #                 image = chosen.getFrame(T=0, channel=nchan)
    #                 data.append(image)
    #             data = np.stack(data, axis=1)
    #         except:
    #             raise Exception(f'Input file type for {fname} not supported.')
    # data = np.squeeze(data)

    # if data.ndim < 3:
    #     raise Exception('Too few channels')
    # if data.ndim == 4 and args.channel is not None:
    #     data = data[:, args.channel]
    # if args.resolution is None:
    #     resolution = get_resolution(fname)
    # #data = np.expand_dims(data, 0)
    # print(data.shape)
    
    # # Get dimensions
    # pinhole_radius = args.pinhole_diameter / 2 / args.magnification
    # zshape = data.shape[0] // 2.
    # rshape =  np.min(data.shape[1:]) // 2.
    # zdims = zshape * resolution[0]
    # rdims = rshape * resolution[1]
    
    # kernel = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=args.ex,
    #                     em_wavelen=args.em, pinhole_radius=pinhole_radius,
    #                     num_aperture=args.na, magnification=args.magnification, 
    #                     pinhole_shape=args.pinhole_shape, refr_index=args.refr_index)
    # algo = fd_restoration.RichardsonLucyDeconvolver(data.ndim, pad_min=np.ones(data.ndim)).initialize()
    
    # if args.verbose:
    #     print(f'Running RL with {args.iters} iterations for {fname}')
    # result = algo.run(fd_data.Acquisition(data=data, kernel=kernel), niter=args.iters)
    
    # result = result.data    
    # result[result > np.iinfo(data.dtype).max] = np.iinfo(data.dtype).max
    # result[result < 0] = 0
    # result = result.astype(data.dtype)
    
    # basename = os.path.splitext(os.path.basename(fname))[0]
    # fname_out = f'{args.output}/{basename}-deconvolved_{args.iters}.tif'
    # tiff.imsave(fname_out, np.expand_dims(result, axis=1), imagej=True, metadata={'spacing':resolution[0], 'unit':'um'}, 
    #             resolution=(1./resolution[1], 1./resolution[2]))
    
# Run deconvolution
p = ThreadPool(args.parallel_workers)
p.map(run, files)
p.close()           
p.join()

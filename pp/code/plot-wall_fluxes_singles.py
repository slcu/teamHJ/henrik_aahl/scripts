#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 20:37:58 2020

@author: henrik
"""
import os
import copy
import numpy as np
import pandas as pd
import tifffile as tiff
from skimage.morphology import ball
from skimage.measure import regionprops
from skimage.segmentation import relabel_sequential
from imgmisc import listdir, to_uint8, autocrop, cut, get_resolution, mkdir
from imgmisc import binary_extract_largest
from img2org.quantify import find_neighbors
from img2org.quantify import get_l1
from img2org.quantify import get_wall
from img2org.quantify import area_between_walls
from img2org.quantify import set_background
from img2org.conversion import create_organism_files
import sys
from multiprocessing import Pool, cpu_count
import pickle
import pandas as pd
import re
from img2org.conversion import read_neigh
from img2org.quantify import get_layers
from imgmisc import get_resolution
from skimage.measure import marching_cubes_lewiner
import pyvista as pv
from phenotastic.mesh import create_cellular_mesh
from phenotastic.mesh import label_cellular_mesh
import gc
import matplotlib 
import matplotlib.pyplot as plt

matplotlib.use('Agg')
# matplotlib.use('Agg')

# from xvfbwrapper import Xvfb
# vdisplay = Xvfb(width=1920, height=1080)
# vdisplay.start()

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pin_patterns')
SEG_DIR = f'{PROJECT_DIR}/data/final/segmentation'
DATA_DIR = f'{PROJECT_DIR}/organism_simulations/output/parameter_sweep_active-131007-WT'
INIT_DIR = f'{PROJECT_DIR}/organism_simulations/init/131007-PIN_GFP-acyl_YFP-WT'
OUT_DIR = f'{PROJECT_DIR}/figures/parameter_sweep_active-131007-WT/flux_directions'
mkdir(OUT_DIR)

seg_files = listdir(SEG_DIR, sorting='natural', include='.tif', recursive=True)
init_files = listdir(INIT_DIR, sorting='natural', include='.init', recursive=False)
data_files = listdir(DATA_DIR, sorting='natural', include='.neigh', recursive=False)

import re
from imgmisc import natural_sort
from img2org.conversion import read_init
plants = np.unique([re.findall('plant_\d+', ff)[0] for ff in data_files])
plants = natural_sort(plants)

plant = plants[0]
for plant in plants:
    print(plant)
    seg_file = [ss for ss in seg_files if plant + '-' in ss][0]
    init_file = [ss for ss in init_files if plant + '-' in ss][0]
    seg_img = tiff.imread(seg_file)
    resolution = get_resolution(seg_file)

    init = read_init(init_file)
    l1, l2 = get_layers(seg_img, bg=0, depth=2)
    l1 = l1 - 1
    l2 = l2 - 1
    centers = init.iloc[l1, :3]

    plant_data_files = [data_file for data_file in data_files if plant + '-' in data_file]
    #plant_data_file = plant_data_files[0]
    for plant_data_file in plant_data_files:
        print('', plant_data_file)
        data = read_neigh(plant_data_file)
        flux_index = 2
        del data['n_cells'], data['n_params']
        all_fluxes = [np.vstack(([ii] * data[ii]['n_neighs'], data[ii]['neighs'], data[ii][f'p{flux_index}'])) for ii in data.keys()]
        all_fluxes = np.hstack(all_fluxes).T

        all_fluxes = pd.DataFrame(all_fluxes)
        all_fluxes_to_l1_from_l1 = all_fluxes.loc[np.logical_and(all_fluxes[1].isin(l1), 
                                                         all_fluxes[0].isin(l1))] 
        mesh = pv.PolyData(centers.values)
        geom = pv.Arrow()  
    
        vectors = []
        scalars = []
        for idx in np.unique(all_fluxes_to_l1_from_l1[0].values):
            # print(idx)
            for neigh_idx in all_fluxes_to_l1_from_l1.loc[all_fluxes_to_l1_from_l1[0] == idx][1]:
                if neigh_idx < idx:
                    continue
                vec = centers.loc[int(idx)].values, centers.loc[int(neigh_idx)].values
                vectors.append(vec)
                scalars.append((all_fluxes_to_l1_from_l1.loc[
                    all_fluxes_to_l1_from_l1[0] == idx].loc[all_fluxes_to_l1_from_l1.loc[all_fluxes_to_l1_from_l1[0] == idx][1] == neigh_idx][2]).values)

        x = np.array([(vec[0][0] + vec[1][0]) / 2 for vec in vectors])
        y = np.array([(vec[0][1] + vec[1][1]) / 2 for vec in vectors])
        z = np.array([(vec[0][2] + vec[1][2]) / 2 for vec in vectors])
        
        dx = np.array([(vec[0][0] - vec[1][0]) for vec in vectors])
        dy = np.array([(vec[0][1] - vec[1][1]) for vec in vectors])
        dz = np.array([(vec[0][2] - vec[1][2]) for vec in vectors])
        
        lengths = np.hypot(np.array(dy), np.array(dz))
        flux = np.squeeze(np.array(scalars))

        show = flux.copy()
        dirs = np.sign(show)   # Flux directions
        u = dirs * dx / lengths
        v = dirs * dy / lengths
        w = dirs * dz / lengths
        
        fig = plt.figure()
        fig.set_size_inches(10, 8, forward=True)
        ax = fig.gca()
        q = ax.quiver(y, z, v, w, abs(show), pivot="middle", units="xy",
                      headlength=5, scale=5, scale_units="inches", width=.5,
                      cmap="turbo")
        nvar = 3
        q.set_clim(max([0, np.mean(abs(show)) - nvar * np.std(abs(show))]), np.mean(abs(show)) + nvar * np.std(abs(show)))
        padding = 5
        plt.xlim(min(init[1]) - padding, max(init[1]) + padding)
        plt.ylim(min(init[2]) - padding, max(init[2]) + padding)
        fig.colorbar(q)
        # plt.show()
        plt.axis('off')
        plt.savefig('/home/henrik/quivers/' + os.path.basename(plant_data_file).split("-end_state")[0] + '_quiver.png')

        thres = np.quantile(abs(flux), .95)
        y = y[abs(flux) > thres]
        z = z[abs(flux) > thres]
        v = v[abs(flux) > thres]
        w = w[abs(flux) > thres]
        show = show[abs(flux) > thres]
        
        fig = plt.figure()
        fig.set_size_inches(10, 8, forward=True)
        ax = fig.gca()
        q = ax.quiver(y, z, v, w, abs(show), pivot="middle", units="xy",
                      headlength=5, scale=5, scale_units="inches", width=.5,
                      cmap="turbo")
        
        nvar = 3
        q.set_clim(max([0, np.mean(abs(show)) - nvar * np.std(abs(show))]), np.mean(abs(show)) + nvar * np.std(abs(show)))
        padding = 5
        plt.xlim(min(init[1]) - padding, max(init[1]) + padding)
        plt.ylim(min(init[2]) - padding, max(init[2]) + padding)
        
        fig.colorbar(q)
        # plt.show()
        plt.axis('off')
        plt.savefig('/home/henrik/quivers_partial/' + os.path.basename(plant_data_file).split("-end_state")[0] + f'-quiver-3_mean.png')
        plt.close()
        import gc
        gc.collect()

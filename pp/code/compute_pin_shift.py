#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 10:33:51 2022

@author: henrikahl
"""

import numpy as np
import mahotas as mh
import tifffile as tiff
import numexpr as ne
from skimage.measure import regionprops
from phenotastic.misc import sph2car
from phenotastic.misc import car2sph
from scipy.spatial import cKDTree
import pyvista as pv
import phenotastic.mesh as mp
from imgmisc import get_resolution
from scipy.ndimage import shift
# from img2org.conversion import read_init, read_neigh_init
from phenotastic.mesh import vertex_neighbors_all
from phenotastic.domains import filter
# from tqdm import tqdm
from multiprocessing import Pool
from imgmisc import listdir
import re
import re
import os
import sys
import numpy as np
import pandas as pd
import mahotas as mh
import pyvista as pv
import tifffile as tiff
from imgmisc import merge
from imgmisc import listdir
from imgmisc import flatten
from imgmisc import autocrop
from imgmisc import rand_cmap
from imgmisc import circular_mask
from imgmisc import find_neighbors
from imgmisc import get_resolution    
from imgmisc import get_l1_from_above
from imgmisc import mkdir
from imgmisc import get_l1
from scipy.ndimage import center_of_mass

from img2org.conversion import read_init
from img2org.conversion import read_neigh_init
import argparse

# parser = argparse.ArgumentParser(description='')

# # Required positional argument
# parser.add_argument('input', type=str, nargs='+',
#                     help='Input files or directory')
# parser.add_argument('--output', type=str,
#                     help='Output directory', default=f'{os.path.curdir}')

# parser.add_argument('--normalise', action='store_true')

# # Optional argument
# parser.add_argument('--suffix', type=str, default='-sphere_projections')
# parser.add_argument('--verbose', action='store_true')
# parser.add_argument('--skip_done', action='store_true')
# parser.add_argument('--parallel_workers', type=int, default=1)

# args = parser.parse_args()

# if len(args.init) == 1 and os.path.isdir(os.path.abspath(args.init[0])):
#     init_files = listdir(args.init[0], sorting='natural', include=['.init'])
# else:
#     init_files = args.init
# if len(args.neigh) == 1 and os.path.isdir(os.path.abspath(args.neigh[0])):
#     neigh_files = listdir(args.neigh[0], sorting='natural', include=['.neigh'])
# else:
#     neigh_files = args.neigh

# # TODO: Cover all cases
# if len(init_files) == 1 and len(neigh_files) == 1:
#     files = [[init_files[0], neigh_files[0]]]
# else: 
#     files = list(zip(init_files, neigh_files))
# mkdir(args.output)

# bname = lambda x: os.path.basename(os.path.splitext(x)[0])
# basename = bname(files[0][0])

sph = mp.remesh(pv.Sphere(radius=1), 1000)
all_values = np.load('/home/henrikahl/projects/pp/data/220711-PIN1GFP_MYRYFP-WT-timepoint_0/shift_values_2.npy', allow_pickle=True)
shifts =  [np.array(re.findall(r'shift_([-+]?(?:\d*\.\d+|\d+))_([-+]?(?:\d*\.\d+|\d+))_([-+]?(?:\d*\.\d+|\d+))', fpair[0][0])[0], dtype=float) for fpair in all_values]
neighs = vertex_neighbors_all(sph)
values = np.array([vv[1] for vv in all_values])

# smooth_all_values = np.array([filter(vals, neighs, np.mean, 5) for vals in values])
# xvalues, yvalues, zvalues =[], [], []
# for iter_, vals in enumerate(smooth_all_values):
#     print(f'{iter_ + 1} / {len(all_values)}')
#     sph['values'] = vals
#     sx, sy, sz = sph.slice_orthogonal()    
#     xvalues.append(sx['values'])
#     yvalues.append(sy['values'])
#     zvalues.append(sz['values'])

means = [np.mean(vals) for vals in values]
maxs = [np.max(vals) for vals in values]

# xstds = np.array([np.std(xx) for xx in xvalues])
# ystds = np.array([np.std(xx) for xx in yvalues])
# zstds = np.array([np.std(xx) for xx in zvalues])

init_files = [ff[0][0] for ff in all_values]
plants =  [np.array(re.findall(r'plant_(\d+)-', init_fname), dtype=float)[0] for init_fname in init_files]
plants = np.array(plants)
shifts = np.array(shifts)

for ii in range(10):
    stds = np.array([np.std(filter(vals, neighs, np.mean, ii)) for vals in values])
    
    all_ = []
    for pp in np.unique(plants):
        all_pp = shifts[plants == pp][np.argmin(stds[plants == pp])]
        print(ii, int(pp), all_pp)
        all_.append(all_pp)
    all_ = np.array(all_)
    print(f'Total:\t{ii}\t' + 
          f'{np.std(all_[:,1])}\t' + 
          f'{np.std(all_[:,2])}\t' + 
          f'{np.std(all_[:,2]) / np.mean(all_[:,2]) + np.std(all_[:,1]) / np.mean(all_[:,1])}\t')

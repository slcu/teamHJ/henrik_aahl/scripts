#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import re
import os
import ants
import numpy as np
import pandas as pd
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from scipy.ndimage import rotate
from skimage.measure import regionprops

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
SEG_DIR = f'{PROJECT_DIR}/data/{dataset}'
CELL_DATA_DIR = f'{PROJECT_DIR}/data/{dataset}/cell_data'
TRANS_DIR = f'{PROJECT_DIR}/data/{dataset}/registration'
OUTPUT_DIR = f'{PROJECT_DIR}/data/{dataset}'
mkdir(OUTPUT_DIR)

seg_files = listdir(SEG_DIR, sorting='natural', include='-refined.tif')
data_files = listdir(CELL_DATA_DIR, sorting='natural', include='cell_data.csv')
trans_files = listdir(TRANS_DIR, sorting='natural', include='.mat')

def collate_registered_cell_data(seg_files, cell_data_files, trans_files):
    # Find meristem that's best fitted by the other meristems
    fixed = [re.findall('f_(plant_\d+)', ff)[0] for ff in trans_files]
    moving = [re.findall('m_(plant_\d+)', ff)[0] for ff in trans_files]
    flips = [re.findall('flip_(\D+)-', ff)[0] for ff in trans_files]
    rots = [float(re.findall('rot_(\d+\.\d+)', ff)[0]) for ff in trans_files]
    mi = [float(re.findall('mi_(\d+\.\d+)', ff)[0]) for ff in trans_files]
    data = pd.DataFrame(data={'fixed':fixed, 'moving':moving, 'flips':flips, 'rots':rots, 'mi':mi})
    best_fixed = data.groupby(['fixed', 'moving']).max().groupby('fixed').sum()['mi'].idxmax()
    best_matches = data.iloc[data.loc[data['fixed'] == best_fixed].groupby('moving')['mi'].idxmax()]
    best_idx = best_matches.index
    
    ffile = [sf for sf in seg_files if best_fixed + '-' in sf][0]
    fres = get_resolution(ffile)
    fdata = tiff.imread(ffile).astype(float)
    fdata = ants.from_numpy(fdata, spacing=fres)
    
    regp = pd.read_csv([ff for ff in cell_data_files if best_fixed + '-' in ff][0], sep='\t')
    dfs = [regp]
    for ff, idx in enumerate(best_idx):
        tf = ants.read_transform(trans_files[idx])
        mfile = [sf for sf in seg_files if best_matches['moving'][idx] + '-' in sf][0]
        mres = get_resolution(mfile)
        
        # Read in the moving image and apply the corresponding 'best' transform
        mdata = tiff.imread(mfile) # .astype(float)
        mdata = mdata[:, ::-1, :] if data.iloc[idx]['flips'] == 'flip' else mdata
        mdata = rotate(mdata, data.iloc[idx]['rots'], axes=(1, 2), reshape=False, order=0).astype(float)
        mdata = ants.from_numpy(mdata, spacing=mres)
        mdata = tf.apply_to_image(mdata, interpolation='nearestneighbor')
        mdata = mdata.numpy()
        mdata = mdata.astype(np.uint16)
        
        # Compute the new centroids for the transformed data
        regp = regionprops(mdata) 
        post_centroids = np.array([list(rp.centroid) for rp in regp]) * mres
        tf_labels = [rr.label for rr in regp]
        
        # Read in the cell geometry data and assign the new centroids to each label
        mfile = [sf for sf in cell_data_files if best_matches['moving'][idx] + '-' in sf][0]
        df = pd.read_csv(mfile, sep='\t')    
        df = df.loc[df['label'].isin(tf_labels)]
        for ii, lab in enumerate(tf_labels):
            df.loc[df['label'] == lab, ['z', 'y', 'x']] = post_centroids[ii]
        dfs.append(df)
    df = pd.concat(dfs, ignore_index=True)
    return df
df = collate_registered_cell_data(seg_files, data_files, trans_files)
df.to_csv(f'{OUTPUT_DIR}/registered_cell_data.csv', sep='\t')
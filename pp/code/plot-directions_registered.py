#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 29 20:37:58 2020

@author: henrik
"""
import os, sys, re
import numpy as np
import pandas as pd
import tifffile as tiff

from scipy.ndimage import rotate
from skimage.measure import regionprops
from img2org.conversion import read_neigh_init, read_init
from imgmisc import listdir
from imgmisc import get_resolution
from imgmisc import mkdir
from imgmisc import natural_sort
from imgmisc import get_layers
from multiprocessing import Pool, cpu_count
import pyvista as pv
import pandas as pd
from imgmisc import vector_mesh
from imgmisc import average_vectors    
import ants

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'
plant_type = 'WT'
wall_depth = 1

init_PIN_index = 4
neigh_PIN_index = 1
neigh_variable_index = 1 # 0: area, 1: PIN, 2: AUX, 3: FLUX

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
SEG_DIR = f'{PROJECT_DIR}/data/{dataset}'
CHANNELS_DIR = f'{PROJECT_DIR}/data/{dataset}'
DATA_DIR = f'{PROJECT_DIR}/data/{dataset}/end_state_prod_1_default' # walls
INIT_DIR = f'{PROJECT_DIR}/data/{dataset}/quantified/' # inits
OUTPUT_DIR = f'{PROJECT_DIR}/data/{dataset}/quantified/quantified/figures'
TRANS_DIR = f'{PROJECT_DIR}/data/{dataset}/registration'
mkdir(OUTPUT_DIR)

seg_files = listdir(SEG_DIR, sorting='natural', include='-refined.tif', recursive=False)
init_files = listdir(INIT_DIR, sorting='natural', include='.init', recursive=False)
data_files = listdir(DATA_DIR, sorting='natural', include='walls_end_state.dat', recursive=False)
trans_files = listdir(TRANS_DIR, sorting='natural', include='.mat')
plants = np.unique([re.findall('plant_(\d+)', ff)[0] for ff in data_files]).astype(int)
    
df = pd.read_csv(f'{PROJECT_DIR}/data/{dataset}/registered_cell_data.csv', sep='\t')
center = df[['z', 'y', 'x']].mean(0).values

def vectors(cell_data, neigh_data, neigh_variable_index, flip_directions=False, normalise=True, mode='max'):
    # Retrieve all the directional data for the subset in question        
    all_dir_data = [np.vstack(([ii] * neigh_data[ii]['n_neighs'], 
                               neigh_data[ii]['neighs'], 
                               neigh_data[ii][f'p{neigh_variable_index}'])) for ii in neigh_data.keys()]
    all_dir_data = pd.DataFrame(np.hstack(all_dir_data).T)
    all_dir_data.columns = ['cell_from', 'cell_to', 'value']
    all_dir_data[['cell_from', 'cell_to']] += 1 # neigh data always offset by 1
    dir_data = all_dir_data.loc[all_dir_data['cell_to'].isin(cell_data['label']) & 
                                all_dir_data['cell_from'].isin(cell_data['label'])] 
    
    # Get which direction to show
    if mode == 'max':
        dir_data = dir_data.loc[dir_data.groupby('cell_from').idxmax()['value']]
    elif mode == 'all':
        pass
    else:
        raise NotImplementedError()

    # Compute vector centers and directions
    all_data = dir_data.merge(cell_data, left_on='cell_from', right_on='label')
    all_data = all_data.merge(cell_data, left_on='cell_to', right_on='label', suffixes=['_from', '_to'])
    centers = (all_data[['z_from', 'y_from', 'x_from']].values + 
                      all_data[['z_to', 'y_to', 'x_to']].values) / 2
    directions = (all_data[['z_to', 'y_to', 'x_to']].values -
                  all_data[['z_from', 'y_from', 'x_from']].values)
    scalars = all_data['value'].values
    
    if flip_directions:
        directions *= -1
    
    if normalise:
        lengths = (directions**2).sum(1) ** .5
        directions = (directions.T / lengths).T
        
    return centers, directions, scalars

all_centers, all_directions, all_scalars = [], [], []
all_l1_centers, all_l1_directions, all_l1_scalars = [], [], []
all_xsect_centers, all_xsect_directions, all_xsect_scalars = [], [], []
plant = plants[1]
for plant in plants:
    print(plant)
    seg_file = [ss for ss in seg_files if f'plant_{plant}-' in ss][0]
    init_file = [ss for ss in init_files if f'plant_{plant}-' in ss][0]
    plant_data_files = [data_file for data_file in data_files if f'plant_{plant}-' in data_file]
    plant_data_file = plant_data_files[0]

    seg_img = tiff.imread(seg_file)
    resolution = get_resolution(seg_file)
    cell_data = df.loc[df['plant'] == plant]
    neigh_data = read_neigh_init(plant_data_file)
    del neigh_data['n_cells'], neigh_data['n_params']
    
    l1, l2 = get_layers(seg_img, background=0, depth=2)
    xsect = cell_data.loc[((cell_data[['centroid-1']] - center[1])**2).sum(1)**.5 < 5, 'label'].values
        
    l1_centers, l1_directions, l1_scalars = vectors(cell_data=cell_data.loc[cell_data['label'].isin(l1)], 
                                                    neigh_data={vv:neigh_data[vv] for vv in neigh_data.keys() if vv + 1 in l1},  # remember offset
                                                    neigh_variable_index=neigh_variable_index)
    xsect_centers, xsect_directions, xsect_scalars = vectors(cell_data=cell_data.loc[cell_data['label'].isin(xsect)], 
                                                    neigh_data={vv:neigh_data[vv] for vv in neigh_data.keys() if vv + 1 in xsect},  # remember offset
                                                    neigh_variable_index=neigh_variable_index)
    centers, directions, scalars = vectors(cell_data=cell_data, 
                                           neigh_data=neigh_data, 
                                           neigh_variable_index=neigh_variable_index)
    all_l1_centers.append(l1_centers)
    all_l1_directions.append(l1_directions)
    all_l1_scalars.append(l1_scalars)
    all_xsect_centers.append(xsect_centers)
    all_xsect_directions.append(xsect_directions)
    all_xsect_scalars.append(xsect_scalars)
    all_centers.append(centers)
    all_directions.append(directions)
    all_scalars.append(scalars)
all_l1_centers = np.vstack(all_l1_centers)
all_l1_directions = np.vstack(all_l1_directions)
all_l1_scalars = np.hstack(all_l1_scalars)
all_xsect_centers = np.vstack(all_xsect_centers)
all_xsect_directions = np.vstack(all_xsect_directions)
all_xsect_scalars = np.hstack(all_xsect_scalars)
all_centers = np.vstack(all_centers)
all_directions = np.vstack(all_directions)
all_scalars = np.hstack(all_scalars)

# TODO  NORMALISE VECTOR SCALARS PROPERLY
if neigh_variable_index == 3:
    all_l1_directions *= -1
    all_xsect_directions *= -1
    all_directions *= -1

if neigh_variable_index == 1:
    var = 'pin'
elif neigh_variable_index == 2:
    var = 'aux'
elif neigh_variable_index == 3:
    var = 'flux'
    
oname = f'/home/henrikahl/projects/pin_patterns/figures/registered_meristems-{var}_directions'

to_keep = ~np.array([any(np.isnan(ff)) for ff in all_centers])
all_scalars = all_scalars[to_keep]
all_centers = all_centers[to_keep]
all_directions = all_directions[to_keep]

r = 8
iters = 2
scale = 8
all_centers_smooth, all_directions_smooth, all_scalars_smooth = average_vectors(all_centers, all_directions, iters, r=r, scalars=all_scalars)
all_scalars_smooth = [np.quantile(np.abs(all_scalars_smooth), .99) if vv > np.quantile(np.abs(all_scalars_smooth), .99) else vv for vv in all_scalars_smooth]
all_scalars_smooth = np.array(all_scalars_smooth)

all_scalars_smooth /= np.quantile(np.abs(all_scalars_smooth), .99)
# all_scalars_smooth = [1 if vv > np.quantile(np.abs(all_scalars_smooth), .99) else 0 for vv in all_scalars_smooth]
# vmesh = vector_mesh(all_centers_smooth, all_directions_smooth, scale=scale, scalars=all_scalars_smooth, scalar_tag='values', scalar_weights=0)    
vmesh = vector_mesh(all_centers_smooth, all_directions_smooth, scale=scale, scalars=all_scalars_smooth, scalar_tag='values')    
p = pv.Plotter(off_screen=False)
# p = pv.Plotter(off_screen=True)
# p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, clim=[0,1])
p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=True, clim=[0,1])
p.set_background('white')
# p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
p.view_yz()
# p.screenshot(f'{oname}-top.png', transparent_background=True,  window_size=[2000,2000])
p.show()

to_keep = ~np.array([any(np.isnan(ff)) for ff in all_l1_centers])
all_l1_scalars = all_l1_scalars[to_keep]
all_l1_centers = all_l1_centers[to_keep]
all_l1_directions = all_l1_directions[to_keep]



r = 15
iters = 2
scale = 8
all_l1_centers_smooth, all_l1_directions_smooth, all_l1_scalars_smooth = average_vectors(all_l1_centers, all_l1_directions, iters, r=r, scalars=all_l1_scalars)
all_l1_scalars_smooth = [np.quantile(np.abs(all_l1_scalars_smooth), .99) if vv > np.quantile(np.abs(all_l1_scalars_smooth), .99) else vv for vv in all_l1_scalars_smooth]
all_l1_scalars_smooth = np.array(all_l1_scalars_smooth)

all_l1_scalars_smooth /= np.quantile(np.abs(all_l1_scalars_smooth), .99)
# all_l1_scalars_smooth = [1 if vv > np.quantile(np.abs(all_l1_scalars_smooth), .99) else 0 for vv in all_l1_scalars_smooth]
vmesh = vector_mesh(all_l1_centers_smooth, all_l1_directions_smooth, scale=scale, scalars=all_l1_scalars_smooth, scalar_tag='values', scalar_weights=0)    
p = pv.Plotter(off_screen=False)
# p = pv.Plotter(off_screen=True)
# p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, clim=[0,1])
p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=True, clim=[0,1])
p.set_background('white')
# p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
p.view_yz()
# p.screenshot(f'{oname}-top.png', transparent_background=True,  window_size=[2000,2000])
p.show()
p.close()


to_keep = ~np.array([any(np.isnan(ff)) for ff in all_xsect_centers])
all_xsect_scalars = all_xsect_scalars[to_keep]
all_xsect_centers = all_xsect_centers[to_keep]
all_xsect_directions = all_xsect_directions[to_keep]

r = 15
iters = 1
scale = 6
all_xsect_centers[:, 1] = np.mean(all_xsect_centers[:, 1])
all_xsect_centers_smooth, all_xsect_directions_smooth, all_xsect_scalars_smooth = average_vectors(all_xsect_centers, 
                                                                                                  all_xsect_directions, 
                                                                                                  iters, 
                                                                                                  r=r, 
                                                                                                  scalars=all_xsect_scalars, 
                                                                                                  weights='linear')
all_xsect_scalars_smooth /= np.quantile(np.abs(all_xsect_scalars_smooth), .99)

vmesh = vector_mesh(all_xsect_centers_smooth, all_xsect_directions_smooth, scale=scale, scalars=all_xsect_scalars_smooth, scalar_tag='values')    
vmesh.rotate_y(-90)
p = pv.Plotter(off_screen=False)
p.set_background('white')
p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, clim=[0,1])
# p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
p.view_xz()
p.show()
# p.screenshot(f'{oname}-side.png', transparent_background=True,  window_size=[2000,2000])
p.close()
# p.show()

# ###############################################################################
# neigh_variable_index = 1

# all_centers, all_directions, all_scalars = [], [], []
# all_l1_centers, all_l1_directions, all_l1_scalars = [], [], []
# all_xsect_centers, all_xsect_directions, all_xsect_scalars = [], [], []
# plant = plants[1]
# for plant in plants:
#     print(plant)
#     seg_file = [ss for ss in seg_files if plant + '-' in ss][0]
#     init_file = [ss for ss in init_files if plant + '-' in ss][0]
#     plant_data_files = [data_file for data_file in data_files if plant + '-' in data_file]
#     plant_data_file = plant_data_files[1]

#     seg_img = tiff.imread(seg_file)
#     resolution = get_resolution(seg_file)
#     cell_data = df.loc[df['plant'] == plant]
#     neigh_data = read_neigh_init(plant_data_file)
#     del neigh_data['n_cells'], neigh_data['n_params']
    
#     l1, l2 = get_layers(seg_img, background=0, depth=2)
#     xsect = cell_data.loc[((cell_data[['centroid-1']] - center[1])**2).sum(1)**.5 < 5, 'label'].values
        
#     l1_centers, l1_directions, l1_scalars = vectors(cell_data=cell_data.loc[cell_data['label'].isin(l1)], 
#                                                     neigh_data={vv:neigh_data[vv] for vv in neigh_data.keys() if vv + 1 in l1},  # remember offset
#                                                     neigh_variable_index=neigh_variable_index)
#     xsect_centers, xsect_directions, xsect_scalars = vectors(cell_data=cell_data.loc[cell_data['label'].isin(xsect)], 
#                                                     neigh_data={vv:neigh_data[vv] for vv in neigh_data.keys() if vv + 1 in xsect},  # remember offset
#                                                     neigh_variable_index=neigh_variable_index)
#     centers, directions, scalars = vectors(cell_data=cell_data, 
#                                            neigh_data=neigh_data, 
#                                            neigh_variable_index=neigh_variable_index)
#     all_l1_centers.append(l1_centers)
#     all_l1_directions.append(l1_directions)
#     all_l1_scalars.append(l1_scalars)
#     all_xsect_centers.append(xsect_centers)
#     all_xsect_directions.append(xsect_directions)
#     all_xsect_scalars.append(xsect_scalars)
#     all_centers.append(centers)
#     all_directions.append(directions)
#     all_scalars.append(scalars)
# all_l1_centers = np.vstack(all_l1_centers)
# all_l1_directions = np.vstack(all_l1_directions)
# all_l1_scalars = np.hstack(all_l1_scalars)
# all_xsect_centers = np.vstack(all_xsect_centers)
# all_xsect_directions = np.vstack(all_xsect_directions)
# all_xsect_scalars = np.hstack(all_xsect_scalars)
# all_centers = np.vstack(all_centers)
# all_directions = np.vstack(all_directions)
# all_scalars = np.hstack(all_scalars)

# if neigh_variable_index == 3:
#     all_l1_directions *= -1
#     all_xsect_directions *= -1
#     all_directions *= -1

# if neigh_variable_index == 3:
#     var = 'flux'
# else:
#     var = 'pin'
    
# oname = f'/home/henrikahl/projects/pin_patterns/figures/registered_meristems-{var}_directions'

# r = 8
# iters = 1
# scale = 8
# all_l1_centers_smooth, all_l1_directions_smooth, all_l1_scalars_smooth = average_vectors(all_l1_centers, all_l1_directions, iters, r=r, scalars=all_l1_scalars)
# vmesh = vector_mesh(all_l1_centers_smooth, all_l1_directions_smooth, scale=scale, scalars=all_l1_scalars_smooth, scalar_tag='values')    
# p = pv.Plotter(off_screen=False)
# p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, clim=[0,1])
# p.set_background('white')
# # p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
# p.view_yz()
# # p.screenshot(f'{oname}-top.png', transparent_background=True,  window_size=[2000,2000])
# p.show()
# p.close()

# r = 10
# iters = 1
# scale = 5
# all_xsect_centers[:, 1] = np.mean(all_xsect_centers[:, 1])
# all_xsect_centers_smooth, all_xsect_directions_smooth, all_xsect_scalars_smooth = average_vectors(all_xsect_centers, all_xsect_directions, iters, r=r, scalars=all_xsect_scalars, weights='linear')
# vmesh = vector_mesh(all_xsect_centers_smooth, all_xsect_directions_smooth, scale=scale, scalars=all_xsect_scalars_smooth, scalar_tag='values')    
# vmesh.rotate_y(-90)
# p = pv.Plotter(off_screen=True)
# p.set_background('white')
# p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, clim=[0,1])
# # p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
# p.view_xz()
# p.screenshot(f'{oname}-side.png', transparent_background=True,  window_size=[2000,2000])
# p.close()
# # p.show()



# # vmesh.clip(normal='y', origin=vmesh.center_of_mass()).plot()

# # r = 15
# # iters = 2
# # all_centers_smooth, all_directions_smooth, all_scalars_smooth = average_vectors(all_centers, all_directions, iters, r=r, scalars=all_scalars, weights='linear')
# # vmesh = vector_mesh(all_centers_smooth, all_directions_smooth, scale=scale, scalars=all_scalars_smooth, scalar_tag='values')
# # p = pv.Plotter(notebook=False)
# # p.add_mesh(vmesh, scalars='values', cmap='turbo', show_scalar_bar=False, smooth_shading=True)
# # p.view_yz()
# # p.set_background('white')
# # # p.camera_position = [(291.59469218587776, 81.38540033482442, 36.95115492180716),
# # #                      (15.950106263160706, 68.2645812034607, 68.15921306610107),
# # #                      (0.11965042420396362, -0.20122030698455787, 0.9722109668404549)]
# # # p.save_graphic('/home/henrikahl/auxin_fluxes.svg', raster=False)
# # p.show()

# # centers, directions, scalars = average_vectors(centers, directions, iters, r=r, scalars=scalars)
# # vmesh = vector_mesh(centers, directions, scale=15, scalars=scalars, scalar_tag='values')    
# # p = pv.Plotter(notebook=False)
# # p.add_mesh(vmesh, scalars='values')
# # p.add_mesh(pv.Sphere(radius=r, center=vmesh.center))
# # p.show()
    


#     # vmesh = average_vectors(vmesh, 0, 10)
    
    
# # mesh = pv.PolyData()
# # for vv in all_vectors:
# #     mesh += vv
# # mesh = weighted_average_vectors(mesh, 3)

# # mesh = all_vectors[0]
# # streamlines, src = mesh.streamlines(
# #     return_source=True,
# #     max_time=100.0,
# #     initial_step_length=2.0,
# #     terminal_speed=0.1,
# #     n_points=25,
# #     # source_radius=2.0,
# #     # source_center=(133.1, 116.3, 5.0),
# # )


# # p = pv.Plotter(notebook=False)
# # p.add_mesh(streamlines.tube(radius=0.2), lighting=False)
# # p.add_mesh(src)
# # # p.add_mesh(boundary, color="grey", opacity=0.25)
# # # p.camera_position = [(10, 9.5, -43), (87.0, 73.5, 123.0), (-0.5, -0.7, 0.5)]
# # p.show()


#     # view='xz'
#     # value_tag='scalars'
#     # show_scalar_bar=True 
#     # cmap='turbo'
#     # invert_view=False
#     # lighting=False
#     # opacity=1
#     # clim=None#

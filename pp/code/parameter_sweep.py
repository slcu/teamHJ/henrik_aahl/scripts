#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 11:29:32 2020

@author: henrikahl
"""

import os
import sys
import numba
import numpy as np
from numba import njit
from imgmisc import mkdir
from imgmisc import listdir
from itertools import product
from scipy.integrate import solve_ivp
from img2org.conversion import read_init
from img2org.conversion import read_neigh_init
from multiprocessing import Pool
from multiprocessing import cpu_count
import pyvista as pv

def to_nested_np(arr):  
            return np.array([np.array(val) for val in arr])
        
def make_2D_array(lis):
    """Funciton to get 2D array from a list of lists
    """
    n = len(lis)
    lengths = np.array([len(x) for x in lis])
    max_len = np.max(lengths)
    arr = np.full((n, max_len), np.nan)

    for i in range(n):
        arr[i, :lengths[i]] = lis[i]
    return arr, lengths

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'
plant_type = 'WT'
wall_depth = 1

volume_index = 3
auxin_index = 6

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
INPUT_DIR = f'{PROJECT_DIR}/data/{dataset}/quantified'
OUTPUT_DIR = f'{PROJECT_DIR}/data/{dataset}/parameter_sweep'
mkdir(OUTPUT_DIR)
# CELLS_DIR = f'{OUTPUT_DIR}/cells'
# WALLS_DIR = f'{OUTPUT_DIR}/walls'
# mkdir(CELLS_DIR)
# mkdir(WALLS_DIR)

init_files = listdir(INPUT_DIR, include=['.init'], exclude=['end_state', 'original'], sorting='natural')

args = list(product(*[np.arange(len(init_files)), np.arange(-3, 4), np.arange(-3, 4), [0]]))
def run_ccat(args):
    file_index, xx, yy, prod = args
    print(os.path.basename(init_files[file_index]), xx, yy, prod)

    # Read in paths and data
    init_path = init_files[file_index]
    neigh_path = f'{init_path[:-5]}.neigh'
    
    # Iteration specific parameters
    De = 2.2e-03                    # passive efflux (p_AH f^cell_AH) : 2.2e-03
    Pe = 1.26e-00 * 10**(float(xx)) # PIN mediated efflux : 1.26e-00
    Di = 1.32e-01                   # passive influx (p_AH f^wall_AH) : 1.32e-01
    Pi = 1.96e-00 * 10**(float(yy)) # AUX mediated influx : 1.96e-00
 
    # Extract values from data
    init = read_init(init_path)
    volumes = init[volume_index].values
    auxin = init[auxin_index].values.copy()
    
    in_l1 = init[7]
    in_l2 = init[8]
    in_bottom = init[9]
    in_l1 = in_l1.index[in_l1 == 1].values
    in_l2 = in_l2.index[in_l2 == 1].values
    in_bottom = in_bottom.index[in_bottom == 1].values
#    if any(init.columns == 'organ_id'):
#        in_organ1to3 = init['organ_id'].index[np.isin(init['organ_id'], [1,2,3])].values

    neigh_data = read_neigh_init(neigh_path)
    neighs, auxs, pins, npins, nauxs, areas, ninl1, ninl2, ninbottom = [], [], [], [], [], [], [], [], []
    del neigh_data['n_cells'], neigh_data['n_params']
    for key, val in neigh_data.items():
        neighs.append(list(val['neighs'].astype(int)))
        ninl1.append(list(np.isin(val['neighs'], in_l1)))
        ninl2.append(list(np.isin(val['neighs'], in_l2)))
        ninbottom.append(list(np.isin(val['neighs'], in_bottom)))
        areas.append(list(val['p0'].astype(float)))
        pins.append(list(val['p1'].astype(float)))
        auxs.append(list(val['p2'].astype(float)))
        npins.append(list(np.array([float(neigh_data[nn]['p1'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))
        nauxs.append(list(np.array([float(neigh_data[nn]['p2'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))
    n_neighs = np.array([sum(nn > 0) for nn in neighs])

    # Ensure comprehensible formats for function defintion
    ntest, nlens = make_2D_array(neighs)
    pins, nlens = make_2D_array(pins)
    auxs, nlens = make_2D_array(auxs)
    npins, nlens = make_2D_array(npins)
    nauxs, nlens = make_2D_array(nauxs)
    areas, nlens = make_2D_array(areas)
    ninl1, nlens = make_2D_array(ninl1)
    ninl2, nlens = make_2D_array(ninl2)
    ninbottom, nlens = make_2D_array(ninbottom)
    ntest = ntest.astype(int)
    fluxes = np.zeros_like(pins)
    
    # Normalise data
    normalisation_factor = np.nanmean([np.nansum(pp) for pp in pins])
    auxin = auxin / normalisation_factor 
    pins = pins / normalisation_factor 
    auxs = auxs / normalisation_factor 
    npins = npins  / normalisation_factor 
    nauxs = nauxs / normalisation_factor 

    # Define the transport function. Using @njit speeds things up massively 
    # (although it does limit our formalism a little by preventing us from 
    # using nested arrays, which is why we convert things to 2D arrays above.)
    # hill_peripheral = lambda x: x.astype(float)**12 / (60**12 + x.astype(float)**12)
    def hill_peripheral(x): 
        return x.astype(float)**12 / (60**12 + x.astype(float)**12)
    # hill_central = lambda x: 1. - x**12 / (30**12 + x**12)
    def hill_central(x):
        return 1. - x**12 / (30**12 + x**12)
    
    p_auxin = .1 if prod == 1 else 0
    d_auxin = .01 if prod in [1, 2, 3, 4] else 0
    p_auxin_auxin = .1 if prod == 2 else 0
    p_auxin_peripheral = (.1 * hill_peripheral(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if prod == 3 else np.zeros(init.shape[0])).astype(float)
    p_auxin_central = (.1 * hill_central(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if prod == 4 else np.zeros(init.shape[0])).astype(float)
    
    # If we're in one of the regional production phenotypes, we should adjust 
    # the production so that the total production in the tissue remains the same
    # between different production cases
    total_prodiction_WT = sum(.1 * volumes)
    if prod == 3:
        p_auxin_peripheral = p_auxin_peripheral / (sum(p_auxin_peripheral * volumes) / total_prodiction_WT)
    elif prod == 4:
        p_auxin_central = p_auxin_central / (sum(p_auxin_central * volumes) / total_prodiction_WT)
    
    basal_efflux = 1 if prod in [1,2,3,4] else 0
    @njit(parallel=True, fastmath=True)
    def cell_cell_auxin_transport(t, values, fluxes):
        output = np.zeros(values.shape[0])       
        for ii in numba.prange(values.shape[0]):
            stem_flux = De / n_neighs[ii] / n_neighs[ii]  * in_bottom[ii] * basal_efflux * values[ii]
            #cell_sum = .0 - stem_flux * n_neighs[ii]
            for jj, nei in enumerate(ntest[ii, :nlens[ii]]):
                flux = areas[ii, jj] * ((Di + Pi * auxs[ii, jj]) * (De + Pe * npins[ii, jj]) * values[nei] - 
                                        (Di + Pi * nauxs[ii, jj]) * (De + Pe * pins[ii, jj]) * values[ii] + stem_flux) / (2 * Di + Pi * (auxs[ii, jj] + nauxs[ii, jj]))
                fluxes[ii, jj] = flux
                #cell_sum += flux 
            output[ii] = 1 / volumes[ii] * cell_sum + ( 
                # case 0: no production & degradation
                p_auxin +                       # case 1: global production
                p_auxin_auxin * values[ii]**2 + # case 2: auxin self-activation
                p_auxin_peripheral[ii] +        # case 3: peripheral auxin production ("from flowers")
                p_auxin_central[ii] -           # case 4: central auxin production ("from CZ")
                d_auxin * values[ii])           # happens in all cases but 0
        if prod != 0:
            output[in_bottom] = 0
        output[output < 0] = 0
        output[np.isnan(output)] = 0
        output[np.isinf(output)] = 0
        return output

    # Solve!
    y0 = auxin.copy()
    t_res = solve_ivp(cell_cell_auxin_transport, y0=y0, t_span=(0, 10**6), method='BDF', args=(fluxes, ), rtol=1e-6, atol=1e-10)

    # Collect data we want to save    
    output = init.copy()
    output[auxin_index] = t_res.y[:, -1]

    corr = np.corrcoef(output[auxin_index].values, output[auxin_index-1].values)[0,1]
    
    l1l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    l1l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    l2l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 
    l2l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 

    output[10] = fluxes.sum(1)
    output[11] = l1l1_fluxes
    output[12] = l1l2_fluxes
    output[13] = l2l2_fluxes
    output[14] = l2l1_fluxes

    p = pv.Plotter(notebook=False)
    p.add_points(output[[0,1,2]].values, scalars=output[auxin_index].values, render_points_as_spheres=True, point_size=30, cmap='turbo')
    p.show()

    # Save output to file
    basename = os.path.basename(os.path.splitext(init_path)[0])
    fname_out = f'{OUTPUT_DIR}/{basename}-pe_{xx}-pi_{yy}-prod_{int(prod)}-cells_end_state.dat'
    print(f'Printing {fname_out}')
    output.to_csv(fname_out, header=False, sep='\t', index=False, float_format='%.15f')

    fname_out = f'{OUTPUT_DIR}/{basename}-pe_{xx}-pi_{yy}-prod_{int(prod)}-walls_end_state.dat'
    print(f'Printing {fname_out}')
    neigh_output = neigh_data.copy()
    for ii in range(len(neigh_output)):
        neigh_output[list(neigh_output.keys())[ii]]['p3'] = fluxes[ii][:nlens[ii]]
    from img2org.conversion import write_neigh_init
    neigh_output['n_cells'] = len(output)
    neigh_output['n_params'] = 4
    write_neigh_init(fname_out, neigh_output)        
    
if len(sys.argv) < 2:
    n_cores = cpu_count() - 1
else:
    n_cores = int(sys.argv[1])


prod_states = [0]
x_range = np.arange(-3, 4)
y_range = np.arange(-3, 4)
args = list(product(*[np.arange(len(init_files)), x_range, y_range, prod_states]))
p = Pool(n_cores)
output = p.map(run_ccat, args)
p.close()           
p.join()


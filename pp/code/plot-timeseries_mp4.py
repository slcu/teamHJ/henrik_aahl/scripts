#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 24 08:33:53 2022

@author: henrikahl
"""

import tifffile as tiff
import pyvista as pv
import numpy as np
from imgmisc import listdir
from imgmisc import mkdir
from imgmisc import get_l1
from imgmisc import match_shape
import os
from pystackreg import StackReg
from imgmisc import autocrop
import cv2
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation
from matplotlib.animation import FuncAnimation
from itertools import count
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

MAXPROJ_DIR = '/media/henrikahl/My Passport1/data/221114-pUBQ10-WT-Timelapse_4h/l1_max_projections'
VID_DIR = f'{MAXPROJ_DIR}/../videos'
mkdir(MAXPROJ_DIR)
mkdir(VID_DIR)

seg_files = listdir('/media/henrikahl/My Passport1/data/221114-pUBQ10-WT-Timelapse_4h/segmented_refined',
                    sorting='natural')
sig_files = listdir('/media/henrikahl/My Passport1/data/221114-pUBQ10-WT-Timelapse_4h/cropped',
                    sorting='natural')

iter_ = 0
# assert(len(seg_files) == len(sig_files))
# for iter_ in range(len(seg_files)):
#     print(f'Maximal projection: {iter_ + 1} / {len(seg_files)}')
#     seg_fname = seg_files[iter_]
#     sig_fname = sig_files[iter_]
#     basename = bname(sig_fname)
#     if os.path.isfile(f'{MAXPROJ_DIR}/{basename}-l1_max_proj.tif'):
#         continue
    
#     seg_img = tiff.imread(seg_fname)
#     sig_img = tiff.imread(sig_fname)
    
#     l1 = get_l1(seg_img, background=0)
#     sig_img[~np.isin(seg_img, l1)] = 0
#     # tiff.imshow(np.max(sig_img, 0))
#     tiff.imwrite(f'{MAXPROJ_DIR}/{basename}-l1_max_proj.tif', 
#                   np.max(sig_img, 0))


plants = [1, 2, 3, 4, 5, 6, 7, 8]
stacks = []
for plant in plants:
    plant_files = listdir(MAXPROJ_DIR, sorting='natural', include=f'plant_{plant}-')
    fimgs = [tiff.imread(pf) for pf in plant_files]
    
    # Get shapes of all images for a given plant
    max_x = max([fi.shape[0] for fi in fimgs])
    max_y = max([fi.shape[1] for fi in fimgs])
    max_shape = [max_x, max_y]
    max_shape = [max(max_shape), max(max_shape)]
    max_shape[0] += 150 # do some padding...
    max_shape[1] += 150
    
    fimgs = [match_shape(fi, max_shape) for fi in fimgs]
    fimg = np.stack(fimgs, axis=0)
    
    # register the stack
    sr = StackReg(StackReg.RIGID_BODY)
    reg_fimg = sr.register_transform_stack(fimg)
    reg_fimg[reg_fimg < 5000] = 0 # arbitrary background removal
    reg_fimg = autocrop(reg_fimg, n=100)
    stacks.append(reg_fimg)

max_shape = [max([ff.shape[1] for ff in stacks]), max([ff.shape[2] for ff in stacks])]
max_shape = [max(max_shape), max(max_shape)]
stacks = [match_shape(ff, [ff.shape[0]] + max_shape) for ff in stacks]

for ii, stack in enumerate(stacks):
    n_imgs = len(stack)
    for jj in range(12-n_imgs):
        stacks[ii] = np.vstack([stacks[ii], np.expand_dims(np.zeros_like(stacks[ii][0]), axis=0)])

stacks = np.array(stacks)
from scipy.ndimage import gaussian_filter
for ii in range(len(stacks)):
    for jj in range(len(stacks[ii])):
        stacks[ii, jj] = gaussian_filter(stacks[ii, jj], 1)

stacks = np.round(stacks / stacks.max() * 255, 0).astype('uint8')

new_stacks = [[], []]
new_stacks[0] = stacks[:4]
new_stacks[1] = stacks[4:]
new_stacks = np.array(new_stacks)
# tiff.imshow(np.dstack(np.dstack(new_stacks)))
new_stacks = np.dstack(np.dstack(new_stacks))
# new_stacks = np.rollaxis(new_stacks, 0).T
cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ['black', "red"])
cmap='inferno'
# new_stacks = new_stacks.T
matplotlib.use('Agg')
# matplotlib.use('Qt5Agg')

for ii in range(len(new_stacks)):
    
    fig = plt.figure(frameon=False, figsize=(15*2,15))
    plt.axis('off')
    i=0
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    im = ax.imshow(new_stacks[ii], animated=True, cmap=cmap, aspect='equal', interpolation='bilinear')
    # plt.tight_layout()
    plt.margins(0,0)
    im.write_png(f'all_plants_im_{ii}.png')

def updatefig(*args):
    global i
    print(i)
    if (i < len(new_stacks) - 1):
        i += 1
    # else:
    #     i=0
    im.set_array(new_stacks[i])
    return im,
ani = animation.FuncAnimation(fig, updatefig,  blit=True, frames=11)
FFwriter = animation.FFMpegWriter(fps=1)
ani.save(f'{VID_DIR}/all_plants-video.mp4', writer=FFwriter)

matplotlib.use('Agg')
for iter_, plant in enumerate(plants):
    fig = plt.figure(frameon=False, figsize=(15,15))
    plt.axis('off')
    i=0
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    im = ax.imshow(stacks[iter_][0], animated=True, cmap=cmap, aspect='equal', interpolation='bilinear')
    # plt.tight_layout()
    plt.margins(0,0)
    def updatefig(*args):
        global i
        if (i < len(stacks[iter_]) - 1):
            i += 1
        else:
            i=0
        im.set_array(stacks[iter_][i])
        return im,
    ani = animation.FuncAnimation(fig, updatefig,  blit=True)
    FFwriter = animation.FFMpegWriter(fps=2)
    ani.save(f'{VID_DIR}/plant_{plant}-video.mp4', writer=FFwriter)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import os
import numpy as np
import pandas as pd
import pyvista as pv
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from skimage.measure import regionprops_table

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
SEG_DIR = f'{PROJECT_DIR}/data/{dataset}'
MESH_DIR = f'{PROJECT_DIR}/data/{dataset}/organ_segmentations'
INIT_DIR = f'{PROJECT_DIR}/data/{dataset}/quantified'
PHY_DIR = f'{PROJECT_DIR}/data/{dataset}/phyllotaxis_data'

# TRANS_DIR = f'{PROJECT_DIR}/data/{dataset}/registration'
OUTPUT_DIR = f'{PROJECT_DIR}/data/{dataset}'
mkdir(OUTPUT_DIR)

seg_files = listdir(SEG_DIR, sorting='natural', include='-refined.tif')
mesh_files = listdir(MESH_DIR, sorting='natural', include='.vtk')
init_files = listdir(INIT_DIR, sorting='natural', include='.init')
phy_files = listdir(PHY_DIR, sorting='natural', include='.csv')

ii = 0
for ii in range(len(seg_files)):
    # Read in data
    seg_img = tiff.imread(seg_files[ii])
    mesh = pv.read(mesh_files[ii])
    
    # p = pv.Plotter(off_screen=True)
    # bname = lambda x: os.path.basename(os.path.splitext(x)[0])
    # p.add_mesh(mesh, cmap='Set2', show_scalar_bar=False, interpolate_before_map=False)
    # p.view_yz()
    # p.screenshot(f'/home/henrikahl/plots/{bname(seg_files[ii])}-mesh.png', transparent_background=True, 
    #              window_size=[2000,2000])
    # p.close()
    
    resolution = get_resolution(seg_files[ii])
    domains = mesh['domains'] 
    pts = np.round(np.array(mesh.points) / resolution).astype(int)
    
    # compute the centrois    
    centers = regionprops_table(seg_img, properties=['label', 'centroid'])
    centers = pd.DataFrame(centers)

    # Compute which organ is above/below/closest the cell centroids
    hits = []
    for jj, row in centers.iterrows():
        print(jj)
        label = row.values[0]
        center = row.values[1:]
        pt = center * resolution
        hit = mesh.ray_trace(origin=pt, end_point=pt + np.array([9999, 0, 0]))
        if len(hit[0]) == 0:
            hit = mesh.ray_trace(origin=pt, end_point=pt - np.array([9999, 0, 0]))
        if len(hit[0]) > 0:
            hit = hit[0][0]
        else:
            hit = mesh.points[mesh.find_closest_point(pt)]
        hits.append(hit)
    hits = np.array([mesh.find_closest_point(pt) for pt in hits])
    import re
    
    # mesh.plot(cmap='Set2', background='white')
    
    # Get list of cells and their computed organs
    domain_labels = mesh['domains'][hits] # unordered organs
    labels = centers.label.values # cell labels
 
    infostr = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)', seg_files[ii])[0]
    dataset, genotype, reporter, plant = infostr
    dataset, plant = int(dataset), int(plant)
    
    phy_df = pd.read_csv(phy_files[ii], sep='\t') # ordered domains (excluding center)
    phy_df['label_unordered'] = [mesh['domains'][
        mesh.find_closest_point(phy_df.loc[jj][['coord-0', 'coord-1', 'coord-2']].values)] for jj in range(len(phy_df))]
    
    mapping = {phy_df.iloc[jj].label_unordered:jj+1 for jj in range(len(phy_df))}
    mapping[0] = 0 # don't hardcode this
    
    mesh['ordered'] = [mapping[dd] for dd in mesh['domains']]
    organ_ids = np.asarray([mapping[dd] for dd in domain_labels], dtype='int')
    
    # df = pd.DataFrame({'dataset':[dataset] * len(labels),
    #                    'genotype':[genotype] * len(labels),
    #                    'reporter':[reporter] * len(labels),
    #                    'plant':[plant] * len(labels),
    #                    'cell_id':labels,
    #                    'organ_id':organ_ids})
    
    from img2org.conversion import read_init, write_init
    init = read_init(init_files[ii])
    init['organ_id'] = np.nan
    for jj, lab in enumerate(labels):
        init.loc[lab-1, 'organ_id'] = organ_ids[jj]
    write_init(init_files[ii], init)
    
    ## Plot the labelled organs    
    from imgmisc import map_replace
    mapping = {lab:domain_labels[jj] + 1 for jj, lab in enumerate(labels)}
    vis_img = map_replace(seg_img, mapping)
# import plotly.graph_objects as go
# import numpy as np
# X, Y, Z = np.indices(vis_img.shape)
# values = vis_img.ravel()
# X, Y, Z = np.mgrid[-1:1:30j, -1:1:30j, -1:1:30j]
# values =    np.sin(np.pi*X) * np.cos(np.pi*Z) * np.sin(np.pi*Y)

# fig = go.Figure(data=go.Volume(
#     x=X.flatten(),
#     y=Y.flatten(),
#     z=Z.flatten(),
#     value=values.flatten(),
#     isomin=0.5,
#     isomax=8,
#     opacity=0.1, # max opacity
#     # opacityscale=[[-0.5, 1], [-0.2, 0], [0.2, 0], [0.5, 1]],
#     surface_count=21,
#     colorscale='RdBu'
#     ))
# fig.show()    
    # tiff.imshow(vis_img)
    # v = pv.wrap(vis_img)
    # v.spacing = resolution
    # v = v.threshold(0.5)
    
    # import pyvista as pv
    # n_colors = len(np.unique(vis_img))
    # p = pv.Plotter()
    # # p.enable_anti_aliasing('ssaa')
    # # p.add_mesh(v, opacity=[0] + [1] * (n_colors-1), cmap='Set1', n_colors=n_colors)
    # p.view_yz()
    # p.set_background('white')
    # p.show()    
    
    vis_img = vis_img.astype(float)
    vis_img = vis_img/np.max(vis_img) * 255
    vis_img = vis_img.astype(np.uint8)
    
    p = pv.Plotter()
    p.add_volume(vis_img, opacity=[0] + [1] * 255, 
                  clim=[0, 254], cmap='tab10', n_colors=255, 
                  shade=True)
    p.set_background('white')
    p.show()
    p.close()
    import gc; gc.collect()
    
# pv.plot()

# trans_files = listdir(TRANS_DIR, sorting='natural', include='.mat')

# def collate_registered_cell_data(seg_files, cell_data_files, trans_files):
#     # Find meristem that's best fitted by the other meristems
#     fixed = [re.findall('f_(plant_\d+)', ff)[0] for ff in trans_files]
#     moving = [re.findall('m_(plant_\d+)', ff)[0] for ff in trans_files]
#     flips = [re.findall('flip_(\D+)-', ff)[0] for ff in trans_files]
#     rots = [float(re.findall('rot_(\d+\.\d+)', ff)[0]) for ff in trans_files]
#     mi = [float(re.findall('mi_(\d+\.\d+)', ff)[0]) for ff in trans_files]
#     data = pd.DataFrame(data={'fixed':fixed, 'moving':moving, 'flips':flips, 'rots':rots, 'mi':mi})
#     best_fixed = data.groupby(['fixed', 'moving']).max().groupby('fixed').sum()['mi'].idxmax()
#     best_matches = data.iloc[data.loc[data['fixed'] == best_fixed].groupby('moving')['mi'].idxmax()]
#     best_idx = best_matches.index
    
#     ffile = [sf for sf in seg_files if best_fixed + '-' in sf][0]
#     fres = get_resolution(ffile)
#     fdata = tiff.imread(ffile).astype(float)
#     fdata = ants.from_numpy(fdata, spacing=fres)
    
#     regp = pd.read_csv([ff for ff in cell_data_files if best_fixed + '-' in ff][0], sep='\t')
#     dfs = [regp]
#     for ff, idx in enumerate(best_idx):
#         tf = ants.read_transform(trans_files[idx])
#         mfile = [sf for sf in seg_files if best_matches['moving'][idx] + '-' in sf][0]
#         mres = get_resolution(mfile)
        
#         # Read in the moving image and apply the corresponding 'best' transform
#         mdata = tiff.imread(mfile) # .astype(float)
#         mdata = mdata[:, ::-1, :] if data.iloc[idx]['flips'] == 'flip' else mdata
#         mdata = rotate(mdata, data.iloc[idx]['rots'], axes=(1, 2), reshape=False, order=0).astype(float)
#         mdata = ants.from_numpy(mdata, spacing=mres)
#         mdata = tf.apply_to_image(mdata, interpolation='nearestneighbor')
#         mdata = mdata.numpy()
#         mdata = mdata.astype(np.uint16)
        
#         # Compute the new centroids for the transformed data
#         regp = regionprops(mdata) 
#         post_centroids = np.array([list(rp.centroid) for rp in regp]) * mres
#         tf_labels = [rr.label for rr in regp]
        
#         # Read in the cell geometry data and assign the new centroids to each label
#         mfile = [sf for sf in cell_data_files if best_matches['moving'][idx] + '-' in sf][0]
#         df = pd.read_csv(mfile, sep='\t')    
#         df = df.loc[df['label'].isin(tf_labels)]
#         for ii, lab in enumerate(tf_labels):
#             df.loc[df['label'] == lab, ['z', 'y', 'x']] = post_centroids[ii]
#         dfs.append(df)
#     df = pd.concat(dfs, ignore_index=True)
#     return df
# df = collate_registered_cell_data(seg_files, data_files, trans_files)
# df.to_csv(f'{OUTPUT_DIR}/registered_cell_data.csv', sep='\t')
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import numpy as np
import os
import numpy as np
import pyvista as pv
import tifffile as tiff
from imgmisc import listdir, mkdir
from imgmisc import get_resolution
from multiprocessing import Pool, cpu_count
import re
import ants
from scipy.ndimage import rotate
import pyvista as pv
from imgmisc import create_mesh
# import pyvistaqt as pvq
import shutil
import pandas as pd
 
import os
import copy
import numpy as np
import pandas as pd
import tifffile as tiff
from skimage.morphology import ball
from skimage.measure import regionprops
from skimage.segmentation import relabel_sequential

from imgmisc import listdir, to_uint8, autocrop, cut, get_resolution, mkdir
from imgmisc import binary_extract_largest

from img2org.quantify import find_neighbors
from img2org.quantify import get_l1
from img2org.quantify import get_wall
from img2org.quantify import area_between_walls
from img2org.quantify import set_background
from img2org.conversion import create_organism_files

import sys
from multiprocessing import Pool, cpu_count
import pickle
import pandas as pd
import re
from img2org.conversion import read_neigh_init, read_init
from img2org.quantify import get_layers
from imgmisc import get_resolution
from skimage.measure import marching_cubes
import pyvista as pv
from phenotastic.mesh import create_cellular_mesh
from phenotastic.mesh import label_cellular_mesh
from imgmisc import weighted_average_points
import gc
import matplotlib 
import matplotlib.pyplot as plt
from imgmisc import natural_sort
import seaborn as sns

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'
NROTS = 8

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
SEG_DIR = f'{PROJECT_DIR}/data/{dataset}'
# SIM_DIR = f'{PROJECT_DIR}/data/{dataset}/quantified/end_state'
SIM_DIR = f'{PROJECT_DIR}/data/{dataset}/parameter_sweep2'
CELL_DATA_DIR = f'{PROJECT_DIR}/data/{dataset}/cell_data'
TRANS_DIR = f'{PROJECT_DIR}/data/{dataset}/registration'
plant_type = 'WT'
wall_depth = 1

init_PIN_index = 4
neigh_PIN_index = 1

OUTPUT_DIR = f'{PROJECT_DIR}/figures/cell_properties'
mkdir(OUTPUT_DIR)

seg_files = listdir(SEG_DIR, sorting='natural', include='-refined.tif')
sim_files = listdir(SIM_DIR, include='cells_end_state.dat')
# sim_files = listdir(SIM_DIR, include='pe_-1-pi_-1-prod_1-cells_end_state.dat')
sim_files = listdir(SIM_DIR, include='cells_end_state.dat')
data_files = listdir(CELL_DATA_DIR, sorting='natural', include='cell_data.csv')
trans_files = listdir(TRANS_DIR, sorting='natural', include='.mat')

def read_simulation_output(ff):
    sdf = pd.DataFrame(pd.read_csv(ff, sep='\t', header=None))
    # print(ff)
    if len(sdf.columns) == 14:
        sdf.insert(loc=9, column='in_bottom', value=[0] * len(sdf))
    if len(sdf.columns) == 15:
        
        
    # sdf.columns = ['z', 'y', 'x', 'vol', 'PIN', 'AUXLAX', 'auxin', 'in_l1', 'in_l2', 'flux_sum', 'flux_l1l1', 'flux_l1l2', 'flux_l2l2', 'flux_l2l1']
    sdf.columns = ['z', 'y', 'x', 'vol', 'PIN', 'AUXLAX', 'auxin', 'in_l1', 'in_l2', 'in_bottom', 'flux_sum', 'flux_l1l1', 'flux_l1l2', 'flux_l2l2', 'flux_l2l1']
    
    sdf['label'] = np.arange(len(sdf)) + 1
    sdf['plant'] = [int(re.findall('plant_(\d+)', ff)[0])] * len(sdf)
    sdf['pe'] = [int(re.findall('pe_(-?\d*\.{0,1}\d+)', ff)[0])] * len(sdf)
    sdf['pi'] = [int(re.findall('pi_(-?\d*\.{0,1}\d+)', ff)[0])] * len(sdf)
    sdf['prod'] = [int(re.findall('prod_(\d+)', ff)[0])] * len(sdf)
    return sdf
for ff in sim_files:
    sdf = pd.DataFrame(pd.read_csv(ff, sep='\t', header=None))
    # print(ff)
    if len(sdf.columns) == 14:
        sdf.insert(loc=9, column='in_bottom', value=[0] * len(sdf))
    if len(sdf.columns) == 16:
        break


sdfs = [read_simulation_output(ff) for ff in sim_files]

sdf = pd.concat(sdfs, ignore_index=True)
sdf = sdf.sort_values(['plant', 'label', 'prod'])
# sdf = sdf.loc[(sdf['pe'] == 0) & 
#             (sdf['pi'] == 0) &
#             (sdf['prod'] == 1)]
df = pd.read_csv(f'{PROJECT_DIR}/data/{dataset}/registered_cell_data.csv', sep='\t')
df = df.merge(sdf, on=['plant', 'label']) # merge with the simulation output data

plants = np.unique([re.findall('plant_\d+', ff)[0] for ff in data_files])
plants = natural_sort(plants)
plant = plants[0]

# Filter
# df = df.loc[df['layer'] == 'L1'] 
df = df.loc[df['vol'] < 400]
df = df.loc[(df['l1_surface_areas'] < 1500) | np.isnan(df['l1_surface_areas'])]
df = df.loc[df['solidity'] > .75]
df = df.loc[df['sphericity'] > .6]
df = df.loc[df['pca_minor_axis_length'] < 3]
df = df.loc[df['pca_major_axis_length'] < 15]
df = df.loc[df['n_neighs'] < 18]
df['majmin_ratio'] = df['pca_major_axis_length'] / df['pca_minor_axis_length']
df = df.loc[df['n_l2neighs'] < 12]
df = df.loc[(df['n_l1neighs'] < 10) | (df['n_l1neighs'] == 0)]
df = df.loc[df['bbox_volume'] > .7]

df['z'] = df['z_x']
df['y'] = df['y_x']
df['x'] = df['x_x']
# df = df.dropna()
# df['maj_sep'] = (df['pca_major_axis_length'] > 4.2).astype(float)
# df.loc[df['in_l1'] == 0, 'maj_sep'] = 0
cols = ['convex_volume', 'equivalent_diameter', 'l1_surface_areas', 
        'max_inscribed_sphere_radius', 'n_l1neighs', 'n_l2neighs', 'n_neighs', 
        'pca_major_axis_length', 'pca_medial_axis_length', 'pca_minor_axis_length', 
        'solidity', 'sphericity', 'surface_area', 'volume', 'majmin_ratio',
        'PIN', 'AUXLAX', 'auxin', 'flux_sum', 'flux_l1l1', 'flux_l1l2', 'flux_l2l1', 'flux_l2l2']
# cols = ['auxin']

# cols = ['PIN']
# for col in cols:
#     plt.figure()
#     sns.violinplot(data=df, y=col)

norm = lambda x: (x - x.mean()) / x.std()
df[cols] = df.groupby('plant').transform(norm)[cols]
for col in cols:
    df[col] = norm(df[col])
# df[cols] = norm(df[cols])
col = 'auxin'

r = 12.5
iters = 1
scalar_weights=False
point_size = 30
for col in cols:
    print(col)
    coldf = df.loc[~np.isnan(df[col])]
    coldf = coldf.loc[coldf.in_bottom == 0]

    points = coldf[['z', 'y', 'x']].values
    scalars = coldf[col].values
    not_na = ~np.array([any(np.isnan(x)) for x in points])
    pts1, smooth_scalars = weighted_average_points(points[not_na], scalars[not_na], iters, r=r, weights='linear', scalar_weights=scalar_weights)
    
    p = pv.Plotter(off_screen=False, notebook=False)
    # p = pv.Plotter(off_screen=True, notebook=False)
    # p.add_points(pts1, scalars=scalars[not_na], cmap='turbo', show_scalar_bar=True, render_points_as_spheres=True, 
    #                 point_size=point_size, opacity=1)
    # p.add_points(pts1, scalars=coldf['in_bottom'].values[not_na], cmap='turbo', show_scalar_bar=True, render_points_as_spheres=True, 
    #               point_size=point_size, opacity=1)
    p.add_points(pts1, scalars=smooth_scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, 
                  point_size=point_size, opacity=1)
    # p.add_points(pts1, scalars=smooth_scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, 
    #               point_size=point_size, opacity=1, clim=[np.quantile(smooth_scalars, .005), np.quantile(smooth_scalars, .995)])
    p.view_yz()
    p.set_background('white')
    p.show()
    # p.screenshot(f'{OUTPUT_DIR}/{dataset}-collated-{col}_top.png', 
    #              transparent_background=True, window_size=[2000, 2000])
    # p.close()
    del p
    gc.collect()

# Internal plots
for col in cols:
    print(col)
    coldf = df.loc[~np.isnan(df[col])]
    # coldf = coldf.loc[coldf['in_bottom'] == 0]
    # coldf = coldf.loc[(coldf['in_l1'] == 0)]
    points = coldf[['z', 'y', 'x']].values
    scalars = coldf[col].values
    not_na = ~np.array([any(np.isnan(x)) for x in points])
    pts1, scalars = weighted_average_points(points[not_na], scalars[not_na], iters, r=r, weights='linear', scalar_weights=scalar_weights)
    
    # offset = -7.5
    filter1 = pts1[:,1] < coldf['y'].mean()
    filter2 = pts1[:,1] > coldf['y'].mean() - 10
    filter_ = filter1 & filter2
    scalars = scalars[filter_]
    pts1 = pts1[filter_]
    
    # scalars = scalars[pts1[:,1] < coldf['centroid-1'].mean() - offset]
    # pts1 = pts1[pts1[:,1] < coldf['centroid-1'].mean() - offset]

    # p = pv.Plotter(off_screen=False, notebook=False)
    # p.add_points(pts1, scalars=scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, point_size=point_size, opacity=1, clim=[np.quantile(scalars, .005), np.quantile(scalars, .995)])
    # p.view_xz(True)
    # p.set_background('white')
    # p.show()

    
    # p = pv.Plotter(off_screen=True, notebook=False)
    p = pv.Plotter(off_screen=False)
    # p.add_points(pts1, scalars=scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, point_size=70, opacity=1, clim=[np.quantile(scalars, .01), np.quantile(scalars, .99)])
    p.add_points(pts1, scalars=scalars, cmap='turbo', show_scalar_bar=True, render_points_as_spheres=True, 
                 point_size=point_size, opacity=1, clim=[np.quantile(scalars, .005), np.quantile(scalars, .995)])
    p.view_xz(True)
    # p.view_xy(False)
    p.set_background('white')
    p.show()
    # p.screenshot(f'{OUTPUT_DIR}/{dataset}-collated-{col}_side.png', 
    #              transparent_background=True, window_size=[2000, 2000])
    # p.close()
    # del p
    # gc.collect()

for layer in ['L1', 'L2', 'L3']:
    for col in cols:
        print(col)
        coldf = df.loc[~np.isnan(df[col])]
        coldf = coldf.loc[coldf['layer'] == layer]
        # coldf = coldf.loc[coldf['centroid-1'] < coldf['centroid-1'].mean()]
        points = coldf[['centroid-0', 'centroid-1', 'centroid-2']].values
        scalars = coldf[col].values
        pts1, scalars = weighted_average_points(points, scalars, iters, r=r, weights='linear', scalar_weights=scalar_weights)
        
        p = pv.Plotter(off_screen=True, notebook=False)
        # p.add_points(pts1, scalars=scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, point_size=70, opacity=1, clim=[np.quantile(scalars, .01), np.quantile(scalars, .99)])
        p.add_points(pts1, scalars=scalars, cmap='turbo', show_scalar_bar=False, render_points_as_spheres=True, point_size=point_size, opacity=1, clim=[np.quantile(scalars, .005), np.quantile(scalars, .995)])
        p.view_yz()
        p.set_background('white')
        p.screenshot(f'{OUTPUT_DIR}/{dataset}-collated-{col}_{layer}_top.png', 
                     transparent_background=True, window_size=[2000, 2000])
        p.close()
        del p
        gc.collect()
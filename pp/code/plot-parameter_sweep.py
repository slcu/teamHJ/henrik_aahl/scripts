#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:27:26 2020

@author: henrik
"""
import os
import numpy as np
import pyvista as pv
from img2org.conversion import read_init
from imgmisc import listdir, mkdir
from multiprocessing import Pool, cpu_count
import gc
import re
import pandas as pd
pv.set_plot_theme('document') 

from xvfbwrapper import Xvfb
vdisplay = Xvfb(width=1920, height=1080)
vdisplay.start()

dataset = '220711-PIN1GFP_MYRYFP-WT-timepoint_0'
plant_type = 'WT'
wall_depth = 1
AUXIN_index = 6
clim = [0, 1]

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
SEG_DIR = f'{PROJECT_DIR}/data/{dataset}'
DATA_DIR = f'{PROJECT_DIR}/data/{dataset}/parameter_sweep2'
OUTPUT_DIR = f'{PROJECT_DIR}/figures'
TOP_DIR = f'{OUTPUT_DIR}/top'
#CROPTOP_DIR = f'{OUTPUT_DIR}/crop_top'
#CROPSIDE1_DIR = f'{OUTPUT_DIR}/crop_side1'
#CROPSIDE2_DIR = f'{OUTPUT_DIR}/crop_side2'
mkdir(TOP_DIR)
#mkdir(CROPTOP_DIR)
#mkdir(CROPSIDE1_DIR)
#mkdir(CROPSIDE2_DIR)

#seg_files = listdir(SEG_DIR, sorting='natural', include='.tif')
data_files = listdir(DATA_DIR, include='cells_end_state.dat', sorting='natural')

def _infostr(fname):
    try:
        output = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)(?:h|M)-\S+-', fname)[0]
    except:
        try:
            output = re.findall('(\S+)-(\S+)-(\S+)-plant_(\d+)', fname)[0]
        except:
            raise Exception(f'Wrong filename format')
    return output

# get all dataset, genotype, plant, time, pe, pi, prod
# get registered coords
# limit to certain prod
# merge info from individual to file

file_index = 0
data_file_index = 0
def plot_file(file_index):
    #dataset, genotype, plant = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', seg_files[file_index])[0]
    dataset, reporters, genotype, plant, time = _infostr(seg_files[file_index)
    data_files_plant = [ff for ff in data_files if f'{dataset}-{reporters}-{genotype}-plant_{plant}-{time}h' in ff]

#    print(data_files_plant)

    # TODO start from here
    
#    mesh = pv.read(full_files[file_index])
#    mesh_topcrop = pv.read(topcrop_files[file_index])
#    mesh_sidecrop1 = pv.read(sidecrop1_files[file_index])
#    mesh_sidecrop2 = pv.read(sidecrop2_files[file_index])
#    mesh_sidecrop1.rotate_y(-90)
#    mesh_sidecrop2.rotate_z(90)
#
#    for data_file_index in range(len(data_files_plant)):
#        dfile = data_files_plant[data_file_index]
#        print(f'plotting {dfile}')
#        output_data = pd.read_csv(dfile, header=None, sep='\t')
#        output_auxin = output_data[AUXIN_index].values 
#        output_auxin = output_auxin / np.max(output_auxin)
/#        
#        # Assign the values to the corresponding cells
#        mesh['values'] = np.zeros(mesh.n_points)
#        mesh_topcrop['values'] = np.zeros(mesh_topcrop.n_points)
#        mesh_sidecrop1['values'] = np.zeros(mesh_sidecrop1.n_points)
#        mesh_sidecrop2['values'] = np.zeros(mesh_sidecrop2.n_points)
#        for ii in np.unique(mesh['cell_id']):
#            mesh['values'][mesh['cell_id'] == ii] = output_auxin[ii - 1]
#        for ii in np.unique(mesh_topcrop['cell_id']):
#            mesh_topcrop['values'][mesh_topcrop['cell_id'] == ii] = output_auxin[ii - 1]
#        for ii in np.unique(mesh_sidecrop1['cell_id']):
#            mesh_sidecrop1['values'][mesh_sidecrop1['cell_id'] == ii] = output_auxin[ii - 1]
#        for ii in np.unique(mesh_sidecrop2['cell_id']):
#            mesh_sidecrop2['values'][mesh_sidecrop2['cell_id'] == ii] = output_auxin[ii - 1]
            
        ### Plot the various perspectives
        # Top
        p = pv.Plotter(off_screen=True, notebook=False)
        p.add_mesh(mesh, cmap='turbo', scalars='values', show_scalar_bar=False, clim=clim)
        p.view_yz()
        p.set_background('white')
        p.screenshot(f'{TOP_DIR}/{os.path.splitext(os.path.basename(dfile))[0]}-auxin_concentrations_full.png', 
                      transparent_background=True, window_size=[2000, 2000])
        p.close()
        del p
        gc.collect()

#        # Croptop
#        p = pv.Plotter(off_screen=True, notebook=False)
#        p.add_mesh(mesh_topcrop, cmap='turbo', scalars='values', show_scalar_bar=False, clim=clim)
#        p.view_yz()
#        p.set_background('white')
#        p.screenshot(f'{CROPTOP_DIR}/{os.path.splitext(os.path.basename(dfile))[0]}-auxin_concentrations_topcrop.png', 
#                      transparent_background=True, window_size=[2000, 2000])
#        p.close()
#        del p
#        gc.collect()
#
#        # Cropside1
#        p = pv.Plotter(off_screen=True, notebook=False)
#        p.add_mesh(mesh_sidecrop1, cmap='turbo', scalars='values', show_scalar_bar=False, clim=clim)
#        p.view_xz(True)
#        p.set_background('white')
#        p.screenshot(f'{CROPSIDE1_DIR}/{os.path.splitext(os.path.basename(dfile))[0]}-auxin_concentrations_cropside1.png', 
#                      transparent_background=True, window_size=[2000, 2000])
#        p.close()
#        del p
#        gc.collect()
#        
#        # Cropside2
#        p = pv.Plotter(off_screen=True, notebook=False)
#        p.add_mesh(mesh_sidecrop2, cmap='turbo', scalars='values', show_scalar_bar=False, clim=clim)
#        p.view_xy()
#        p.set_background('white')
#        p.screenshot(f'{CROPSIDE2_DIR}/{os.path.splitext(os.path.basename(dfile))[0]}-auxin_concentrations_cropside2.png', 
#                      transparent_background=True, window_size=[2000, 2000])
#        p.close()
#        del p
#        gc.collect()

import sys
if len(sys.argv) < 2:
    n_cores = cpu_count() - 1
else:
    n_cores = int(sys.argv[1])

p = Pool(n_cores)
p.map(plot_file, np.arange(len(seg_files)))
p.close()
p.join()    
vdisplay.stop()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 30 16:46:53 2022

@author: henrikahl
"""

# Calculate cell properties per organ stage
# Calculate auxin abundance / average auxin abundance per organ
# Calculate PIN abundance / average auxin abundance per organ

from imgmisc import listdir
import pandas as pd
import pyvista as pv
from img2org.conversion import read_init
import os
import re
import seaborn as sns
import matplotlib.pyplot as plt
bname = lambda x: os.path.basename(os.path.splitext(x)[0])

# pd.read_csv('/home/henrikahl/projects/svmeristem/data/221114-pUBQ10-WT-Timelapse_4h/tracking/221122-pUBQ10-WT-plant_2-0h_to_4h-tracking.csv',
#             sep='\t')


init_files = listdir('/home/henrikahl/projects/pp/data/220711-PIN1GFP_MYRYFP-WT-timepoint_0/quantified', include='.init', sorting='natural')
auxin_files = listdir('/home/henrikahl/projects/pp/data/220711-PIN1GFP_MYRYFP-WT-timepoint_0/parameter_sweep2/', 
                      include='pe_0-pi_0-prod_1-cells_end_state.dat', sorting='natural')
cd_files = listdir('/home/henrikahl/projects/pp/data/220711-PIN1GFP_MYRYFP-WT-timepoint_0/cell_data', 
                      include='.csv', sorting='natural')
iter_ = 0
fname = init_files[iter_]

idfs = []
for fname in init_files:
    dataset, reporters, genotype, plant, time = re.findall(
        '(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', bname(fname))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)
    df = read_init(fname)
    df['dataset'] = dataset
    df['plant'] = plant
    df['time'] = time
    df['reporters'] = reporters
    df['genotype'] = genotype
    df['label'] = range(1, len(df)+1)
    idfs.append(df)
adfs = []
for fname in auxin_files:
    dataset, reporters, genotype, plant, time = re.findall(
        '(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', bname(fname))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)
    df = pd.read_csv(fname, sep='\t', header=None)
    df['dataset'] = dataset
    df['plant'] = plant
    df['time'] = time
    df['reporters'] = reporters
    df['genotype'] = genotype
    df['label'] = range(1, len(df)+1)
    adfs.append(df)
cdfs = []
for fname in cd_files:
    dataset, reporters, genotype, plant, time = re.findall(
        '(\S+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h', bname(fname))[0]
    dataset, plant, time = int(dataset), int(plant), int(time)
    df = pd.read_csv(fname, sep='\t', header=0)
    df['dataset'] = dataset
    df['plant'] = plant
    df['time'] = time
    df['reporters'] = reporters
    df['genotype'] = genotype
    cdfs.append(df)
    
df = pd.concat(idfs, ignore_index=True)
df2 = pd.concat(adfs, ignore_index=True)
df3 = pd.concat(cdfs, ignore_index=True)

df.columns = ['centroid-0','centroid-1','centroid-2', 'volume', 'pin', 'aux', 'auxin', 'l1', 'l2', 'bottom', 'organ', 
              'dataset', 'plant', 'time', 'reporters', 'genotype', 'label']
df2.columns = ['centroid-0','centroid-1','centroid-2', 'volume', 'pin', 'aux', 'auxin', 'l1', 'l2', 'bottom', 'fluxes', 'l1l1', 'l1l2', 'l2l2', 'l2l1',
              'dataset', 'plant', 'time', 'reporters', 'genotype', 'label']
df['auxin'] = df2['auxin']
df['fluxes'] = df2['fluxes']
df['l1l1'] = df2['l1l1']
df['l1l2'] = df2['l1l2']
df = df.merge(df3, on=['dataset', 'plant', 'time', 'reporters', 'genotype', 'label'])
df['volume'] = df['volume_x']

# df['layer'] = ['L1' if df.iloc[ii]['l1'] == 1 else 'L2' if df.iloc[ii]['l2'] == 1 else 'L3' for ii in range(len(df))]

# df = df.loc[(df.l2 != 1) & (df.l1 != 1)]
df['meanpin'] = df.groupby(['dataset', 'organ', 'plant']).transform(lambda x: x.mean())['pin']
df['meanauxin'] = df.groupby(['dataset', 'organ', 'plant']).transform(lambda x: x.mean())['auxin']
df['meanvol'] = df.groupby(['dataset', 'organ', 'plant']).transform(lambda x: x.mean())['volume']
df['meanfluxes'] = df.groupby(['dataset', 'organ', 'plant']).transform(lambda x: x.mean())['fluxes']
df['layermeanpin'] = df.groupby(['dataset', 'organ', 'plant', 'layer']).transform(lambda x: x.mean())['pin']
df['layermeanvol'] = df.groupby(['dataset', 'organ', 'plant', 'layer']).transform(lambda x: x.mean())['volume']
df['layermeanauxin'] = df.groupby(['dataset', 'organ', 'plant', 'layer']).transform(lambda x: x.mean())['auxin']
df['layermeanfluxes'] = df.groupby(['dataset', 'organ', 'plant', 'layer']).transform(lambda x: x.mean())['fluxes']
df['organ'] = df['organ'].astype(int)
df = df.loc[df.organ < 8]
df['eccentricity'] = df['pca_major_axis_length'] /df['pca_minor_axis_length']
df['meansphericity'] = df.groupby(['dataset', 'organ', 'plant']).transform(lambda x: x.mean())['sphericity']

df = df.loc[df.volume< 400]
df = df.loc[df.sphericity > .5]
df = df.loc[df.solidity > .5]
df = df.loc[df.eccentricity < 12]

###############################################################################    
# Organ - full
###############################################################################    
sns.set(font_scale=3)
sns.set_style('ticks')
plt.figure(figsize=(14, 12))
# plt.savefig(f'{OUTPUT_DIR}/meristems-curvature_vs_size.svg')
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanvol',
                palette='Set2', showfliers=False)
ax = sns.scatterplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanvol', hue='organ',
                palette='Set2', linewidths=2, edgecolor='black', ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean cell volume')
plt.xlabel('Organ index')
plt.tight_layout()
# plt.subplots_adjust(left=.1, right=0.9)
ax.legend(loc='center right', ncol=1, frameon=False,
            title='Organ', bbox_to_anchor=(1.55, .5))
plt.savefig('/home/henrikahl/projects/pp/figures/organ_cell_data/mean_volume.svg')

plt.figure(figsize=(14, 12))
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanpin',
                palette='Set2', fliersize=0)
sns.scatterplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanpin', hue='organ',
                palette='Set2', linewidths=1, edgecolor='black', ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean cell PIN')
plt.xlabel('Organ index')
plt.tight_layout()
# plt.subplots_adjust(left=.1, right=0.9)
ax.legend(loc='center right', ncol=1, frameon=False,
            title='Organ', bbox_to_anchor=(1.55, .5))
plt.savefig('/home/henrikahl/projects/pp/figures/organ_cell_data/mean_pin.svg')

plt.figure(figsize=(14, 12))
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanauxin',
                palette='Set2', fliersize=0)
sns.scatterplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanauxin', hue='organ',
                palette='Set2', linewidths=1, edgecolor='black', ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean cell auxin')
plt.xlabel('Organ index')
plt.tight_layout()
# plt.subplots_adjust(left=.1, right=0.9)
ax.legend(loc='center right', ncol=1, frameon=False,
            title='Organ', bbox_to_anchor=(1.55, .5))
plt.savefig('/home/henrikahl/projects/pp/figures/organ_cell_data/mean_auxin.svg')

plt.figure(figsize=(14, 12))
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanfluxes',
                palette='Set2', fliersize=0)
sns.scatterplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meanfluxes', hue='organ',
                palette='Set2', linewidths=1, edgecolor='black', ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean cell auxin fluxes')
plt.xlabel('Organ index')
plt.tight_layout()
# plt.subplots_adjust(left=.1, right=0.9)
ax.legend(loc='center right', ncol=1, frameon=False,
            title='Organ', bbox_to_anchor=(1.55, .5))
plt.savefig('/home/henrikahl/projects/pp/figures/organ_cell_data/mean_auxin_fluxes.svg')

plt.figure(figsize=(14, 12))
plt.ticklabel_format(axis="y", style="sci", scilimits=(-0, 0))
plt.ticklabel_format(axis="x", style="sci", scilimits=(-0, 0))
ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meansphericity',
                palette='Set2', fliersize=0)
sns.scatterplot(data=df.drop_duplicates(['dataset', 'plant', 'organ']), x='organ', y='meansphericity', hue='organ',
                palette='Set2', linewidths=1, edgecolor='black', ax=ax)
ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)
plt.ylabel('Mean cell sphericity')
plt.xlabel('Organ index')
plt.tight_layout()
# plt.subplots_adjust(left=.1, right=0.9)
ax.legend(loc='center right', ncol=1, frameon=False,
            title='Organ', bbox_to_anchor=(1.55, .5))
plt.savefig('/home/henrikahl/projects/pp/figures/organ_cell_data/mean_sphericity.svg')

df.loc[df.layer == 'L4+', 'layer'] = 'L3'
###############################################################################    
# Organ - layers
###############################################################################    
sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanvol',
                palette='Set2', hue='layer')
plt.ylabel('Mean cell volume')
plt.xlabel('Organ index')

sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanpin',
                palette='Set2', hue='layer')
plt.ylabel('Mean cell PIN')
plt.xlabel('Organ index')

sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanauxin',
                palette='Set2', hue='layer')
plt.ylabel('Mean cell auxin')
plt.xlabel('Organ index')

ax = sns.boxplot(data=df.drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanfluxes',
                palette='Set2', hue='layer')
# sns.regplot(data=df.loc[df.layer == 'L1'].drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanfluxes',
#             lowess=True, ax=ax)
# sns.regplot(data=df.loc[df.layer == 'L2'].drop_duplicates(['dataset', 'plant', 'organ', 'layer']), x='organ', y='layermeanfluxes',
#             lowess=True, ax=ax)
plt.ylabel('Mean cell auxin fluxes')
plt.xlabel('Organ index')


# sns.violinplot(data=df.loc[df.l1 == 1], x='organ', y='meanpin')




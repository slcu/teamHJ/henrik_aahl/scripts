#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 09:57:49 2020

@author: henrik
"""
import math
import os
import matplotlib
import matplotlib.pyplot as plt
from imgmisc import listdir, autocrop, mkdir, rand_cmap
import numpy as np
from PIL import Image, ImageChops

matplotlib.use('Agg')

wall_depth = 1
# plant_type = 'mutants'
# dataset = '131007-PIN_GFP-acyl_YFP-Timelapse_4h-WT'

# Input parameters
PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pp')
INPUT_DIR = f'/home/henrikahl/projects/pp/figures/parameter_sweep/top'

# files = listdir(, 
                # include='prod_6-cells_end_state.dat')
OUTPUT_DIR = f'/home/henrikahl/projects/pp/figures'
# OUTPUT_DIR = f'{PROJECT_DIR}/figures/parameter_sweep_active/collated/{dataset}/{plant_type}/wall_depth_{wall_depth}/concentrations'

files = listdir(INPUT_DIR, include='.png', sorting='natural')
result_figsize_resolution = 40 # 1 = 100px
mkdir(OUTPUT_DIR)

import re
from imgmisc import autocrop_2d
import gc
# plants = np.unique([re.findall('plant_\d+', ff)[0] for ff in files])
# crops = np.unique([ff.split('_')[-1].split('.png')[0] for ff in files])
# mutants = np.unique([re.findall(f'wall_depth_{wall_depth}-(\S+)-pe_', ff)[0] for ff in files])
# mutants = [mm for mm in mutants if 'uniform_auxin' in mm]

# plant = plants[0]
# crop = crops[0]
# prod = 'prod_1'
# for plant in plants:
#     for crop in crops:
#         for mutant in mutants:
    
# for ii in range(5):
#     [print(ii) for ii in range(3)]    
#     print(0
#     print(ii)
    
for prod in ['prod_6']:#, "prod_1"]:#, 'prod_6']:
    # print(f'Running {plant}')
    plant_singles_files = [fname for fname in files if prod in fname]
    if len(plant_singles_files) == 0:
        continue
    
    grid_size = 7 #math.ceil(math.sqrt(len(plant_singles_files)))
    fig, axes = plt.subplots(grid_size, grid_size, figsize=(result_figsize_resolution, result_figsize_resolution), sharex=True, sharey=True)
    
    # set labels
    labels = np.arange(-3,4)

    current_file_number = 0
    # image_filename = plant_singles_files[0]
    for ii in range(len(plant_singles_files)):
        image_filename = plant_singles_files[ii]
        # TODO double check that this is right
        y_position = int(re.findall('-pe_-?\d+', image_filename)[0].split('_')[1]) + 3 
        x_position = int(re.findall('-pi_-?\d+', image_filename)[0].split('_')[1]) + 3
        
        img = Image.open(plant_singles_files[current_file_number])
        img = np.asarray(autocrop_2d(img))
        axes[x_position, y_position].axis('off')
        axes[x_position, y_position].imshow(img)
            
        print(f'{current_file_number + 1}/{len(plant_singles_files)} : {image_filename}')
    
        current_file_number += 1
    fig.tight_layout()
    fig.add_subplot(111, frameon=False)
    
    plt.tick_params(labelcolor='black', top=False, bottom=True, left=True, right=False)
    plt.xticks(1/7/2 + np.arange(0, 1 - 1/7/2, 1/7), labels=labels, fontsize=50)
    # plt.tick_params(axis='x', which='major', pad=-50)
    
    plt.yticks(1/7/2 + np.arange(0, 1 - 1/7/2, 1/7), labels=labels[::-1], fontsize=50, rotation='vertical')
    plt.xlabel("Influx (AUX/LAX) sensitivity", fontsize=80)
    plt.ylabel("Efflux (PIN) sensitivity", fontsize=80)
    fig.tight_layout()
    
    # outname = os.path.basename(plant_singles_files[0][:plant_singles_files[0].find('-pe_')]) + f'-{prod}' + f'-{crop}.png'
    plt.subplots_adjust(left=0.05, right=1.0, bottom=0.05, top=1.0, wspace=0, hspace=0)
    plt.savefig(f'{OUTPUT_DIR}/parameter_sweep-{prod}.png')
    plt.close()
    gc.collect()
# vdisplay.stop()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import re
import os
import ants
import shutil
import argparse
import numpy as np
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from imgmisc import get_l1_voxel_mask
from datetime import datetime
from itertools import product
from scipy.ndimage import rotate
from scipy.ndimage import distance_transform_edt
from multiprocessing import Pool
from phenotastic.mesh import fill_beneath
from phenotastic.mesh import fill_contour
from loguru import logger
from imgmisc import parse_input_files
from imgmisc import bname

def l1_distance_mask(data, threshold=None, resolution=[1, 1, 1]):
    '''
    Generate a distance-to-background mask only of the epidermis.

    Parameters
    ----------
    data : np.array 
        Raw intensity data.
    threshold : float, optional
        Distance threshold specifying how deep the L1 goes.
    resolution : list or np.array of length 3, optional
        Image physical resolution. The default is [1, 1, 1].

    Returns
    -------
    mask : np.array
        Distance-to-background mask of the L1.

    '''
    surface = get_l1_voxel_mask(data, mask=data, resolution=resolution)
    dist2surf = distance_transform_edt(~surface, sampling=resolution)
    mask = dist2surf.copy()
    mask[~(data & (dist2surf < threshold))] = 0
    return mask

# Define some parameters
script_desc = """
This script takes input files, a certain number of YX rotations, and generates all 
permutations of registrations accordingly. It generates a binary mask from input 
segmented (binary or labelled), estimates the epidermis, and used this information 
to register the shapes to each other.
"""
parser = argparse.ArgumentParser(description=script_desc)

# Required positional argument
parser.add_argument('--input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    default=f'{os.path.curdir}', help='Output directory. Defaults to the current directorys.')
# Optional argument
parser.add_argument('--l1depth', type=float, default=10, help='The depth in um of the L1.')
parser.add_argument('--rotations', type=int, default=8, help='The number of steps in a full 360 degree rotation. Defaults to 8.')
parser.add_argument('--mirror_symmetry', action='store_false', help='Whether there is mirror symmetry and an XY flip should be tested. Defaults to True.')
parser.add_argument('--self_registration', action='store_true', help='Whether to do self-registration. Defaults to False.')
parser.add_argument('--verbose', action='store_true', help='Verbose flag. Defaults to False.')
parser.add_argument('--skip_done', action='store_true', help='Whether to skip already done files in the output directory. Defaults to False.')
parser.add_argument('--parallel_workers', type=int, default=1, help='The number of parallel workers. Defaults to 1.')

args = parser.parse_args()

# Define some parameters
NROTS = args.rotations
OUTPUT_DIR = args.output
L1_DEPTH = args.l1depth
mkdir(OUTPUT_DIR)

# Identify the file list
files = parse_input_files(args.input, include=['.tif', '.tiff', '.lsm', '.czi'])

# Define the function arguments
all_args = list(product(*[files,
                          files,
                          ['flip', 'noflip'] if args.mirror_symmetry else ['noflip'],
                          np.arange(0, 360, 360 / NROTS)]))

if not args.self_registration:
    all_args = [arg for arg in all_args if arg[0] != arg[1]] 

if not args.verbose:
    logger.disable("")
    
def generate_transformation(input_args):
    logger.info(f'Running argument set {input_args}')
    logger.info(f'{len(listdir(OUTPUT_DIR))} / {len(all_args)} done')
    fixfile, movfile, flip, rot = input_args
    mplant = re.findall('plant_\d+', movfile)[0]
    fplant = re.findall('plant_\d+', fixfile)[0]

    # Load the fixed data
    finfostr = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)', bname(fixfile))[0]
    fdataset, fgenotype, freporter, fplant = finfostr
    fdataset, fplant = int(fdataset), int(fplant)
    minfostr = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)', bname(movfile))[0]
    mdataset, mgenotype, mreporter, mplant = minfostr
    mdataset, mplant = int(mdataset), int(mplant)

    # Break if done
    ostr = f'{OUTPUT_DIR}/{fdataset}-{fgenotype}-{freporter}-f_{fplant}-' + \
        f'{mdataset}-{mgenotype}-{mreporter}-m_{mplant}-flip_{flip}-rot_{rot}'
    if args.skip_done and (ostr in '-'.join(OUTPUT_DIR)):
        logger.info(f'{ostr} already done. Skipping...')
        return

    # Load image resolutions
    fres = get_resolution(fixfile)
    mres = get_resolution(movfile)

    # Load the fixed data and generate a distance mask for the epidermis. This
    # is that we'll use as registration information.
    logger.info(f'{datetime.now().strftime("%H:%M:%S")} - loading {bname(fixfile)}')
    fdata = tiff.imread(fixfile)
    fdata = fdata > 0
    fdata = fill_contour(fdata, True)
    fdata = fill_beneath(fdata)
    fdata = l1_distance_mask(fdata, L1_DEPTH, fres)
    fdata = ants.from_numpy(fdata, spacing=fres)

    # Load the moving data and flip/rotate it
    mdata = tiff.imread(movfile).astype(float)
    mdata = mdata[:, ::-1, ::] if flip == 'flip' else mdata
    mdata = rotate(mdata, rot, axes=(1, 2), reshape=False, order=0)
    mdata = mdata > 0
    mdata = fill_contour(mdata, True)
    mdata = fill_beneath(mdata)
    mdata = l1_distance_mask(mdata, L1_DEPTH, mres)
    mdata = ants.from_numpy(mdata, spacing=mres)

    # Perform the registration and compute the image similarity post registration
    logger.info(f'registering {fixfile}')
    result1 = ants.registration(fixed=fdata, moving=mdata, type_of_transform='Affine',
                                aff_metric='mattes', aff_sampling=128, grad_step=.1)
    result2 = ants.registration(fixed=fdata, moving=mdata, type_of_transform='Affine',
                                aff_metric='mattes', aff_sampling=128, grad_step=.1)
    mutual_information1 = abs(
        ants.image_mutual_information(fdata, result1['warpedmovout']))
    mutual_information2 = abs(
        ants.image_mutual_information(fdata, result2['warpedmovout']))
    mis = [mutual_information1, mutual_information2]
    result = [result1, result2][np.argmax(mis)]
    mutual_information = [mutual_information1,
                          mutual_information2][np.argmax(mis)]

    # Save the transformation matrix
    fout = f'{OUTPUT_DIR}/{ostr}-mi_{abs(mutual_information):.12f}-fwd.mat'
    logger.info(f'printing {fout} fwd')
    shutil.move(result['fwdtransforms'][0], fout)

logger.info(f'Running {len(all_args)} registrations on {args.parallel_workers} cores.')
p = Pool(args.parallel_workers)
p.map(generate_transformation, all_args)
p.close()
p.join()

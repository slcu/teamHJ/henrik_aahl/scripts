#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import argparse
import os
import re
import sys
from multiprocessing import Pool, cpu_count

import czifile as cz
import h5py
import mahotas as mh
import numpy as np
import pyvista as pv
import tifffile as tiff
from skimage.segmentation import relabel_sequential
from skimage.transform import resize

import phenotastic.domains as boa
import phenotastic.mesh as mp
from img2org.quantify import set_background
from imgmisc import (
    binary_extract_largest,
    create_mesh,
    get_resolution,
    listdir,
    mkdir,
    rand_cmap,
    sgroup,
)
from phenotastic.mesh import remesh, remove_tongues, repair_small, smooth_boundary

bname = lambda x: os.path.basename(os.path.splitext(x)[0])

parser = argparse.ArgumentParser(description="")

# Required positional argument
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output", type=str, help="Output directory", default=f"{os.path.curdir}"
)

# Optional argument
parser.add_argument("--background", type=int, default=0)
parser.add_argument("--resolution", type=float, nargs=3, default=None)
parser.add_argument("--plot_input", action="store_true")
parser.add_argument("--plot_curvature", action="store_true")
parser.add_argument("--plot_result", action="store_true")
parser.add_argument("--suffix", type=str, default="-flower_segmentation")
parser.add_argument("--verbose", action="store_true")
parser.add_argument("--skip_done", action="store_true")
parser.add_argument("--parallel_workers", type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting="natural", include=[".tif", ".tiff"])
else:
    files = args.input
mkdir(args.output)

from loguru import logger

def run(fname):

    oname = f"{bname(fname)}{args.suffix}"
    if oname in ''.join(listdir(args.output)):
        logger.info('Done! Breaking...')
        return

    mesh = pv.read(fname)
    
    p = pv.Plotter(off_screen=True)
    p.add_mesh(
        mesh,
        scalars="domains",
        cmap="tab20",
        interpolate_before_map=False,
    )
    p.view_yz()
    p.set_background('white')
    p.screenshot(f'{args.output}/{oname}.png',
                 transparent_background=True, window_size=[2000, 2000])
    p.close()

n_cores = args.parallel_workers
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()

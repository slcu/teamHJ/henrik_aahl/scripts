#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import argparse
import os
from multiprocessing import Pool

import numpy as np
from loguru import logger

from imgmisc import (
    bname,
    listdir,
    mkdir,
    parse_image_input,
    parse_input_files,
    parse_resolution,
)
from phenotastic.mesh import (
    create_mesh,
    remesh,
    remove_tongues,
    repair_small,
    smooth_boundary,
)

script_desc = """Generate a meshed surface from a (binary) contour."""
parser = argparse.ArgumentParser(description=script_desc)

# Required positional argumesnt
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output",
    type=str,
    default=f"{os.path.curdir}",
    help="Output directory. Defaults to the current directory.",
)

# Optional argument

# algorithm parameters
parser.add_argument(
    "--hole_repair_threshold",
    type=float,
    default=100,
    help="The threshold for the number of edges needed in a hole in order to close it. If more than the threshold number, the hole is not closed.",
)
parser.add_argument(
    "--downscaling",
    type=float,
    default=0.025,
    help="How much the mesh should be downscaled by. Improves computation time but makes the mesh much more coarse.",
)
parser.add_argument(
    "--upscaling",
    type=float,
    default=2,
    help="Upscaling factor to be applied after downscaling. Retrieves some resolution which often helps for visualisation and smoothing.",
)
parser.add_argument(
    "--smooth_iter",
    type=int,
    default=200,
    help="Iterations for the laplacian smoothing.",
)
parser.add_argument(
    "--smooth_relax",
    type=float,
    default=0.01,
    help="The relaxation factor for the laplacian smoothing.",
)
parser.add_argument(
    "--tongues_ratio",
    type=float,
    default=3,
    help='The ratio between the shortest path distance between two points and the distance walking along the mesh boundary. Removes "tongues" in the mesh if the ratio is bigger than the threshold.',
)
parser.add_argument(
    "--tongues_radius",
    type=float,
    default=10,
    help="Upper bound for the euclidean distance to consider points within.",
)

parser.add_argument(
    "--resolution",
    type=float,
    nargs=3,
    default=None,
    help="The image resolution in ZYX format unless contained in the image metadata. Defaults to retrieve from the image metadata.",
)
parser.add_argument(
    "--suffix",
    type=str,
    default="-refined",
    help='The output file suffix. Defaults to "-refined"',
)
parser.add_argument(
    "--verbose", action="store_true", help="Verbose printing. Defaults to False."
)
parser.add_argument(
    "--skip_done",
    action="store_true",
    help="Whether to skip done files already found in the output directory. Defaults to False.",
)
parser.add_argument(
    "--parallel_workers",
    type=int,
    default=1,
    help="The number of parallel workers. Defaults to 1.",
)
args = parser.parse_args()

if not args.verbose:
    logger.disable("")

files = parse_input_files(args.input)
mkdir(args.output)

fname = files[0]

def generate_surface(fname):
    basename = f"{bname(fname)}_mesh.vtk"
    if any(
        [
            f"{args.output}/{basename}" in ff
            for ff in listdir(args.output, include=".vtk")
        ]
    ):
        logger.info(f"{basename} done. Skipping...")
        return
    contour = parse_image_input(fname, None)
    resolution = parse_resolution(fname, args.resolution)

    # just to make it so all tissue parts touching the image borders don't end up with big gaps
    contour = np.pad(contour, 1)
    contour = contour[1:]

    mesh = create_mesh(contour, resolution)
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    mesh = repair_small(mesh, args.hole_repair_threshold)
    mesh = remesh(mesh, int(mesh.n_points * args.downscaling), sub=0)

    mesh = remove_tongues(
        mesh,
        radius=args.tongues_radius,
        threshold=args.tongues_ratio,
        hole_edges=args.hole_repair_threshold,
        verbose=args.verbose,
    )
    mesh = mesh.extract_largest().clean()
    mesh = repair_small(mesh, args.hole_repair_threshold)
    mesh = mesh.smooth(args.smooth_iter, args.smooth_relax)
    mesh = remesh(mesh, int(args.upscaling * mesh.n_points))
    mesh = smooth_boundary(mesh, args.smooth_iter, args.smooth_relax)

    mesh.save(f"{args.output}/{basename}")


p = Pool(args.parallel_workers)
p.map(generate_surface, files)
p.close()
p.join()

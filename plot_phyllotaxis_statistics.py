#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 11:29:32 2020

@author: henrikahl
"""

import os
import argparse
import numpy as np
import tifffile as tiff
# from multiprocessing import Pool, cpu_count
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from imgmisc import listdir
from imgmisc import natural_sort

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
# cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ['black', "red"])

from xvfbwrapper import Xvfb
vdisplay = Xvfb(width=1920, height=1080)
vdisplay.start()

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}/phyllotaxis_statistics_figures')

# Optional argument
parser.add_argument('--xres', type=float, default=6.4)
parser.add_argument('--yres', type=float, default=4.8)
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include='.csv')
else:
    files = args.input
mkdir(args.output)

# files = listdir('/media/henrikahl/My Passport1/data/nadia/cropped/contours/phyllotaxis_data', include='.csv')
df = pd.concat((pd.read_csv(f, sep='\t') for f in files), ignore_index=True)
order = natural_sort(np.unique(df['genotype']))

# Plot variability
plt.figure(figsize=(args.xres, args.yres))
ax = sns.boxplot(data=df.drop_duplicates(['genotype', 'plant', 'dataset',
    'reporters']), x='genotype', y='plant_divang_std', order=order)
ax = sns.swarmplot(data=df.drop_duplicates(['genotype', 'plant', 'dataset',
    'reporters']), x='genotype', y='plant_divang_std', edgecolor='black',
    linewidth=1, ax=ax, order=order)
plt.xlabel('Genotype/treatment')
plt.ylabel('Plant divergence angle std.')
plt.savefig(f'{args.output}/phyllotaxis_standard_deviation.png')
plt.close()

# Plot size
plt.figure(figsize=(args.xres, args.yres))
ax = sns.boxplot(data=df.loc[df['organ_index'] == 0], x='genotype',
        y='distance', order=order)
ax = sns.swarmplot(data=df.loc[df['organ_index'] == 0], x='genotype',
        y='distance', edgecolor='black', linewidth=1, ax=ax, order=order)
plt.xlabel('Genotype/treatment')
plt.ylabel('Meristem size')
plt.savefig(f'{args.output}/meristem_size.png')
plt.close()



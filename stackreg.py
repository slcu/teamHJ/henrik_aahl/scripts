import os
import numpy as np
import czifile as cz
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from pystackreg import StackReg
import argparse

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--suffix', type=str, default='-stackreg')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--split_channels', action='store_true')
parser.add_argument('--channel_offset', type=int, default=1)
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    files = args.input
mkdir(args.output)
# print(files)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
print(files)
def run(fname):
    if args.verbose:
        print(f'Now running {fname}')

    try:
        data = tiff.imread(fname)
    except:
        try:
            data = cz.czifile(fname)
        except:
            raise Exception(f'Input file type for {fname} not supported.')
    data = np.squeeze(data)
    if data.ndim < 3:
        raise Exception('Too few channels')
    if data.ndim == 4 and args.channel is not None:
        data = data[:, args.channel]
    if args.resolution is None:
        resolution = np.asarray(get_resolution(fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution
    
    # Run StackReg to correct for moving stage / moving meristems
    pretype = data.dtype
    data = data.astype(float)
    print(f'Running StackReg for file {fname} with shape {data.shape}')
    sr = StackReg(StackReg.RIGID_BODY)
    if data.ndim > 3:
        trsf_mat = sr.register_stack(np.max(data, 1))
        for ii in range(data.shape[1]):
            data[:, ii] = sr.transform_stack(data[:, ii], tmats=trsf_mat)
    else:
        trsf_mat = sr.register_stack(data)
        data = sr.transform_stack(data, tmats=trsf_mat)
    data[data < 0] = 0
    if pretype not in [float, np.float]:
        data[data > np.iinfo(pretype).max] = np.iinfo(pretype).max
    data = data.astype(pretype)
    
    
    # Save output    
    if args.split_channels:
        if data.ndim <= 4: 
            raise Exception(f'Trying to split channels, but input is of shape {data.shape}.' + 
                            ' Channel splitting requires input in format ZCYX.')
        
        for cc in range(data.shape[1]):
            iname = os.path.basename(os.path.splitext(fname)[0]) + f'-C{cc+args.channel_offset}' + f'{args.suffix}.tif'
            iname = f'{args.output}/{iname}'
            tiff.imsave(iname, data=np.expand_dims(data[:, cc], 1), resolution=1/resolution[1:], imagej=True, 
                metadata={'spacing':resolution[0], 'unit':'um'})
    else:
        iname = os.path.basename(os.path.splitext(fname)[0]) + f'{args.suffix}.tif'
        iname = f'{args.output}/{iname}'
        tiff.imwrite(iname, data=np.expand_dims(data, 1) if data.ndim == 3 else data, resolution=1/resolution[1:], imagej=True, 
                    metadata={'spacing':resolution[0], 'unit':'um'})
    del data
            
from multiprocessing import Pool
p = Pool(args.parallel_workers)
output = p.map(run, files)
p.close()
p.join()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 14:13:51 2020

@author: henrikahl
"""
import os
import psf
import numpy as np
import argparse
import czifile as cz
import tifffile as tiff
from imgmisc import listdir
from imgmisc import mkdir
from imgmisc import get_resolution
from flowdec import data as fd_data
from flowdec import restoration as fd_restoration
from multiprocessing.pool import ThreadPool

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('ex', type=float)
parser.add_argument('em', type=float)

# Optional argument
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')
parser.add_argument('--mirror_padding', type=int, default=[16, 32, 32], nargs=3)
#parser.add_argument('--stride', type=int, default=[32, 64, 64], nargs=3)
#parser.add_argument('--patch', type=int, default=[64, 128, 128], nargs=3)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--magnification', type=float, default=20)
parser.add_argument('--na', type=float, default=1.0)
parser.add_argument('--refr_index', type=float, default=1.333)
parser.add_argument('--iters', type=int, default=25)
parser.add_argument('--pinhole_diameter', type=float, default=29.1096)
parser.add_argument('--pinhole_shape', type=str, default='round', choices=['round', 'square'])
parser.add_argument('--normalise', action='store_true')

parser.add_argument('--verbose', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)
args = parser.parse_args()
bname = lambda x: os.path.basename(os.path.splitext(x)[0])


def create_psf(zshape, rshape, zdims, rdims, ex_wavelen, em_wavelen, pinhole_radius,
               num_aperture=1.0, refr_index=1.333,  pinhole_shape='square',
               psf_type=(psf.ISOTROPIC | psf.CONFOCAL), magnification=20.0):

    args = dict(shape=(int(zshape), int(rshape)),
                dims=(zdims, rdims),
                ex_wavelen=ex_wavelen,
                em_wavelen=em_wavelen,
                num_aperture=num_aperture,
                refr_index=refr_index,
                pinhole_radius=pinhole_radius,
                pinhole_shape=pinhole_shape,
                magnification=magnification)

    obsvol = psf.PSF(psf_type, **args)

    return obsvol.volume()

def norm_shape(shape):
    '''
    Normalize numpy array shapes so they're always expressed as a tuple, 
    even for one-dimensional shapes.

    Parameters
        shape - an int, or a tuple of ints

    Returns
        a shape tuple
    '''
    try:
        i = int(shape)
        return (i,)
    except TypeError:
        # shape was not a number
        pass

    try:
        t = tuple(shape)
        return t
    except TypeError:
        # shape was not iterable
        pass

    raise TypeError('shape must be an int, or a tuple of ints')

def sliding_window(a, ws, ss=None, flatten=True):
    '''
    Return a sliding window over a in any number of dimensions

    Parameters:
        a  - an n-dimensional numpy array
        ws - an int (a is 1D) or tuple (a is 2D or greater) representing the size 
             of each dimension of the window
        ss - an int (a is 1D) or tuple (a is 2D or greater) representing the 
             amount to slide the window in each dimension. If not specified, it
             defaults to ws.
        flatten - if True, all slices are flattened, otherwise, there is an 
                  extra dimension for each dimension of the input.

    Returns
        an array containing each n-dimensional window from a

    from http://www.johnvinyard.com/blog/?p=268
    '''
    from numpy.lib.stride_tricks import as_strided as ast

    if ss is None:
        # ss was not provided. the windows will not overlap in any direction.
        ss = ws
    ws = norm_shape(ws)
    ss = norm_shape(ss)

    # convert ws, ss, and a.shape to numpy arrays so that we can do math in every 
    # dimension at once.
    ws = np.array(ws)
    ss = np.array(ss)
    shape = np.array(a.shape)


    # ensure that ws, ss, and a.shape all have the same number of dimensions
    ls = [len(shape),len(ws),len(ss)]
    if 1 != len(set(ls)):
        raise ValueError(\
        'a.shape, ws and ss must all have the same length. They were %s' % str(ls))

    # ensure that ws is smaller than a in every dimension
    if np.any(ws > shape):
        raise ValueError('ws cannot be larger than a in any dimension. a.shape was %s and ws was %s' % (str(a.shape),str(ws)))

    # how many slices will there be in each dimension?
    newshape = norm_shape(((shape - ws) // ss) + 1)
    # the shape of the strided array will be the number of slices in each dimension
    # plus the shape of the window (tuple addition)
    newshape += norm_shape(ws)
    # the strides tuple will be the array's strides multiplied by step size, plus
    # the array's strides (tuple addition)
    newstrides = norm_shape(np.array(a.strides) * ss) + a.strides
    strided = ast(a, shape=newshape, strides=newstrides)
    if not flatten:
        return strided

    # Collapse strided so that it has one more dimension than the window.  I.e.,
    # the new array is a flat list of slices.
    meat = len(ws) if ws.shape else 0
    firstdim = (np.product(newshape[:-meat]),) if ws.shape else ()
    dim = firstdim + (newshape[-meat:])
    # remove any dimensions with size 1
    dim = np.squeeze(dim)
    return strided.reshape(dim)

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi', '.lif'])
else:
    files = args.input
mkdir(args.output)
print(files)

def deconvolve(fname):
    # Read in data
    print("Read in data")
    try:
        data = tiff.imread(fname)
    except:
        try:
            data = cz.czifile(fname)
        except:
            try:
                import read_lif as lif
                reader = lif.Reader(fname)
                series = reader.getSeries()
                chosen = series[0]
                data = []
                for nchan in range(len(chosen.getChannels())):
                    image = chosen.getFrame(T=0, channel=nchan)
                    data.append(image)
                data = np.stack(data, axis=1)
            except:
                raise Exception(f'Input file type for {fname} not supported.')
    data = np.squeeze(data)

    if data.ndim < 3:
        raise Exception(f'Error in {fname} - requires at least 3 dimensions')
    if data.ndim == 4 and args.channel is not None:
        data = data[:, args.channel]
    if args.resolution is None:
        resolution = get_resolution(fname)
    
    # Generate PSF
    print("Generate PSF")
    pinhole_radius = args.pinhole_diameter / 2 / args.magnification
    zshape = data.shape[0] // 2.
    rshape =  np.min(data.shape[1:]) // 2.
    zdims = zshape * resolution[0]
    rdims = rshape * resolution[1]
    kernel = create_psf(zshape, rshape, zdims, rdims, ex_wavelen=args.ex,
                        em_wavelen=args.em, pinhole_radius=pinhole_radius,
                        num_aperture=args.na, magnification=args.magnification, 
                        pinhole_shape=args.pinhole_shape, refr_index=args.refr_index)

    # Run deconvolution
    print("Run deconvolution")
    algo = fd_restoration.RichardsonLucyDeconvolver(data.ndim, pad_min=np.ones(data.ndim)).initialize()
    if args.verbose:
        print(f'Running RL with {args.iters} iterations for {fname}')
    result = algo.run(fd_data.Acquisition(data=data, kernel=kernel), niter=args.iters)
    
    # Scale the result to the right range
    print("Scale result")
    result = result.data
    if args.normalise:
        result = result / np.max(result) * np.iinfo(data.dtype).max
    result[result < 0] = 0
    result[result > np.iinfo(data.dtype).max] = np.iinfo(data.dtype).max
    result = result.astype(data.dtype)
   
    # Save to file
    print("Save")
    basename = os.path.splitext(os.path.basename(fname))[0]
    fname_out = f'{args.output}/{basename}-deconvolved_{args.iters}.tif'
    tiff.imwrite(fname_out, np.expand_dims(result, axis=1), imagej=True, metadata={'spacing':resolution[0], 'unit':'um'}, 
                resolution=(1./resolution[1], 1./resolution[2]))
    
# Run
p = ThreadPool(args.parallel_workers)
p.map(deconvolve, files)
p.close()           
p.join()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 10:37:07 2022

@author: henrikahl
"""

from imgmisc import parse_resolution
from imgmisc import parse_image_input
import os
import argparse
from imgmisc import mkdir
import tifffile as tiff
from imgmisc import parse_input_files
from multiprocessing import Pool

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
# parser.add_argument('--output', type=str,
#                     help='Output directory', default=f'{os.path.curdir}')
# parser.add_argument('--channel', type=int, default=None)
# parser.add_argument('--masking', type=float, default=1)
parser.add_argument('--parallel_workers', type=int, default=1)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-divergence_angle_data')
# parser.add_argument('--segmented', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--verbose', action='store_true')
args = parser.parse_args()

files = parse_input_files(
    args.input, include=['.tif', '.tiff', '.lsm', '.czi'])
# mkdir(args.output)


def compress(fname):
    data = parse_image_input(fname, None)
    resolution = parse_resolution(fname, args.resolution)

    tiff.imwrite(fname, data, resolution=[1/resolution[1], 1/resolution[2]], metadata={
                 'unit': 'um', 'spacing': resolution[0]}, compression='lzw')

p = Pool(args.parallel_workers)
p.map(compress, files)
p.close()
p.join()

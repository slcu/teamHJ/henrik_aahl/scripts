#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 11:29:32 2020

@author: henrikahl
"""

import os
from img2org.conversion import read_neigh_init, read_init
from imgmisc import listdir, mkdir
import numpy as np
from numba import njit
from scipy.integrate import solve_ivp
import sys
from itertools import product
from multiprocessing import Pool
from multiprocessing import cpu_count

def to_nested_np(arr):  
            return np.array([np.array(val) for val in arr])
        
def make_2D_array(lis):
    """Funciton to get 2D array from a list of lists
    """
    n = len(lis)
    lengths = np.array([len(x) for x in lis])
    max_len = np.max(lengths)
    arr = np.full((n, max_len), np.nan)

    for i in range(n):
        arr[i, :lengths[i]] = lis[i]
    return arr, lengths

dataset = '131007-PIN_GFP-acyl_YFP-Timelapse_4h-WT'
plant_type = 'WT'
wall_depth = 1

volume_index = 3
auxin_index = 6

PROJECT_DIR = os.path.abspath(f'{os.path.expanduser("~")}/projects/pin_patterns')

# INPUT_DIR = f'/home/henrikahl/deconv'
# INPUT_DIR = '/home/henrikahl/'
# INPUT_DIR = '/home/henrikahl/24thJune2021_SP8_SAM_test'
# INPUT_DIR = '/home/henrikahl/hanky'
# INPUT_DIR = '/home/henrikahl/bentest'
# INPUT_DIR = '/home/henrikahl/210720-PG_MY-WT'
INPUT_DIR = '/home/henrikahl/210802-PG_MY-WT'
# INPUT_DIR = '/home/henrikahl/210616-PINGFP_MMtdTomato-WT-lambda'
# INPUT_DIR = '/home/henrikahl/25thJune2021_StretchP8_PIN1GFP_MY'

OUTPUT_DIR = INPUT_DIR
# CELLS_DIR = f'{OUTPUT_DIR}/cells'
# WALLS_DIR = f'{OUTPUT_DIR}/walls'
# mkdir(CELLS_DIR)
# mkdir(WALLS_DIR)

init_files = listdir(INPUT_DIR, include=['.init'], exclude=['end_state', 'original'], sorting='natural')

init_files = ['/home/henrikahl/210802-PG_MY-WT/210802-PG_MY-WT-plant_6-C2-cropped_predictions_multicut-refined-wall_depth_1-shift_0.0_0.19736842105263158_-0.25.init']
# init_files = [ff for ff in init_files if 'shift' in ff]

# init_files = ['/home/henrikahl/C2-201112-PIN_GFP-MM_dsRed-WT-SAM2-FM1_predictions_multicut-refined-wall_depth_1.init']
# init_files = ['/home/henrikahl/C2-210323-PIN1GFP_PI-WT-plant_1-whole_plant-1-cropped-converted_predictions_multicut-refined-wall_depth_1.init']
# init_files = init_files[5:]
args = list(product(*[np.arange(len(init_files)), np.arange(-3, 4), np.arange(-3, 4), [0,1]]))
# def run(iter_):
iter_ = 0
for iter_ in range(len(init_files)):
    args = [[iter_, 0, 0, 0]]
    args = args[0]
    file_index, xx, yy, prod = args

    import re
    shift = np.array(re.findall(f'shift_(\S+)_(\S+)_(\S+).init', init_files[file_index]), dtype='float')[0]
    # if os.path.isfile(f'/home/henrikahl/shift/{shift[0]}_{shift[1]}_{shift[2]}.png'):
    #     return
    
# args = list(product(*[np.arange(len(init_files)), np.arange(-3, 4), np.arange(-3, 4), [2]]))
# args = args[24] # + 49
# args = args[-1] # + 49
# def run_ccat(args):
    print(os.path.basename(init_files[file_index]), xx, yy, prod)

    # Read in paths and data
    init_path = init_files[file_index]
    neigh_path = f'{init_path[:-5]}.neigh'
    
    # Iteration specific parameters
    De = 2.2e-03                    # passive efflux (p_AH f^cell_AH) : 2.2e-03
    Pe = 1.26e-00 * 10**(float(xx)) # PIN mediated efflux : 1.26e-00
    Di = 1.32e-01                   # passive influx (p_AH f^wall_AH) : 1.32e-01
    Pi = 1.96e-00 * 10**(float(yy)) # AUX mediated influx : 1.96e-00
 
    # Extract values from data
    init = read_init(init_path)
    volumes = init[volume_index].values
    auxin = init[auxin_index].values.copy()
    
    in_l1 = init[7]
    in_l2 = init[8]
    # in_bottom = init[9]
    in_l1 = in_l1.index[in_l1 == 1].values
    in_l2 = in_l2.index[in_l2 == 1].values
    # not_in_bottom = in_bottom.index[in_bottom != 1].values
    
    neigh_data = read_neigh_init(neigh_path)
    neighs, auxs, pins, npins, nauxs, areas, ninl1, ninl2 = [], [], [], [], [], [], [], []
    del neigh_data['n_cells'], neigh_data['n_params']
    for key, val in neigh_data.items():
        neighs.append(list(val['neighs'].astype(int)))
        ninl1.append(list(np.isin(val['neighs'], in_l1)))
        ninl2.append(list(np.isin(val['neighs'], in_l2)))
        areas.append(list(val['p0'].astype(float)))
        pins.append(list(val['p1'].astype(float)))
        auxs.append(list(val['p2'].astype(float)))
        npins.append(list(np.array([float(neigh_data[nn]['p1'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))
        nauxs.append(list(np.array([float(neigh_data[nn]['p2'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))

    # Ensure comprehensible formats for function defintion
    ntest, nlens = make_2D_array(neighs)
    pins, nlens = make_2D_array(pins)
    auxs, nlens = make_2D_array(auxs)
    npins, nlens = make_2D_array(npins)
    nauxs, nlens = make_2D_array(nauxs)
    areas, nlens = make_2D_array(areas)
    ninl1, nlens = make_2D_array(ninl1)
    ninl2, nlens = make_2D_array(ninl2)
    ntest = ntest.astype(int)
    fluxes = np.zeros_like(pins)
    
    # Normalise data
    normalisation_factor = np.nanmean([np.nansum(pp) for pp in pins])
    auxin = auxin / normalisation_factor 
    pins = pins / normalisation_factor 
    auxs = auxs / normalisation_factor 
    npins = npins  / normalisation_factor 
    nauxs = nauxs / normalisation_factor 

    # Define the transport function. Using @njit speeds things up massively 
    # (although it does limit our formalism a little by preventing us from 
    # using nested arrays, which is why we convert things to 2D arrays above.)
    import numba
    # hill_peripheral = lambda x: x.astype(float)**12 / (60**12 + x.astype(float)**12)
    def hill_peripheral(x): 
        return x.astype(float)**12 / (60**12 + x.astype(float)**12)
    # hill_central = lambda x: 1. - x**12 / (30**12 + x**12)
    def hill_central(x):
        return 1. - x**12 / (30**12 + x**12)
    
    p_auxin = .1 if prod == 1 else 0
    d_auxin = .1 if prod in [1, 2, 3, 4] else 0
    p_auxin_auxin = .1 if prod == 2 else 0
    p_auxin_peripheral = (.1 * hill_peripheral(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if prod == 3 else np.zeros(init.shape[0])).astype(float)
    p_auxin_central = (.1 * hill_central(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if prod == 4 else np.zeros(init.shape[0])).astype(float)
    
    # If we're in one of the regional production phenotypes, we should adjust 
    # the production so that the total production in the tissue remains the same
    total_prodiction_WT = sum(.1 * volumes)
    if prod == 3:
        p_auxin_peripheral = p_auxin_peripheral / (sum(p_auxin_peripheral * volumes) / total_prodiction_WT)
    elif prod == 4:
        p_auxin_central = p_auxin_central / (sum(p_auxin_central * volumes) / total_prodiction_WT)
    
    @njit(parallel=True, fastmath=True)
    def cell_cell_auxin_transport(t, values, fluxes):
        output = np.zeros(values.shape[0])       
        for ii in numba.prange(values.shape[0]):
            cell_sum = .0
            for jj, nei in enumerate(ntest[ii, :nlens[ii]]):
                flux = areas[ii, jj] * ((Di + Pi * auxs[ii, jj]) * (De + Pe * npins[ii, jj]) * values[nei] - 
                                        (Di + Pi * nauxs[ii, jj]) * (De + Pe * pins[ii, jj]) * values[ii]) / (2 * Di + Pi * (auxs[ii, jj] + nauxs[ii, jj]))
                fluxes[ii, jj] = flux
                cell_sum += flux
            output[ii] = 1 / volumes[ii] * cell_sum + ( 
                # case 0: no production & degradation
                p_auxin +                       # case 1: global production
                p_auxin_auxin * values[ii]**2 + # case 2: auxin self-activation
                p_auxin_peripheral[ii] +        # case 3: peripheral auxin production ("from flowers")
                p_auxin_central[ii] -           # case 4: central auxin production ("from CZ")
                d_auxin * values[ii])           # happens in all cases but 0
        return output

    # Solve!
    y0 = auxin.copy()
    t_res = solve_ivp(cell_cell_auxin_transport, y0=y0, t_span=(0, 10**6), method='BDF', args=(fluxes, ), rtol=1e-6, atol=1e-10)

    # Collect data we want to save    
    output = init.copy()
    output[auxin_index] = t_res.y[:, -1]
    
    # l1l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    # l1l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    # l2l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 
    # l2l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 

    # output[10] = fluxes.sum(1)
    # output[11] = l1l1_fluxes
    # output[12] = l1l2_fluxes
    # output[13] = l2l2_fluxes
    # output[14] = l2l1_fluxes

    # import seaborn as sns
    # sns.scatterplot(x=output.loc[output[2] > output[2].mean()][1], y=output.loc[output[2] > output[2].mean()][0], c=output.loc[output[2] > output[2].mean()][9], s=40)
    # sns.scatterplot(x=output.loc[output[8] == 1][1], y=output.loc[output[8] == 1][2], c=output.loc[output[8] == 1][9], s=120)
    
    values = output[auxin_index].values
    # L1
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    
    # L2+
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn not in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn not in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn not in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn not in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    # values = np.array([np.mean((values[ii] + values[[nn for nn in neighs[ii] if nn not in in_l1]].sum()) / (len([nn for nn in neighs[ii] if nn not in in_l1]) + 1)) if ii in in_l1 else values[ii] for ii in range(len(values))])
    values = np.array([np.mean((values[ii] + values[neighs[ii]].sum()) / (len(neighs[ii]) + 1)) for ii in range(len(values))])
    values = np.array([np.mean((values[ii] + values[neighs[ii]].sum()) / (len(neighs[ii]) + 1)) for ii in range(len(values))])
    values = np.array([np.mean((values[ii] + values[neighs[ii]].sum()) / (len(neighs[ii]) + 1)) for ii in range(len(values))])
    
    import re
    import pyvista as pv
    # p = pv.Plotter(notebook=False, off_screen=True)
    p = pv.Plotter(notebook=False, off_screen=False)
    sph = pv.Sphere(radius=5, phi_resolution=15, theta_resolution=15)
    p.set_background('white')
    axis = 0
    mesh = pv.PolyData(output[[0,1,2]].values)
    # mesh = pv.PolyData(output[[0,1,2]].values[output.values[:,axis] < output.values[:,axis].mean()])
    # mesh['scalars'] = values[output.values[:,axis] < output.values[:,axis].mean()]
    mesh['scalars'] = values
    mesh = mesh.glyph(scale=False, geom=sph)
    # p.add_points(output[[0,1,2]].values, scalars=output[auxin_index].values, render_points_as_spheres=True, point_size=30, cmap='turbo')
    p.add_mesh(mesh, cmap='turbo', smooth_shading=True, clim=[np.quantile(values, 0.001), np.quantile(values, 0.999)])
    p.view_yz(True if '.lif' in init_files[file_index] else False)
    p.set_background('white')
    shift = np.array(re.findall(f'shift_(\S+)_(\S+)_(\S+).init', init_files[file_index]), dtype='float')[0]
    p.show()
    # p.screenshot(f'/home/henrikahl/shift/{shift[0]}_{shift[1]}_{shift[2]}.png',
                  # transparent_background=False, window_size=[2000, 2000])
    p.close()
    # p.show()

# import tifffile as tiff
# d = tiff.TiffFile("/home/henrikahl/Downloads/PIN1-GFP-myr-YFP-pCLV3dsRED-SAM2-FM4.tiff")
# from imgmisc import get_resolution

#     # Save output to file
#     basename = os.path.basename(os.path.splitext(init_path)[0])
#     fname_out = f'{CELLS_DIR}/{basename}-pe_{xx}-pi_{yy}-prod_{int(prod)}-cells_end_state.dat'
#     print(f'Printing {fname_out}')
#     output.to_csv(fname_out, header=False, sep='\t', index=False, float_format='%.15f')

#     fname_out = f'{WALLS_DIR}/{basename}-pe_{xx}-pi_{yy}-prod_{int(prod)}-walls_end_state.dat'
#     print(f'Printing {fname_out}')
#     neigh_output = neigh_data.copy()
#     for ii in range(len(neigh_output)):
#         neigh_output[list(neigh_output.keys())[ii]]['p3'] = fluxes[ii][:nlens[ii]]
#     from img2org.conversion import write_neigh_init
#     neigh_output['n_cells'] = len(output)
#     neigh_output['n_params'] = 4
#     write_neigh_init(fname_out, neigh_output)        
    
# if len(sys.argv) < 2:
#     n_cores = cpu_count() - 1
# else:
#     n_cores = int(sys.argv[1])

# n_cores = 3
# # args = list(product(*[np.arange(len(init_files)), np.arange(-3, 4), np.arange(-3, 4), [0, 1, 2, 3, 4]]))
# p = Pool(n_cores)
# output = p.map(run, range(len(init_files)))
# p.close()           
# p.join()


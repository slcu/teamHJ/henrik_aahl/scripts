#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 11:48:51 2020

@author: henrik
"""

import os
import sys
import copy
import numpy as np
import mahotas as mh
import czifile as cz
import tifffile as tiff
from imgmisc import get_layers
from imgmisc import find_neighbors
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import get_bottom
from imgmisc import get_resolution
from skimage.measure import regionprops
from skimage.transform import resize
from skimage.morphology import ball
# from img2org.quantify import get_wall
# from img2org.quantify import area_between_walls
from multiprocessing import Pool, cpu_count
from scipy.ndimage import shift
import argparse
from imgmisc import validate_inputs, validate_connectivity, compute_neighbours
import scipy.ndimage as nd
from skimage.measure import marching_cubes
from skimage.measure import mesh_surface_area            

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('--seg_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--pin_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--shift', type=float, nargs=3, default=[0.,0.,0.])
parser.add_argument('--fct', type=str, default='sum')
parser.add_argument('--wall_depth', type=int, default=1)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-stackreg')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.seg_input) == 1 and os.path.isdir(os.path.abspath(args.seg_input[0])):
    seg_files = listdir(args.seg_input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    seg_files = args.seg_input
if len(args.pin_input) == 1 and os.path.isdir(os.path.abspath(args.pin_input[0])):
    pin_files = listdir(args.pin_input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    pin_files = args.pin_input

# TODO: Cover all cases
if len(seg_files) == 1 and len(pin_files) == 1:
    files = [[seg_files[0], pin_files[0]]]
else: 
    files = list(zip(seg_files, pin_files))
mkdir(args.output)

# files = [['/home/henrikahl/220621/220621-PIN1GFP_MYRYFP-WT-plant_1-0.02cyt_lambda-C2-cropped_predictions_multicut-refined.tif', 
#           '/home/henrikahl/220621/220621-PIN1GFP_MYRYFP-WT-plant_1-0.02cyt_lambda-C1-cropped.tif']]

bname = lambda x: os.path.basename(os.path.splitext(x)[0])

if args.fct == 'max':
    PIN_FCT = np.max
elif args.fct == 'mean':
    PIN_FCT = np.mean
elif args.fct == 'median':
    PIN_FCT = np.median
else:
    PIN_FCT = np.sum

# PIN_FCT = np.sum

if args.verbose:
    print('Input files are:')
    for fpair in files:
        print(f'    {fpair[0]} {fpair[1]}\n')
    
wall_selem = ball(2) if args.wall_depth < 2 else ball(args.wall_depth) # important
def run(input_files):
    seg_fname, pin_fname = input_files
    basename = bname(seg_fname)
    if args.verbose:
        print(f'Initiating run on {basename}')

    if args.skip_done:
        done_files = listdir(args.output, include='.init')
        init_oname = f'{args.output}/{basename}-wall_depth_{args.wall_depth}-shift_{args.shift[0]}_{args.shift[1]}_{args.shift[2]}.init'
        if any([init_oname == ff for ff in done_files]):
            print('Skipping {basename} - already done')
            return

    # Read in data and limit PIN expression to the depth that we are interested in
    seg_img = tiff.imread(seg_fname)
    if args.resolution is None:
        resolution = np.asarray(get_resolution(seg_fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution

    # Read in PIN and shift if necessary
    pin_img = tiff.imread(pin_fname)
    if tuple(args.shift) != (0, 0, 0):
        if args.verbose:
            print(f'Shifting {basename}')
        pin_img = shift(pin_img, args.shift, mode='reflect', order=3)

    # Resize PIN data if needed
    if pin_img.shape != seg_img.shape:
        if args.verbose:
            print(f'Reshaping {basename} from {pin_img.shape} to {seg_img.shape}')
        seg_img = resize(pin_img,
                         seg_img.shape,
                         # np.round(np.divide(seg_img.shape, scaling_factor)).astype('int'),
                         mode='edge',
                         anti_aliasing=False,
                         anti_aliasing_sigma=None,
                         preserve_range=True,
                         order=3).astype(float)
    pin_img = to_uint8(pin_img, normalize=False)

    # Compute the cell neighbours
    if args.verbose:
        print(f'Finding neighbours for {basename}')
    neighs = find_neighbors(seg_img, background=0, connectivity=1)

    # Get the cell layers we need to keep track of (L1, L2, bottom)
    if args.verbose:
        print(f'Getting layers for {basename}')
    l1, l2 = get_layers(seg_img, background=0, depth=2)
    bottom_cells = get_bottom(seg_img)
    
    # Compute cell properties such as wall concentrations/area
    region_properties = regionprops(seg_img, intensity_image=pin_img)
    all_props = {rr.label:{} for rr in region_properties}
    def wall_area(wall1, wall2, resolution=[1,1,1]):
        verts1, faces1, _, _ = marching_cubes(wall1, level=0.5, spacing=resolution, allow_degenerate=False)
        verts2, faces2, _, _ = marching_cubes(wall2, level=0.5, spacing=resolution, allow_degenerate=False)
        verts1, verts2 = [tuple(x) for x in verts1], [tuple(x) for x in verts2]
        verts_overlap = set(x for x in set(verts1) & set(verts2))
        
        # Get vertices
        remaining_verts1 = np.where([vv in verts_overlap for vv in verts1])[0]
        faces1 = faces1[np.all(np.isin(faces1, remaining_verts1), 1)]
        remaining_verts2 = np.where([vv in verts_overlap for vv in verts2])[0]
        faces2 = faces2[np.all(np.isin(faces2, remaining_verts2), 1)]
        
        verts1 = np.array(verts1)
        verts2 = np.array(verts2)
        
        # There will be a small difference in area due to non-overlapping faces
        # despite the overlapping vertices
        area1 = mesh_surface_area(verts1, faces1)
        area2 = mesh_surface_area(verts2, faces2)
        area = (area1 + area2) / 2
        return area

    def quantify_membrane(seg_img, int_img, background=0, connectivity=1,
            selem=None, neighs=None, fct=np.sum, verbose=False):
        
        # Compute which cells have interfaces with one another
        if neighs is None:
            neighs = find_neighbors(seg_img, background=background, connectivity=connectivity)
        interfaces = set()
        for nn, neigh in neighs.items():
            for nnn in neigh:
                interfaces.add((nn, nnn))
                interfaces.add((nnn, nn))
        interfaces = {iface:[] for iface in interfaces}

        # Validate all inputs
        seg_img, _, _ = validate_inputs(seg_img, mask=None, resolution=None)
        int_img, _, _ = validate_inputs(int_img, mask=None, resolution=None)
        connectivity, offset = validate_connectivity(seg_img.ndim, connectivity,
                                                      offset=None)
        if selem is None:
            selem = ball(1)
        
        # Pad the image, and mask so that we can use the mask to keep from running
        # off the edges
        pad_width = [(p, p) for p in offset]
        seg_img = np.pad(seg_img, pad_width, mode='constant')
        int_img = np.pad(int_img, pad_width, mode='constant')

        # Get a mask of all the internal cell borders in pixel format
        seg_mask = mh.borders(seg_img, selem)
        seg_mask[seg_img == background] = False
    
        # Get flattened versions of everything
        seg_flat_neighbourhood = compute_neighbours(seg_img, selem, offset)
        int_img_raveled = int_img.ravel()
        seg_img_raveled = seg_img.ravel()
        indices = np.where(seg_mask.ravel())[0]
        
        # Go through the interfaces and collate all the PIN voxels at the
        # corresponding interfaces
        point_neighbours = np.array(list(map(lambda x: x + seg_flat_neighbourhood, indices)))
        ikeys = set(list(interfaces.keys()))
        for ii, ptn in enumerate(point_neighbours):
            if verbose:
                print(ii / len(point_neighbours))
            idx = indices[ii]
            this_cell = seg_img_raveled[idx]
            cells_in_reach = np.unique(seg_img_raveled[ptn])
            cells_in_reach = [cc for cc in cells_in_reach if (this_cell, cc) in ikeys]
            
            for vv in cells_in_reach:
                interfaces[(this_cell, vv)].append(int_img_raveled[idx] / len(cells_in_reach))

        interfaces = {key: fct(vals) for key, vals in interfaces.items()}
    
        return interfaces

    def interface_areas(seg_img, neighs=None, verbose=False):
        # Limit the volume of interest to the cells in question
        bboxes = nd.find_objects(seg_img) 
        b = []
        for bbox in bboxes:
            c = []
            for axis, bb in enumerate(bbox):
                c.append(slice(max([bb.start - 2, 0]), min([bb.stop + 2, seg_img.shape[axis]])))
            b.append(c)
        bboxes = b
        
        # Compute the interface areas by generating triangle meshes from the
        # voxels neighbouring another cell
        areas = {}
        selem = np.ones((3, 3, 3)) # TODO might this introduce a bias?
        for l1, l1_neighs in neighs.items():
            if verbose:
                print(l1 / len(neighs))
            cell_mask = seg_img[bboxes[l1-1][0].start:bboxes[l1-1][0].stop,
                                bboxes[l1-1][1].start:bboxes[l1-1][1].stop,
                                bboxes[l1-1][2].start:bboxes[l1-1][2].stop]
            l1_cell_mask = cell_mask == l1
            l1_dil = nd.binary_dilation(l1_cell_mask, selem)
    
            l1_neighs = l1_neighs[l1_neighs > l1]
            for l2 in l1_neighs:
                l2_cell_mask = cell_mask == l2
                l2_dil = nd.binary_dilation(l2_cell_mask, selem)
                border_mask = l1_dil & l2_dil
                w1 = border_mask & l1_cell_mask
                w2 = border_mask & l2_cell_mask
                                    
                area = wall_area(w1, w2, resolution)
                areas[(l1, l2)] = area
                areas[(l2, l1)] = area
        return areas
    
    if args.verbose:
        print('Getting bottom cells')
    bottom_cells = get_bottom(seg_img)

    # Get signal intensities and wall areas
    if args.verbose:
        print(f'Computing interface areas for {basename}')
    mem_areas = interface_areas(seg_img, neighs=neighs)

    if args.verbose:
        print(f'Computing membrane abundance for {basename}')
    mem_pins = quantify_membrane(seg_img, pin_img, selem=ball(1), neighs=neighs)

    if args.verbose:
        print(f'Printing to file for {basename}')
    labels = [rr.label for rr in region_properties]
    coords = [np.multiply(rr.centroid, resolution) for rr in region_properties]
    volumes = [np.multiply(rr.image.sum(), np.prod(resolution)) for rr in region_properties]
    total_PIN = [rr.intensity_image.sum() for rr in region_properties]
    in_l1 = [1 if label in l1 else 0 for label in labels]
    in_l2 = [1 if label in l2 else 0 for label in labels]
    in_bottom = [1 if label in bottom_cells else 0 for label in labels]

    all_props = {label:{} for label in labels}
    for label in labels:
        all_props[label]['coords'] = coords[label-1]
        all_props[label]['volume'] = volumes[label-1]
        all_props[label]['total_PIN'] = total_PIN[label-1]
        all_props[label]['L1'] = in_l1[label-1]
        all_props[label]['L2'] = in_l2[label-1]
        all_props[label]['bottom'] = in_bottom[label-1]
        cell_neighs, Pij, Pji, aij = [], [], [], []
        for neigh in neighs[label]:
            if mem_areas[(label, neigh)] == 0:
                continue
            cell_neighs.append(neigh)
            aij.append(mem_areas[(label, neigh)])
            Pij.append(mem_pins[(label, neigh)])
            Pji.append(mem_pins[(neigh, label)])
        all_props[label]['neighbors'] = cell_neighs
        all_props[label]['a_ij'] = aij
        all_props[label]['P_ij'] = Pij
        all_props[label]['P_ji'] = Pji

    # Compute normalised PIN values
    total_PIN = np.array([r['total_PIN'] / r['volume'] for r in all_props.values()])
    membrane_PIN = [np.array(val["P_ij"]) / val["a_ij"] for val in all_props.values()] 
    membrane_AUX = [np.array([sum(val["P_ij"]) / sum(val["a_ij"])] * len(val["a_ij"])) if len(val["a_ij"]) > 0 else np.array([]) for val in all_props.values()]

    # Calculate or assign init values based on the above
    init_total_PIN = total_PIN  
    init_auxin = total_PIN 
    init_total_AUX = total_PIN 
    init_membrane_PIN = membrane_PIN 
    init_membrane_AUX = membrane_AUX 
    
    # Write init file
    init = str(len(all_props)) + " 10" 
    neigh = str(len(all_props)) + " 4"
    for cell_id in all_props.keys():

        # Cell variables
        init += "\n" + str(all_props[cell_id]["coords"][0])
        init += " " + str(all_props[cell_id]["coords"][1])
        init += " " + str(all_props[cell_id]["coords"][2])
        init += " " + str(all_props[cell_id]["volume"])
        init += " " + str(init_total_PIN[cell_id - 1])
        init += " " + str(init_total_AUX[cell_id - 1])
        init += " " + str(init_auxin[cell_id - 1])
        init += " " + str(all_props[cell_id]["L1"]) 
        init += " " + str(all_props[cell_id]["L2"]) 
        init += " " + str(all_props[cell_id]["bottom"]) 
        
        # Membrane variables
        n_neighs = len(all_props[cell_id]["neighbors"])
        neigh += "\n" + str(cell_id - 1) 
        neigh += " " + str(n_neighs)

        for ii in range(n_neighs):
            neigh += " " + str(all_props[cell_id]["neighbors"][ii] - 1)
        for ii in range(n_neighs):
            neigh += " " + str(all_props[cell_id]["a_ij"][ii])
        for ii in range(n_neighs):
            neigh += " " + str(init_membrane_PIN[cell_id - 1][ii])
        for ii in range(n_neighs):
            neigh += " " + str(init_membrane_AUX[cell_id - 1][ii])
        for jj in range(n_neighs): # Fluxes placeholder
            neigh += " " + str(0)

    init_filepath = f'{args.output}/{basename}-wall_depth_{args.wall_depth}-shift_{args.shift[0]}_{args.shift[1]}_{args.shift[2]}.init'
    neigh_filepath = f'{args.output}/{basename}-wall_depth_{args.wall_depth}-shift_{args.shift[0]}_{args.shift[1]}_{args.shift[2]}.neigh'

    with open(init_filepath, 'w') as f1:
        f1.write(init)
    with open(neigh_filepath, 'w') as f2:
        f2.write(neigh)
    if args.verbose:
        print(f'{basename} complete!\n\n')

n_cores = args.parallel_workers
print(f'Using {n_cores} cores')
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()

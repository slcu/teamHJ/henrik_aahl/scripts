#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 14:57:11 2020

@author: henrikahl
"""

import re
import os
import ants
import shutil
import argparse
import numpy as np
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import get_resolution
from imgmisc import get_l1_voxel_mask
from datetime import datetime
from itertools import product
from scipy.ndimage import rotate
from scipy.ndimage import distance_transform_edt
from multiprocessing import Pool
from phenotastic.mesh import fill_beneath
from phenotastic.mesh import fill_contour
# from imgmisc import l1_distance_mask
def bname(x): return os.path.basename(os.path.splitext(x)[0])


def l1_distance_mask(data, distance_threshold=None, resolution=[1, 1, 1], background=0):
    '''
    Generate a distance-to-background mask only of the epidermis.

    Parameters
    ----------
    data : np.array 
        Raw intensity data.
    threshold : float, optional
        Distance threshold specifying how deep the L1 goes.
    resolution : list or np.array of length 3, optional
        Image physical resolution. The default is [1, 1, 1].
    background : int
        The background label. Default is 0.

    Returns
    -------
    mask : np.array
        Distance-to-background mask of the L1.

    '''
    surface = get_l1_voxel_mask(data, mask=data, resolution=resolution, background=background)
    dist2surf = distance_transform_edt(~surface, sampling=resolution)
    mask = dist2surf.copy()
    mask[~(data & (dist2surf < distance_threshold))] = 0
    return mask

# Define some parameters
parser = argparse.ArgumentParser(description='')

# Required positional argument
# parser.add_argument('--input', type=str, nargs='+',
#                     help='Input files or directory')
parser.add_argument('--from_files', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--to_files', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--rotation', type=int, default=0)
parser.add_argument('--flip', type=list, default='noflip')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

# Define some parameters
OUTPUT_DIR = args.output
mkdir(OUTPUT_DIR)

# Identify the file list
# isdir = lambda x : os.path.isdir(os.path.abspath(x))
# if len(args.from_files) == 1 and isdir(args.from_files[0]):
#     from_files = listdir(args.from_files[0], sorting='natural', include=['.tif', '.tiff', '.lsm', '.czi'])
# else:
#     from_files = args.from_files
# if len(args.to_files) == 1 and isdir(args.to_files[0]):
#     to_files = listdir(args.to_files[0], sorting='natural', include=['.tif', '.tiff', '.lsm', '.czi'])
# else:
#     to_files = args.to_files

# Define the function arguments
# all_args = list(product(*[files,
#                           files,
#                           ['flip', 'noflip'],
#                           np.arange(0, 360, 360 / NROTS)]))

# def generate_transformation(input_args):
# print(f'Running argument set {input_args}')
# print(f'{len(listdir(OUTPUT_DIR))} / {len(all_args)} done')
# fixfile, movfile, flip, rot = input_args
movfile = args.from_files
fixfile = args.to_files
flip = args.flip
rot = args.rotation


mplant = re.findall('plant_\d+', movfile)[0]
fplant = re.findall('plant_\d+', fixfile)[0]

# Don't do self-registration
if fixfile == movfile:
    quit()

# Load the fixed data
finfostr = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)', bname(fixfile))[0]
fdataset, fgenotype, freporter, fplant = finfostr
fdataset, fplant = int(fdataset), int(fplant)
minfostr = re.findall('(\d+)-(\S+)-(\S+)-plant_(\d+)', bname(movfile))[0]
mdataset, mgenotype, mreporter, mplant = minfostr
mdataset, mplant = int(mdataset), int(mplant)

# Break if done
oname = f'{OUTPUT_DIR}/{fdataset}-{fgenotype}-{freporter}-f_{fplant}-{mdataset}-{mgenotype}-{mreporter}-m_{mplant}-flip_{flip}-rot_{rot}'
if args.skip_done and (oname in '-'.join(OUTPUT_DIR)):
    print('Done. Skipping...')
    quit()

# Load image resolutions
fres = get_resolution(fixfile)
mres = get_resolution(movfile)

# Load the fixed data and generate a distance mask for the epidermis. This
# is that we'll use as registration information.
print(f'{datetime.now().strftime("%H:%M:%S")} - loading {bname(fixfile)}')
fdata = tiff.imread(fixfile)
fdata = fdata > 0
fdata = fill_contour(fdata, True)
fdata = fill_beneath(fdata)
fdata = l1_distance_mask(fdata, 10, fres)
fdata = ants.from_numpy(fdata, spacing=fres)

# Load the moving data and flip/rotate it
mdata = tiff.imread(movfile).astype(float)
mdata = mdata[:, ::-1, ::] if flip == 'flip' else mdata
mdata = rotate(mdata, rot, axes=(1, 2), reshape=False, order=0)
mdata = mdata > 0
mdata = fill_contour(mdata, True)
mdata = fill_beneath(mdata)
mdata = l1_distance_mask(mdata, 10, mres)
mdata = ants.from_numpy(mdata, spacing=mres)

# Perform the registration and compute the image similarity post registration
print(f'registering {fixfile}')
result1 = ants.registration(fixed=fdata, moving=mdata, type_of_transform='Affine',
                            aff_metric='mattes', aff_sampling=128, grad_step=.1)
result2 = ants.registration(fixed=fdata, moving=mdata, type_of_transform='Affine',
                            aff_metric='mattes', aff_sampling=128, grad_step=.1)
mutual_information1 = abs(
    ants.image_mutual_information(fdata, result1['warpedmovout']))
mutual_information2 = abs(
    ants.image_mutual_information(fdata, result2['warpedmovout']))
mis = [mutual_information1, mutual_information2]
result = [result1, result2][np.argmax(mis)]
mutual_information = [mutual_information1,
                      mutual_information2][np.argmax(mis)]

# Save the transformation matrix
fout = f'{oname}-mi_{abs(mutual_information):.12f}-fwd.mat'
print(f'printing {fout} fwd')
shutil.move(result['fwdtransforms'][0], fout)

print(f'Done {fout}')

# p = Pool(args.parallel_workers)
# p.map(generate_transformation, all_args)
# p.close()
# p.join()

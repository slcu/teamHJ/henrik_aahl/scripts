#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 27 10:33:51 2022

@author: henrikahl
"""

import os
import re
import numpy as np
import pyvista as pv
import phenotastic.mesh as mp

from imgmisc import mkdir
from imgmisc import listdir
from scipy.spatial import cKDTree
from phenotastic.misc import sph2car
from phenotastic.misc import car2sph
from img2org.conversion import read_init
from img2org.conversion import read_neigh_init
from multiprocessing import Pool

import argparse
parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('--init', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--neigh', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')



parser.add_argument('--normalise', action='store_true')
parser.add_argument('--time', action='store_true')

# Optional argument
parser.add_argument('--suffix', type=str, default='-sphere_projections')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.init) == 1 and os.path.isdir(os.path.abspath(args.init[0])):
    init_files = listdir(args.init[0], sorting='natural', include=['.init'])
else:
    init_files = args.init
if len(args.neigh) == 1 and os.path.isdir(os.path.abspath(args.neigh[0])):
    neigh_files = listdir(args.neigh[0], sorting='natural', include=['.neigh'])
else:
    neigh_files = args.neigh

# TODO: Cover all cases
if len(init_files) == 1 and len(neigh_files) == 1:
    files = [[init_files[0], neigh_files[0]]]
else: 
    files = list(zip(init_files, neigh_files))
mkdir(args.output)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
basename = bname(files[0][0])

sph = mp.remesh(pv.Sphere(radius=1), 1000)
def run(fpair):
    init_fname, ninit_fname = fpair
    init = read_init(init_fname)
    ninit = read_neigh_init(ninit_fname)
    ninit.pop('n_cells')
    ninit.pop('n_params')
    
    shifts = re.findall(r'shift_([-+]?(?:\d*\.\d+|\d+))_([-+]?(?:\d*\.\d+|\d+))_([-+]?(?:\d*\.\d+|\d+))', init_fname)[0]
    shifts = np.array(shifts, float)
    
    data = []
    for key, value in ninit.items():
        neighs = np.where(value['neighs'] > key)[0]
        for nidx in neighs:
            neigh_label = value['neighs'][nidx]
            direction = init.loc[neigh_label][[0,1,2]].values - init.loc[key][[0,1,2]].values
            direction = direction / np.linalg.norm(direction)
            diff = ninit[neigh_label]['p1'][ninit[neigh_label]['neighs'] == key][0] - ninit[key]['p1'][nidx]
            
            if diff < 0:
                direction *= -1
                diff = -diff
            
            data.append(list(direction) + [diff])
    data = np.array(data)        
    rpa = car2sph(data[:, :3])
    sphproj = sph2car(rpa)
    tree = cKDTree(sph.points)
    d, p = tree.query(sphproj)
    
    values = np.zeros(len(sph.points))
    if args.normalise:
        norm = np.zeros(len(sph.points))
        for ii in range(len(sphproj)):
            values[p[ii]] += data[ii, 3]
            norm[p[ii]] += 1
        values = values / norm
    else:
        for ii in range(len(sphproj)):
            values[p[ii]] += data[ii, 3]

    return fpair, values
    # return dataset, genotype, reporter, plant, shifts[0], shifts[1], shifts[2], fpair[0], fpair[1], values

p = Pool(args.parallel_workers)
output = p.map(run, files)
p.close()
p.join()

output = np.array(output)

# df = pd.DataFrame(output, columns = ['dataset','genotype','reporter', 'plant', 'shift-0', 'shift-1', 'shift-2', 'init', 'neigh', 'value'])
np.save(f'{args.output}/{basename}{args.suffix}.npy', output)
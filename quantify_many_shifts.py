#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 10:37:07 2022

@author: henrikahl
"""

import os
import sys
import numpy as np
import argparse
import subprocess
from imgmisc import mkdir
from imgmisc import listdir
from itertools import product
from multiprocessing import Pool


HOME = os.path.expanduser("~")
pypath_default = f'{HOME}/.local/anaconda3/envs/py38/bin/python'
qpath_default = f'{HOME}/projects/quantify.py'

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('--seg_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--pin_input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--xrange', type=float, default=[0, 0, 1], nargs=3)
parser.add_argument('--yrange', type=float, default=[0, 0, 1], nargs=3)
parser.add_argument('--zrange', type=float, default=[0, 0, 1], nargs=3)
parser.add_argument('--wall_depth', type=int, default=1)        

parser.add_argument('--resolution', type=float, nargs=3, default=None)
# parser.add_argument('--suffix', type=str, default='-divergence_angle_data')
parser.add_argument('--segmented', action='store_true')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--pypath', type=str, default=pypath_default)
parser.add_argument('--qpath', type=str, default=qpath_default)

parser.add_argument('--background', type=int, default=0)
parser.add_argument('--fct', type=str, default='sum')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()
# PYPATH = '/home/hpa22/.local/anaconda3/envs/py38/bin/python' if HOME == "/home/hpa22" else '/home/henrikahl/.local/anaconda3/envs/py37/bin/python'
print(args.yrange)
x1 = np.linspace(args.xrange[0], args.xrange[1], int(args.xrange[2]))
x2 = np.linspace(args.yrange[0], args.yrange[1], int(args.yrange[2]))
x3 = np.linspace(args.zrange[0], args.zrange[1], int(args.zrange[2]))


# x1 = np.linspace(0, 10, 10)
# x2 = np.linspace(0, 10, 10)
# x3 = np.linspace(0, 10, 10)

if len(args.seg_input) == 1 and os.path.isdir(os.path.abspath(args.seg_input[0])):
    seg_files = listdir(args.seg_input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    seg_files = args.seg_input
if len(args.pin_input) == 1 and os.path.isdir(os.path.abspath(args.pin_input[0])):
    pin_files = listdir(args.pin_input[0], sorting='natural', include=['.tif', '.tiff',
        '.lsm', '.czi'])
else:
    pin_files = args.pin_input
    
mkdir(args.output)

if len(seg_files) == 1 and len(pin_files) == 1:
    files = [[seg_files[0], pin_files[0]]]
else: 
    files = list(zip(seg_files, pin_files))

    # from imgmisc import listdir    
    # seg_files = listdir('/home/henrikahl/220621', include='-refined', sorting='natural')
    # pin_files = listdir('/home/henrikahl/220621', include='-C1-', sorting='natural')
    
all_args = list(product(*[files, x1, x2, x3]))

print(files)
bname = lambda x: os.path.basename(os.path.splitext(x)[0])
def run(input_args):
    
    sfname, pfname = input_args[0]
    xshift, yshift, zshift = input_args[1:]
    if args.verbose:
        print(f'Now running {bname(sfname)}')


    # print(x11, x21)
    basename = f'{bname(sfname)}-wall_depth_{args.wall_depth}-shift_{xshift}_{yshift}_{zshift}.init'
    print(f'Running {basename}')
    if os.path.isfile(f'{args.output}/{basename}'):
        print(f'File {basename} exists. Breaking')
        return
    
    # print(input_args)

    subprocess.run([args.pypath,
                    args.qpath,
                    "--seg_input",
                    sfname,
                    "--pin_input",
                    pfname,
                    "--fct",
                    "sum",
                    "--shift",
                    f"{xshift}",
                    f"{yshift}",
                    f"{zshift}",
                    "--wall_depth",
                    f"{args.wall_depth}",
                    "--output",
                    args.output,
                    "--skip_done"
                    ])

if len(sys.argv) == 1:
    n_cores = 1
else:
    n_cores = int(args.parallel_workers)
p = Pool(n_cores)
p.map(run, all_args)
p.close()
p.join()    

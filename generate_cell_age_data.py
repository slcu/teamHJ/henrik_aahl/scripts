import os
import pickle
import re

import numpy as np
import pandas as pd
import tifffile as tiff
from imgmisc import bname, find_neighbors, get_layers, listdir
from loguru import logger
from tqdm import tqdm
import argparse
from imgmisc import parse_input_files
from imgmisc import parse_hstr

script_desc = """
Script to generate cell age data from segmented files and tracking data.\n\n

The output DataFrame contains the following information:\n\n

    dataset : The dataset name. Typically the acquisition date, e.g. 220403\n
    genotype : The genotype of the data. E.g. WT, Col-0, csi1, pin1. Can also
        be used to denote the treatment conditions, e.g. NPA, BAP, etc.
    reporters : The reporter combination used, if any. E.g. PI, WUSGFP\n
    plant : The plant index, a scalar.\n
    time : The time if part of a timelapse, a scalar.\n
    label : The cell ID in the segmented image, an integer.\n
    age : The age of a given cell, defined as the number of hours since the last
        division. A floating point scalar.\n
    will_divide : Whether a cell will divide in the span between the current and 
        the next timepoint, a boolean scalar, i.e. 0 or 1\n
    just_divided : Whether a cell divided in the time since the previous timepoint,
        a boolean scalar, i.e. 0 or 1\n
    lineage : The cell lineage ID, a scalar\n
    branch : The branch ID, a scalar

"""
parser = argparse.ArgumentParser(description=script_desc)
parser.add_argument("--seg_input", type=str, nargs="+", help="Input segmentation files or directory")
parser.add_argument("--tracking_input", type=str, nargs="+", help="Input tracking files or directory")
parser.add_argument('--tracking_score_threshold', type=float, default=0.3, help='The DICE score threshold for considering two labels as well-tracked. Discards cells under this limit. Defaults to 0.3.')
parser.add_argument('--discard_file_threshold', type=float, default=0.8,
        help='Scoring threshold for the 90 percent quantile of non-background cells. If this measure is lower than the threshold, this file is disregarded. Defaults to 0.8.')
parser.add_argument('--output', type=str,
                    help='Output name', default=f'{os.path.curdir}/cell_age_data.csv')
parser.add_argument('--background', type=int, default=0, help='The background label. Defaults to 0.')
parser.add_argument("--verbose", action="store_true", help='Verbose flag. Defaults to False.')
parser.add_argument("--skip_done", action="store_true", help='Whether to skip already done files in the output directory. Defaults to False.')
parser.add_argument("--parallel_workers", type=int, default=1, help='The number of parallel workers. Defaults to 1.')
args = parser.parse_args()

BACKGROUND = args.background
TRACKING_SCORE_THRESHOLD = args.tracking_score_threshold
DISCARD_FILE_THRESHOLD = args.discard_file_threshold

seg_files = parse_input_files(args.seg_input, include='.tif')
tracking_files = parse_input_files(args.tracking_input, include='.csv')

iterator = 0
tfile = tracking_files[iterator]
ages = pd.DataFrame(columns=[
    "dataset",
    "genotype",
    "reporters",
    "plant",
    "time",
    "label",
    "age",
    "will_divide",
    "just_divided",
    "lineage",
    "branch",
])

def df_filter(df, **kwargs):
    """ Filter a DataFrame with a query """
    query = qstr(**kwargs)
    return df.query(query)

def qstr(**kwargs):
    """
    Generate an exclusive identity query string from named input values.

    Returns
    -------
    query : str
        The query string.

    Example
    -------
    >>> dataset, reporters, plant, from_, cell = 221122, 'pUBQ10', 1, 4, 6113
    >>> qstr(dataset=dataset, reporters=reporters, plant=plant, time=from_, label=cell)
    >>> Out[1]: 'dataset==221122 & reporters=="pUBQ10" & plant==1 & time==4 & label==6113'

    """
    query_list = []
    for key, value in kwargs.items():
        if value is not None:
            val = kwargs[key]
            val_str = f'"{val}"' if isinstance(val, str) else f'{str(val)}'
            query_list.append(f'{key}=={val_str}')
    query = ' & '.join(query_list)
    return query

def substr_filter(pattern, list_):
    res = [x for x in list_ if re.search(pattern, bname(x))]
    return res
    
for tfile in tqdm(tracking_files, disable=(not args.verbose)):
    infostr = parse_hstr(tfile)
    dataset, reporters, genotype, plant, from_, to_ = infostr
    dataset, plant, from_, to_ = int(dataset), int(plant), int(from_), int(to_)

    plant_tracking_files = substr_filter(f"plant_{plant}-", tracking_files)
    all_froms = [int(re.findall("(\d+)h", bname(ff))[0]) for ff in plant_tracking_files]
    all_tos = [int(re.findall("(\d+)h", bname(ff))[1]) for ff in plant_tracking_files]

    prev_seg_file = substr_filter(f"plant_{plant}-{from_}h", seg_files)[0]
    new_seg_file = substr_filter(f"plant_{plant}-{to_}h", seg_files)[0]
    
    prev_data = tiff.imread(prev_seg_file)
    prev_cells = np.unique(prev_data)
    prev_cells = prev_cells[prev_cells != BACKGROUND]

    if from_ == all_froms[0]:
        n_prev = len(prev_cells)
        prev_ages = pd.DataFrame(
            {
                "dataset":       [dataset] * n_prev,
                "reporters":     [reporters] * n_prev,
                "genotype":      [genotype] * n_prev,
                "plant":         [plant] * n_prev,
                "time":          [from_] * n_prev,
                "label":         prev_cells,
                "age":           [np.nan] * n_prev,
                "just_divided":  [np.nan] * n_prev,
                "will_divide":   [np.nan] * n_prev,
                "lineage":       np.arange(n_prev),
                "branch":        [0] * n_prev,
            }
        )
        ages = ages.append(prev_ages, ignore_index=True)

    # Read in the cells in the next timepoint
    new_data = tiff.imread(new_seg_file)
    new_cells = np.unique(new_data)
    new_cells = new_cells[new_cells != BACKGROUND]

    # Load the tracking data and remove the cells which have too low tracking scores
    tdata = pd.read_csv(tfile, sep="\t")
    tdata_orig = tdata.copy()
    tdata = tdata.query('(cell_t1 != @BACKGROUND) & (cell_t2 != @BACKGROUND)')
    
    excluded = tdata.query('dice_score <= @tracking_score_threshold')["cell_t1"]
    tdata = tdata.query('dice_score > @tracking_score_threshold')

    # Get the scoring maps
    tmaps = tdata[["cell_t1", "cell_t2"]].values
    tscores = tdata["dice_score"].values

    # If the scores for the best tracked cells are too low, the tracking has failed
    # and we must discard this timepoint
    best_tracked_score = lambda x: np.quantile(x[(x > TRACKING_SCORE_THRESHOLD) & (x < 1)], 0.9)
    if best_tracked_score(tscores) < DISCARD_FILE_THRESHOLD:
        logger.info(f"File {bname(tfile)} too poorly tracked. Abandoning.")
        n_new_cells = len(new_cells)
        new_ages = pd.DataFrame(
            {
                "dataset":      [dataset] * n_new_cells,
                "reporters":    [reporters] * n_new_cells,
                "genotype":     [genotype] * n_new_cells,
                "plant":        [plant] * n_new_cells,
                "time":         [to_] * n_new_cells,
                "label":        new_cells,
                "age":          [np.nan] * n_new_cells,
                "will_divide":  [np.nan] * n_new_cells,
                "just_divided": [np.nan] * n_new_cells,
                "lineage":      [np.arange(ages.lineage.max() + 1, ages.lineage.max() + 1 + n_new_cells)],
                "branch":       [0] * n_new_cells
            }
        )
        ages = ages.append(new_ages, ignore_index=True)
        continue

    # Figure out corresponding sister pairs
    tracked_prev_id, counts = np.unique(tmaps[:, 0], return_counts=True)

    will_divide = tracked_prev_id[counts > 1]
    wont_divide = tracked_prev_id[counts == 1]

    # TODO this rewrite
    for cell in will_divide:
        cell_daughters = tmaps[tmaps[:, 0] == cell, 1]
        cell_daughter_scores = tscores[tmaps[:, 0] == cell]
        cell_daughters = np.sort(cell_daughters)

        mother_lineage = df_filter(ages, dataset=dataset, reporters=reporters, genotype=genotype,
                                   plant=plant, time=from_, label=cell).lineage.iloc[0]
        lineage_maxbranch = df_filter(ages, dataset=dataset, reporters=reporters, genotype=genotype,
                                      plant=plant, time=from_, lineage=mother_lineage).branch.max()

        # if too many cells matched, avoid
        if len(cell_daughters) > 2 or any(~np.isin(cell_daughters, new_cells)):

            n_daughters = sum(np.isin(cell_daughters, new_cells))
            new_ages = pd.DataFrame(
                {
                    "dataset": [dataset] * n_daughters,
                    "reporters": [reporters] * n_daughters,
                    "genotype": [genotype] * n_daughters,
                    "plant": [plant] * n_daughters,
                    "time": [to_] * n_daughters,
                    "label": cell_daughters[np.isin(cell_daughters, new_cells)],
                    "age": [np.nan] * n_daughters,
                    "will_divide": [np.nan] * n_daughters,
                    "just_divided": [np.nan] * n_daughters,
                    "lineage": [mother_lineage] * n_daughters,
                    "branch": np.arange(0, n_daughters)
                }
            )
        else:
            n_daughters = len(cell_daughters)
            new_ages = pd.DataFrame(
                {
                    "dataset": [dataset] * n_daughters,
                    "reporters": [reporters] * n_daughters,
                    "genotype": [genotype] * n_daughters,
                    "plant": [plant] * n_daughters,
                    "time": [to_] * n_daughters,
                    "label": cell_daughters,
                    "age": [(to_ - from_) / 2] * n_daughters,
                    "will_divide": [np.nan] * n_daughters,
                    "just_divided": [1] * n_daughters,
                    "lineage": [mother_lineage] * n_daughters,
                    "branch": np.arange(lineage_maxbranch + 1, lineage_maxbranch + 1 + n_daughters)
                }
            )
            ages.loc[ages.eval(qstr(dataset=dataset, reporters=reporters, plant=plant, time=from_, label=cell)), 'will_divide'] = 1
        ages = ages.append(new_ages, ignore_index=True)

    # Propagate the ages we knew from last time point
    prev_ages = ages.query(qstr(dataset=dataset, reporters=reporters, genotype=genotype,
                          plant=plant, time=from_) + ' & label.isin(@wont_divide)')[['label', 'age', 'lineage', 'branch']]
    new_ids = [df_filter(tdata, plant=plant, t1=from_, t2=to_, cell_t1=lab).cell_t2.iloc[0] for lab in prev_ages.label]

    new_ages = pd.DataFrame(
        {
            "dataset": [dataset] * len(new_ids),
            "reporters": [reporters] * len(new_ids),
            "genotype": [genotype] * len(new_ids),
            "plant": [plant] * len(new_ids),
            "time": [to_] * len(new_ids),
            "label": new_ids,
            "age": prev_ages["age"] + (to_ - from_),
            "will_divide": [np.nan] * len(new_ids),
            "just_divided": [0] * len(new_ids),
            "lineage": prev_ages["lineage"],
            "branch": prev_ages["branch"],
        }
    )
    ages = ages.append(new_ages, ignore_index=True)

    # Assign nan data to the cells that haven't been tracked well
    well_tracked_labels = df_filter(ages, dataset=dataset, reporters=reporters, 
                                    genotype=genotype, plant=plant, time=to_).label
    new_cells_left = new_cells[~np.isin(new_cells, well_tracked_labels)]
    
    # TODO - rewrite this train wreck
    mother_cells = [tdata_orig.query(qstr(dataset=dataset, reporters=reporters, genotype=genotype, plant=plant, t2=to_, cell_t2=cell)).cell_t1.iloc[0] for cell in new_cells_left]
    prev_lineages = []
    branches = []
    lin_maxbr = dict()
    maxlin = ages.lineage.max()
    for mc in mother_cells:
        try:
            mc_lin = df_filter(ages, dataset=dataset, reporters=reporters, genotype=genotype, plant=plant, time=from_, label=mc).lineage.iloc[0]
            br = df_filter(ages, lineage=mc_lin).branch.max() + lin_maxbr.get(mc_lin, 0) + 1
            lin_maxbr[mc_lin] = lin_maxbr.get(mc_lin, 0) + 1
            branches.append(br)
            prev_lineages.append(mc_lin)
        except:
            prev_lineages.append(maxlin + 1)
            branches.append(0)
            maxlin = maxlin + 1
    
    new_ages = pd.DataFrame(
        {
            "dataset": [dataset] * len(new_cells_left),
            "reporters": [reporters] * len(new_cells_left),
            "genotype": [genotype] * len(new_cells_left),
            "plant": [plant] * len(new_cells_left),
            "time": [to_] * len(new_cells_left),
            "label": new_cells_left,
            "age": [np.nan] * len(new_cells_left),
            "will_divide": [np.nan] * len(new_cells_left),
            "just_divided": [np.nan] * len(new_cells_left),
            "lineage": prev_lineages, #np.arange(ages.lineage.max() + 1, ages.lineage.max() + 1 + len(new_cells_left)),
            "branch": branches #[0] * len(new_cells_left),
        }
    )
    ages = ages.append(new_ages, ignore_index=True)

df = pd.DataFrame(ages)
df = df.convert_dtypes()
df.to_csv(f"{args.output}", index=False, sep="\t")

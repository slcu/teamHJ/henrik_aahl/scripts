#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 11:29:32 2020

@author: henrikahl
"""

import re
import os
import sys
import copy
import numba
import argparse
import numpy as np
import pyvista as pv
from numba import njit
from imgmisc import mkdir
from itertools import product
from multiprocessing import Pool
from imgmisc import listdir, mkdir
from multiprocessing import cpu_count
from scipy.integrate import solve_ivp
from img2org.conversion import read_neigh_init, read_init
from img2org.conversion import write_neigh_init
from multiprocessing import Pool, cpu_count
#from scipy.integrate import LSODA
#from scikits.odes import ode

from xvfbwrapper import Xvfb
vdisplay = Xvfb(width=1920, height=1080)
vdisplay.start()

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('--init', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--neigh', type=str, nargs='+',
                    help='Input files or directory')
# parser.add_argument('--output', type=str,
                    # help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--end_state_dir', type=str,
                    help='', default=None)
parser.add_argument('--figure_dir', type=str,
                    help='', default=None)
parser.add_argument('--prod_case', type=int, default=0)
parser.add_argument('--default_prod', type=float, default=.1)
parser.add_argument('--default_deg', type=float, default=.1)
parser.add_argument('--smooth_iterations', type=int, default=0)
parser.add_argument('--active_influx_order', type=float, default=0)
parser.add_argument('--active_efflux_order', type=float, default=0)
parser.add_argument('--scaling', type=float, default=None)
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.init) == 1 and os.path.isdir(os.path.abspath(args.init[0])):
    init_files = listdir(args.init[0], sorting='natural', include=['.init'])
else:
    init_files = args.init
if len(args.neigh) == 1 and os.path.isdir(os.path.abspath(args.neigh[0])):
    neigh_files = listdir(args.neigh[0], sorting='natural', include=['.neigh'])
else:
    neigh_files = args.neigh

# TODO: Cover all cases
if len(init_files) == 1 and len(neigh_files) == 1:
    files = [[init_files[0], neigh_files[0]]]
else: 
    files = list(zip(init_files, neigh_files))
# mkdir(args.output)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])

def to_nested_np(arr):  
            return np.array([np.array(val) for val in arr])
        
def make_2D_array(lis):
    """Funciton to get 2D array from a list of lists
    """
    n = len(lis)
    lengths = np.array([len(x) for x in lis])
    max_len = np.max(lengths)
    arr = np.full((n, max_len), np.nan)

    for i in range(n):
        arr[i, :lengths[i]] = lis[i]
    return arr, lengths

volume_index = 3
auxin_index = 6

# args = [[iter_, 0, 0, 0]]
# args = args[0]
# file_index, xx, yy, prod = args

def run(files):
    init_fname, neigh_fname = files
    basename = bname(init_fname)
    print(f'Initiating run on {basename}')

    # shift = np.array(re.findall(f'shift_(\S+)_(\S+)_(\S+).init', init_fname), dtype='float')[0]
    if args.skip_done:
        done_files = listdir(args.figure_dir, include='.png')
        if any([basename in ff for ff in done_files]):
            print(f'Skipping {basename} - already done')
            return
        done_files = listdir(args.end_state_dir, include='.init')
        if any([basename in ff for ff in done_files]):
            print(f'Skipping {basename} - already done')
            return
    
    # Iteration specific parameters
    # For derivation, see Modeling Auxin Transport and Plant Development (2006)
    # https://link.springer.com/article/10.1007/s00344-006-0066-x/tables/1
    De = 2.2e-03                    # passive efflux (p_AH f^cell_AH) : 2.2e-03
    Te = 1.26e-00 * 10**(float(args.active_efflux_order)) # PIN mediated efflux: 1.26e-00 p_PIN fcell_a- N+
    Di = 1.32e-01                   # passive influx (p_AH f^wall_AH) : 1.32e-01 
    Ti = 1.96e-00 * 10**(float(args.active_influx_order)) # AUX mediated influx : 1.96e-00 p_AUX * fwall_a- * N+

    if args.scaling is not None:
        De *= 10**args.scaling
        Di *= 10**args.scaling
        Te *= 10**args.scaling
        Ti *= 10**args.scaling
     
    # Extract values from data
    init = read_init(init_fname)
    volumes = init[volume_index].values
    auxin = init[auxin_index].values.copy()
    
    in_l1 = init[7]
    in_l2 = init[8]
    in_bottom = init[9]
    in_l1 = in_l1.index[in_l1 == 1].values
    in_l2 = in_l2.index[in_l2 == 1].values
    in_bottom = in_bottom.index[in_bottom == 1].values
    print(f'In bottom: {len(in_bottom)}')
    print(f'In init: {len(init)}')
    # not_in_bottom = in_bottom.index[in_bottom != 1].values
    
    neigh_data = read_neigh_init(neigh_fname)
    neighs, auxs, pins, npins, nauxs, areas, ninl1, ninl2, ninbottom = [], [], [], [], [], [], [], [], []
    del neigh_data['n_cells'], neigh_data['n_params']
    for key, val in neigh_data.items():
        neighs.append(list(val['neighs'].astype(int)))
        ninl1.append(list(np.isin(val['neighs'], in_l1)))
        ninl2.append(list(np.isin(val['neighs'], in_l2)))
        areas.append(list(val['p0'].astype(float)))
        pins.append(list(val['p1'].astype(float)))
        auxs.append(list(val['p2'].astype(float)))
        npins.append(list(np.array([float(neigh_data[nn]['p1'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))
        nauxs.append(list(np.array([float(neigh_data[nn]['p2'][neigh_data[nn]['neighs'] == key]) for nn in val['neighs']]).astype(float)))
        ninbottom.append(list(np.isin(val['neighs'], in_bottom)))
    n_neighs = np.array([sum(np.array(nn) > 0) for nn in neighs])

    # Ensure comprehensible formats for function defintion
    ntest, nlens = make_2D_array(neighs)
    pins, nlens = make_2D_array(pins)
    auxs, nlens = make_2D_array(auxs)
    npins, nlens = make_2D_array(npins)
    nauxs, nlens = make_2D_array(nauxs)
    areas, nlens = make_2D_array(areas)
    ninl1, nlens = make_2D_array(ninl1)
    ninl2, nlens = make_2D_array(ninl2)
    ninbottom, nlens = make_2D_array(ninbottom)
    ntest = ntest.astype(int)
    fluxes = np.zeros_like(pins)
    
    # Normalise data
    normalisation_factor = np.nanmean([np.nansum(pp) for pp in pins])
    auxin = auxin / normalisation_factor 
    pins = pins / normalisation_factor 
    auxs = auxs / normalisation_factor 
    npins = npins  / normalisation_factor 
    nauxs = nauxs / normalisation_factor 
    
    # Define the transport function. Using @njit speeds things up massively 
    # (although it does limit our formalism a little by preventing us from 
    # using nested arrays, which is why we convert things to 2D arrays above.)
    # hill_peripheral = lambda x: x.astype(float)**12 / (60**12 + x.astype(float)**12)
    def hill_peripheral(x): 
        return x.astype(float)**12 / (60**12 + x.astype(float)**12)
    # hill_central = lambda x: 1. - x**12 / (30**12 + x**12)
    def hill_central(x):
        return 1. - x**12 / (30**12 + x**12)

    p_auxin = .1 if args.prod_case == 1 else 0
    d_auxin = .1 if args.prod_case in [1, 2, 3, 4] else 0
    p_auxin_auxin = .1 if args.prod_case == 2 else 0
    p_auxin_peripheral = (.1 * hill_peripheral(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if args.prod_case == 3 else np.zeros(init.shape[0])).astype(float)
    p_auxin_central = (.1 * hill_central(((init.loc[:, 1:2].values - init.loc[:, 1:2].mean(0).values)**2).sum(1)**.5) if args.prod_case == 4 else np.zeros(init.shape[0])).astype(float)
    
    # If we're in one of the regional production phenotypes, we should adjust 
    # the production so that the total production in the tissue remains the same
    total_prodiction_WT = sum(.1 * volumes)
    if args.prod_case == 3:
        p_auxin_peripheral = p_auxin_peripheral / (sum(p_auxin_peripheral * volumes) / total_prodiction_WT)
    elif args.prod_case == 4:
        p_auxin_central = p_auxin_central / (sum(p_auxin_central * volumes) / total_prodiction_WT)
    
    print(p_auxin, p_auxin_auxin, p_auxin_peripheral.sum(),
            p_auxin_central.sum(), d_auxin)
    
    #basal_efflux = 1 if args.prod_case in [1,2,3,4] else 0
    @njit(parallel=True, fastmath=True)
    #def cell_cell_auxin_transport(t, values, output):
    def cell_cell_auxin_transport(t, values, fluxes):
        output = np.zeros(values.shape[0])       
        print(t)
        for ii in numba.prange(values.shape[0]):
            total_bottom_flux = .0
            cell_sum = .0
            for jj, nei in enumerate(ntest[ii, :nlens[ii]]):
                flux = areas[ii, jj] * ((Di + Ti *  auxs[ii, jj]) * (De + Te * npins[ii, jj]) * values[nei] - 
                                        (Di + Ti * nauxs[ii, jj]) * (De + Te *  pins[ii, jj]) * values[ii]) / (2 * Di + Ti * (auxs[ii, jj] + nauxs[ii, jj]))
                bottom_flux = areas[ii,jj] * De * values[ii] / n_neighs[ii]**2 / (2 * Di + Ti * (auxs[ii, jj] + nauxs[ii, jj]))
                flux = flux - bottom_flux
                total_bottom_flux += bottom_flux
                fluxes[ii, jj] = flux
                cell_sum += flux
            cell_sum -= total_bottom_flux
            output[ii] = 1 / volumes[ii] * cell_sum + ( 
                # case 0: no production & degradation
                p_auxin +                       # case 1: global production
                p_auxin_auxin * values[ii]**2 + # case 2: auxin self-activation
                p_auxin_peripheral[ii] +        # case 3: peripheral auxin production ("from flowers")
                p_auxin_central[ii] -           # case 4: central auxin production ("from CZ")
                d_auxin * values[ii])           # happens in all cases but 0
        output[output < 0] = 0
        output[np.isnan(output)] = 0
        output[np.isinf(output)] = 0        
        return output
    
    # Solve!
    y0 = auxin.copy()
    t_res = solve_ivp(cell_cell_auxin_transport, y0=y0, t_span=(0, 10**0),
            method='RK45', args=(fluxes, ))#, rtol=1e-9, atol=1e-12)

    # Collect data we want to save    
    output = init.copy()
    # output[auxin_index] = t_res.y
    
    #output[auxin_index] = solution.values.y[:,0]
    output[auxin_index] = t_res.y[:, -1]
    print(f"Auxin before: {(auxin * init[volume_index]).sum()}")
    print(f"Auxin after: {(output[auxin_index] * output[volume_index]).sum()}")
    print(f"Auxin diff: {(output[auxin_index] * output[volume_index]).sum() - (auxin * init[volume_index]).sum()}")

    corr = np.corrcoef(output[auxin_index].values, output[auxin_index-1].values)[0,1]
    
    l1l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    l1l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l1 else 0 for ii in np.arange(fluxes.shape[0])])
    l2l2_fluxes = np.array([sum(fluxes[ii][ninl2[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 
    l2l1_fluxes = np.array([sum(fluxes[ii][ninl1[ii] == 1]) if ii in in_l2 else 0 for ii in np.arange(fluxes.shape[0])]) 

    output[10] = fluxes.sum(1)
    output[11] = l1l1_fluxes
    output[12] = l1l2_fluxes
    output[13] = l2l2_fluxes
    output[14] = l2l1_fluxes

    # Save output to file
    if args.end_state_dir is not None:
        mkdir(args.end_state_dir)
        basename = bname(init_fname)
        fname_out = f'{args.end_state_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-cells_end_state.dat'
        output.to_csv(fname_out, header=False, sep='\t', index=False, float_format='%.15f')
    
        fname_out = f'{args.end_state_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-walls_end_state.dat'
        print(f'Printing {fname_out}')
        neigh_output = neigh_data.copy()
        for ii in range(len(neigh_output)):
            neigh_output[list(neigh_output.keys())[ii]]['p3'] = fluxes[ii][:nlens[ii]]
        neigh_output['n_cells'] = len(output)
        neigh_output['n_params'] = 4
        write_neigh_init(fname_out, neigh_output)        
    
    if args.figure_dir is not None:
        mkdir(args.figure_dir)
        values = output[auxin_index].values
        for iter_ in range(args.smooth_iterations):
            values = np.array([np.mean((values[ii] + values[neighs[ii]].sum()) / (len(neighs[ii]) + 1)) for ii in range(len(values))])
        
        p = pv.Plotter(notebook=False, off_screen=True)
        p.set_background('white')
        sph = pv.Sphere(radius=5, phi_resolution=15, theta_resolution=15)
        mesh = pv.PolyData(output[[0,1,2]].values)
        mesh['scalars'] = values
        mesh = mesh.glyph(scale=False, geom=sph)
        p.add_mesh(mesh, cmap='turbo', smooth_shading=True, clim=[np.quantile(values, 0.001), np.quantile(values, 0.999)])
        p.view_yz(True if '.lif' in init_fname else False)
        p.set_background('white')
        p.screenshot(f'{args.figure_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-corr_{corr}-auxin_top.png',
                       transparent_background=False, window_size=[2000, 2000])
        p.close()
        p.deep_clean()
    
        p = pv.Plotter(notebook=False, off_screen=True)
        p.set_background('white')
        sph = pv.Sphere(radius=5, phi_resolution=15, theta_resolution=15)
        mesh = pv.PolyData(output[[0,1,2]].values)
        mesh['scalars'] = values
        mesh = mesh.glyph(scale=False, geom=sph)
        p.add_mesh(mesh, cmap='turbo', smooth_shading=True, clim=[np.quantile(values, 0.001), np.quantile(values, 0.999)])
        p.view_yz(False if '.lif' in init_fname else True)
        p.screenshot(f'{args.figure_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-corr_{corr}-auxin_bottom.png',
                       transparent_background=False, window_size=[2000, 2000])
        p.close()
        p.deep_clean()
    
        # PIN1
        values = output[auxin_index-1].values
        for iter_ in range(args.smooth_iterations):
            values = np.array([np.mean((values[ii] + values[neighs[ii]].sum()) / (len(neighs[ii]) + 1)) for ii in range(len(values))])
        
        p = pv.Plotter(notebook=False, off_screen=True)
        p.set_background('white')
        sph = pv.Sphere(radius=5, phi_resolution=15, theta_resolution=15)
        mesh = pv.PolyData(output[[0,1,2]].values)
        mesh['scalars'] = values
        mesh = mesh.glyph(scale=False, geom=sph)
        p.add_mesh(mesh, cmap='turbo', smooth_shading=True, clim=[np.quantile(values, 0.001), np.quantile(values, 0.999)])
        p.view_yz(True if '.lif' in init_fname else False)
        p.screenshot(f'{args.figure_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-corr_{corr}-pin_top.png',
                       transparent_background=False, window_size=[2000, 2000])
        p.close()
        p.deep_clean()
    
        p = pv.Plotter(notebook=False, off_screen=True)
        p.set_background('white')
        sph = pv.Sphere(radius=5, phi_resolution=15, theta_resolution=15)
        mesh = pv.PolyData(output[[0,1,2]].values)
        mesh['scalars'] = values
        mesh = mesh.glyph(scale=False, geom=sph)
        p.add_mesh(mesh, cmap='turbo', smooth_shading=True, clim=[np.quantile(values, 0.001), np.quantile(values, 0.999)])
        p.view_yz(False if '.lif' in init_fname else True)
        p.screenshot(f'{args.figure_dir}/{basename}-pe_{args.active_efflux_order}-pi_{args.active_influx_order}-prod_{int(args.prod_case)}-corr_{corr}-pin_bottom.png',
                       transparent_background=False, window_size=[2000, 2000])
        p.close()
        p.deep_clean()

n_cores = args.parallel_workers
print(f'Using {n_cores} cores')
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()

vdisplay.stop()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 14:27:26 2020

@author: henrik
"""
import os
import gc
import sys
import argparse
import numpy as np
import pyvista as pv
import czifile as cz
import tifffile as tiff
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import find_neighbors
from imgmisc import get_resolution
from scipy.ndimage import center_of_mass
from multiprocessing import Pool

pv.set_plot_theme('document')

from xvfbwrapper import Xvfb
vdisplay = Xvfb(width=1920, height=1080)
vdisplay.start()


parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--cmap', type=str, default='tab20')
parser.add_argument('--resolution', type=float, nargs=3, default=None)

parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff'])
else:
    files = args.input
mkdir(args.output)

bname = lambda x: os.path.basename(os.path.splitext(x)[0])
def run(fname):
    if args.verbose:
        print(f'Now running {fname}')
    
    basename = bname(fname)
    if args.skip_done:
        done_files = listdir(args.output, include='.png')
        init_oname = f'{args.output}/{basename}-segmentation_top.png'
        if any([init_oname == ff for ff in done_files]):
            print('Skipping {basename} - already done')
            return
        
    try:
        data = tiff.imread(fname)
    except:
        try:
            data = cz.czifile(fname)
        except:
            raise Exception(f'Input file type for {fname} not supported.')
    data = np.squeeze(data)
    if data.ndim < 3:
        raise Exception('Too few channels')
    if data.ndim == 4 and args.channel is not None:
        data = data[:, args.channel]
    if args.resolution is None:
        resolution = np.asarray(get_resolution(fname))
        if resolution[0] < 1e-3:
            resolution = np.array(resolution) * 1e6
    else:
        resolution = args.resolution
    seg_img = data #tiff.imread(fname)

    # Identify cell neighbours and label so that adjacent cells don't have the
    # same value
    neighs = find_neighbors(seg_img, background=0)
    mapping = np.full(len(neighs), -1)
    for cell_id, cell_neighs in neighs.items():
        arr = np.append(cell_id, cell_neighs)
        labelled = mapping[arr-1] != -1
        label = 1
        while True:
            if label not in mapping[arr[labelled] - 1]:
                mapping[cell_id - 1] = label
                break
            label += 1

    vis_img = np.zeros_like(seg_img)
    for vv, label in enumerate(np.unique(seg_img)[1:]):
        vis_img[seg_img == label] = mapping[vv]

    # Plot!
    # Full
    cmap = args.cmap
    p = pv.Plotter(off_screen=True, notebook=False)
    p.add_volume(vis_img, cmap=cmap, opacity=[
                 0] + [1] * 254, show_scalar_bar=False, shade=True, diffuse=.9, resolution=resolution)
    p.view_yz()
    p.set_background('white')
    p.screenshot(f'{args.output}/{basename}-segmentation_top.png',
                 transparent_background=True, window_size=[2000, 2000])
    p.close()

    center = np.round(center_of_mass(vis_img > 0)).astype(int)
    vis_img = vis_img[:, :, :center[1]]
    vis_img = np.pad(vis_img, 1)

    p = pv.Plotter(off_screen=True, notebook=False)
    p.add_volume(vis_img.T, cmap=cmap, opacity=[
                 0] + [1] * 254, show_scalar_bar=False, shade=True, diffuse=.9, resolution=np.array(resolution)[::-1])
    p.view_yz()
    p.set_background('white')
    p.screenshot(f'{args.output}/{basename}-segmentation_side.png',
                 transparent_background=True, window_size=[2000, 2000])
    p.close()

    # we need to remove each actor...
    # https://github.com/pyvista/pyvista/issues/482
    p.clear()
    for ren in p.renderers:
        for actor in list(ren._actors):
            ren.remove_actor(actor)

    del p, vis_img, seg_img
    gc.collect()

n_cores = int(args.parallel_workers)
p = Pool(n_cores)
output = p.map(run, files)
p.close()           
p.join()

vdisplay.stop()

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 20 10:57:53 2023

@author: henrikahl
"""

import argparse
import itertools
import os
from multiprocessing import Pool
from time import time

import numpy as np
import tifffile as tiff
from dipy.align.imaffine import (
    AffineRegistration,  # AffineMap,
    MutualInformationMetric,
    transform_centers_of_mass,
)
from dipy.align.transforms import (
    AffineTransform3D,
    RigidTransform3D,
    TranslationTransform3D,
)
from loguru import logger
from scipy.ndimage import distance_transform_edt, rotate
from skimage.transform import resize
from tqdm import tqdm

from imgmisc import (
    bname,
    get_resolution,
    listdir,
    match_shape,
    mkdir,
    parse_hstr,
    parse_input_files,
)

parser = argparse.ArgumentParser(description="")
parser.add_argument(
    "--from_files", type=str, nargs="+", help="Input files or directory"
)
parser.add_argument("--to_files", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output", type=str, help="Output directory", default=f"{os.path.curdir}"
)

parser.add_argument("--resolution", type=float, nargs=3, default=None)
parser.add_argument("--time", action="store_true")

parser.add_argument("--suffix", type=str, default="-cell_data")
parser.add_argument("--verbose", action="store_true")
parser.add_argument("--skip_done", action="store_true")
parser.add_argument("--parallel_workers", type=int, default=1)
args = parser.parse_args()

if not args.verbose:
    logger.disable("")

from_files = parse_input_files(args.from_files, include=[".tiff", ".tif"])
to_files = parse_input_files(args.to_files, include=[".tiff", ".tif"])
mkdir(args.output)

all_args = list(itertools.product(from_files, to_files))
all_args = [arg for arg in all_args if arg[0] != arg[1]]

# sfile = files[30]
# mfile = files[70]
def check_done(fname, folder):
    value = bname(fname) in "".join(listdir(folder))
    return value


def run(inputs_):
    start = time()
    sfile, mfile = inputs_
    logger.info("Loading data")

    fname1 = bname(sfile)
    fname2 = bname(mfile)
    d1, r1, g1, p1, t1 = parse_hstr(fname1)
    d2, r2, g2, p2, t2 = parse_hstr(fname2)
    obase = f"{d1}-{r1}-{g1}-plant_{p1}-{t1}h_to_{d2}-{r2}-{g2}-plant_{p2}-{t2}h"

    if sfile == mfile:
        return

    if check_done(obase, args.output):
        logger.info(f"{obase} already done. Breaking...")
        return

    static = tiff.imread(sfile)
    moving = tiff.imread(mfile)

    ### We downscale the images a little to reduce memory consumption
    orig_factor = 6
    static = resize(static, np.asarray(static.shape) // orig_factor, order=0)
    moving = resize(moving, np.asarray(moving.shape) // orig_factor, order=0)
    sres = np.asarray(get_resolution(sfile)) * orig_factor
    mres = np.asarray(get_resolution(mfile)) * orig_factor

    # We want to match the epidermis in particular
    static = static > 0
    moving = moving > 0

    static = distance_transform_edt(static, sampling=sres)
    moving = distance_transform_edt(moving, sampling=mres)

    static[static > 10] = 0
    moving[moving > 10] = 0

    # Let's ensure everything we analyse have the same image size
    # static = match_shape(static, np.c_[static.shape, moving.shape].max(1))
    # moving = match_shape(moving, np.c_[static.shape, moving.shape].max(1))
    static = match_shape(
        static, np.array([450, 1000, 1000]) // orig_factor
    )  # np.c_[static.shape, moving.shape].max(1))
    moving = match_shape(
        moving, np.array([450, 1000, 1000]) // orig_factor
    )  # np.c_[static.shape, moving.shape].max(1))

    logger.info("Setting up...")
    metric = MutualInformationMetric(nbins=256, sampling_proportion=None)
    static_grid2world = np.diag(np.append(sres, 1))
    moving_grid2world = np.diag(np.append(mres, 1))

    # Because meristem registration involves a silly amount of local minima, we
    # need to hack the procedure to test out different starting rotations from
    # which we can then do a proper registration.
    logger.info("Translation registration")
    flips = [True, False]
    rots = np.arange(0, 360, 360 / 16)
    flip_rots = list(itertools.product(flips, rots))

    affreg = AffineRegistration(
        metric=metric, level_iters=[100], sigmas=[0], factors=[4], method="L-BFGS-B"
    )

    scores = []
    for arg in tqdm(flip_rots):

        # For whatever reason, I can't make reflection and rotation work with dipy
        m = moving.copy()
        m = m[..., ::-1] if arg[0] else m
        m = rotate(m, arg[1], axes=(1, 2), reshape=False)

        # Basic translation alignment
        c_of_mass = transform_centers_of_mass(
            static, static_grid2world, m, moving_grid2world
        )
        transform = TranslationTransform3D()
        translation, ps, score = affreg.optimize(
            static,
            m,
            transform,
            None,
            static_grid2world,
            moving_grid2world,
            starting_affine=c_of_mass.affine,
            ret_metric=True,
        )
        # transformed = translation.transform(m)
        scores.append([arg[0], arg[1], ps, score])

    # Apply the best settings
    best_idx = scores[np.argmin([dd[3] for dd in scores])]
    arg = best_idx
    moving = moving[..., ::-1] if arg[0] else moving
    moving = rotate(moving, arg[1], axes=(1, 2), reshape=False)

    logger.info("Finer rigid registration")
    affreg = AffineRegistration(
        metric=metric,
        level_iters=[1000, 100, 10],
        sigmas=[2, 1, 0],
        factors=[2, 2, 1],
        method="L-BFGS-B",
    )
    transmat = TranslationTransform3D().param_to_matrix(arg[2])
    rigid, ps, score = affreg.optimize(
        static,
        moving,
        RigidTransform3D(),
        None,
        static_grid2world,
        moving_grid2world,
        starting_affine=transmat,
        ret_metric=True,
    )
    # tiff.imshow(transformed - static)

    logger.info("Finer affine registration")
    affine, ps, score = affreg.optimize(
        static,
        moving,
        AffineTransform3D(),
        None,
        static_grid2world,
        moving_grid2world,
        starting_affine=rigid.affine,
        ret_metric=True,
    )
    # transformed = affine.transform(moving)
    # tiff.imshow(transformed - static)
    logger.info("Finished!")
    end = time()
    logger.info(f"Time spent: {end - start} seconds")

    oname = f'{obase}-flip_{"flip" if arg[0] else "noflip"}-rot_{arg[1]}'
    oname = f"{oname}-mi_{abs(arg[3]):.12f}"
    oname = f"{args.output}/{oname}.npy"

    np.save(oname, affine.affine, allow_pickle=False)

n_cores = args.parallel_workers
p = Pool(n_cores)
p.map(run, all_args)
p.close()
p.join()


# # Try if this works on the original data - possibly issues with spacing
# sorig = tiff.imread(sfile)
# morig = tiff.imread(mfile)
# sorig = match_shape(sorig, [450, 1024, 1024])
# morig = match_shape(morig, [450, 1024, 1024])
# sorigres = get_resolution(sfile)
# morigres = get_resolution(mfile)
# sorig = (sorig > 0).astype(float)
# morig = (morig > 0).astype(float)


# morig = morig[..., ::-1] if arg[0] else moving
# morig = rotate(morig, arg[1], axes=(1, 2), reshape=False)


# afmap = AffineMap(affine.affine,
#                   domain_grid2world=np.diag(np.append(morigres, 1)),
#                   codomain_grid2world=np.diag(np.append(sorigres, 1)),
#                   domain_grid_shape=morig.shape)
# transformed = afmap.transform(morig,
#                               image_grid2world=np.diag(np.append(morigres, 1)),
#                               sampling_grid_shape=morig.shape)
# tiff.imshow(transformed - sorig)

# morigfixed = fix(morig, arg, sorigres, morigres)
# del morig

# import gc
# gc.collect()
# morigfixed = (morigfixed > 0).astype(float)
# sorig = (sorig > 0).astype(float)
# tiff.imshow(morigfixed - sorig)

# Do we need to do the rigid transform as well?
# transform = AffineTransform3D()
# afmap = AffineMap(affine.affine,
#                   domain_grid2world=np.diag(np.append(morigres, 1)),
#                   codomain_grid2world=np.diag(np.append(sorigres, 1)),
#                   domain_grid_shape=morigfixed.shape)
# transformed = afmap.transform(morigfixed,
#                               image_grid2world=np.diag(np.append(morigres, 1)),
#                               sampling_grid_shape=morigfixed.shape)
# tiff.imshow(sorig - transformed)
# del transformed

# def fix(moving, arg, static_res, moving_res):
#     static_grid2world = np.diag(np.append(static_res, 1))
#     moving_grid2world = np.diag(np.append(moving_res, 1))

#     transform = TranslationTransform3D()
#     afmap = AffineMap(transform.param_to_matrix(arg[2]), # + c_of_mass.affine - np.eye(4, 4),
#         domain_grid2world=moving_grid2world,
#         codomain_grid2world=static_grid2world)

#     moving = moving[..., ::-1] if arg[0] else moving
#     moving = rotate(moving, arg[1], axes=(1, 2), reshape=False)
#     moving = afmap.transform(moving,
#                              image_grid2world=moving_grid2world,
#                              sampling_grid_shape=moving.shape)
#     return moving

# mfixed = fix(moving, best_idx, sres, mres)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 11:53:44 2020

@author: henrik
"""

import argparse
import os
from multiprocessing import Pool

import numpy as np
import pandas as pd
import pyvista as pv
from loguru import logger

import phenotastic.domains as boa
import phenotastic.mesh as mp
from imgmisc import (
    bname,
    create_mesh,
    listdir,
    mkdir,
    parse_image_input,
    parse_input_files,
    parse_resolution,
)

from phenotastic.mesh import remesh, remove_tongues, repair_small, smooth_boundary

parser = argparse.ArgumentParser(description="")

# Required positional argument
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output", type=str, help="Output directory", default=f"{os.path.curdir}"
)

# Optional argument
parser.add_argument("--background", type=int, default=0)
parser.add_argument("--resolution", type=float, nargs=3, default=None)
parser.add_argument("--meristem_method", type=str, default="com")
parser.add_argument("--minmax_iterations", type=int, default=20)
parser.add_argument("--mean_iterations", type=int, default=40)
parser.add_argument("--depth", type=float, default=0.0005)
parser.add_argument("--angle_threshold", type=float, default=12)
parser.add_argument("--bottom_crop", type=float, default=0)
parser.add_argument("--curvature_limits", type=float, default=0.01)
parser.add_argument("--disconnected_threshold", type=float, default=20)
parser.add_argument("--segmented", action="store_true")

parser.add_argument("--hole_repair_threshold", type=float, default=100)
parser.add_argument("--downscaling", type=float, default=0.003125)
parser.add_argument("--upscaling", type=float, default=2)
parser.add_argument("--smooth_iter", type=int, default=200)
parser.add_argument("--smooth_relax", type=float, default=0.01)
parser.add_argument("--tongues_ratio", type=float, default=3)
parser.add_argument("--tongues_radius", type=float, default=10)

parser.add_argument("--plot_input", action="store_true")
parser.add_argument("--plot_curvature", action="store_true")
parser.add_argument("--plot_result_dir", type=str, default=None)
parser.add_argument("--settings_file", type=str, default=None)
parser.add_argument("--suffix", type=str, default="-flower_segmentation")
parser.add_argument("--verbose", action="store_true")
parser.add_argument("--skip_done", action="store_true")
parser.add_argument("--skip_done_plot", action="store_true")
parser.add_argument("--parallel_workers", type=int, default=1)
args = parser.parse_args()

files = parse_input_files(args.input, include=[".tif", ".tiff"])
mkdir(args.output)

if not args.verbose:
    logger.disable("")


def run(fname):
    oname = f"{bname(fname)}{args.suffix}"

    if args.skip_done and (oname in "".join(listdir(args.output))):
        logger.info(f"File {fname} already done. Skipping...")
        return
    elif args.skip_done_plot and (oname in "".join(listdir(args.plot_result_dir))):
        logger.info(f"File {fname} already done. Skipping...")
        return

    # Parse inputs
    logger.info(f"Loading data for {bname(fname)}")
    contour = parse_image_input(fname)
    resolution = parse_resolution(fname, args.resolution)

    # just to make it so all tissue parts touching the image borders don't end up with big gaps
    contour = np.pad(contour, 1)
    contour = contour[1:]

    logger.info(f"Creating and processing mesh {bname(fname)}")
    mesh = create_mesh(contour, resolution)
    del contour

    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    mesh = repair_small(mesh, args.hole_repair_threshold)
    mesh = remesh(mesh, int(mesh.n_points * args.downscaling), sub=0)

    mesh = remove_tongues(
        mesh,
        radius=args.tongues_radius,
        threshold=args.tongues_ratio,
        hole_edges=args.hole_repair_threshold,
    )
    mesh = mesh.extract_largest().clean()

    mesh = repair_small(mesh, args.hole_repair_threshold)
    mesh = mesh.smooth(args.smooth_iter, args.smooth_relax)
    mesh = remesh(mesh, int(args.upscaling * mesh.n_points))
    mesh = smooth_boundary(mesh, args.smooth_iter, args.smooth_relax)

    mesh = mesh.compute_normals(auto_orient_normals=True)
    mesh = mesh.clip(normal="-x", origin=[args.bottom_crop, 0, 0])
    try:
        mesh = mp.correct_normal_orientation_topcut(
            mesh,
            mesh.ray_trace(
                [0] + list(mesh.center[1:]), mesh.center + np.array([9999999, 0, 0])
            )[0][0]
            - np.array([5, 0, 0]),
        )
    except:
        pass
    mesh = mesh.extract_largest()
    mesh = mesh.clean()
    if args.plot_input:
        mesh.plot()

    neighs = mp.vertex_neighbors_all(mesh)

    clim = args.curvature_limits
    mesh["curvature"] = -mesh.curvature("mean")
    mesh["curvature"][mesh["curvature"] < -clim] = -clim
    mesh["curvature"][mesh["curvature"] > clim] = clim
    mesh["curvature"] = boa.minmax(mesh["curvature"], neighs, args.minmax_iterations)
    mesh["curvature"] = boa.set_boundary_values(
        mesh, scalars=mesh["curvature"], values=np.min(mesh["curvature"])
    )
    mesh["curvature"] = boa.mean(mesh["curvature"], neighs, args.mean_iterations)

    if args.plot_curvature:
        mesh.plot(scalars="curvature", cmap="turbo", interpolate_before_map=False)

    mesh["domains"] = boa.steepest_ascent(
        mesh, scalars=mesh["curvature"], neighbours=neighs, verbose=True
    )

    mesh["domains"] = boa.merge_depth(
        mesh,
        domains=mesh["domains"],
        scalars=mesh["curvature"],
        threshold=args.depth,
        verbose=True,
        neighbours=neighs,
        mode="max",
    )

    mesh["domains"] = boa.merge_engulfing(
        mesh, domains=mesh["domains"], threshold=0.6, neighbours=neighs, verbose=True
    )

    neighs = np.array(neighs)
    meristem_index = boa.define_meristem(
        mesh, mesh["domains"], method=args.meristem_method, neighs=neighs
    )

    mesh["domains"] = boa.merge_disconnected(
        mesh,
        domains=mesh["domains"],
        meristem_index=meristem_index,
        threshold=args.disconnected_threshold,
        neighbours=neighs,
        verbose=True,
    )

    meristem_index = boa.define_meristem(
        mesh, mesh["domains"], method=args.meristem_method, neighs=neighs
    )

    mesh["domains"] = boa.merge_angles(
        mesh,
        domains=mesh["domains"],
        meristem_index=meristem_index,
        threshold=args.angle_threshold,
    )

    if args.plot_result_dir is not None:
        p = pv.Plotter(off_screen=True)
        p.add_mesh(
            mesh,
            scalars="domains",
            cmap="Set2",
            interpolate_before_map=False,
        )
        p.view_yz()
        p.screenshot(
            f"{args.plot_result_dir}/{bname(fname)}{args.suffix}.png",
            transparent_background=True,
            window_size=[2000, 2000],
        )
        p.close()

    # Save settings to file if applicable
    if args.settings_file:
        df = pd.DataFrame(
            {
                "fname": bname(fname),
                "meristem_method": args.meristem_method,
                "minmax_iterations": args.minmax_iterations,
                "mean_iterations": args.mean_iterations,
                "depth": args.depth,
                "angle_threshold": args.angle_threshold,
                "bottom_crop": args.bottom_crop,
                "curvature_limits": args.curvature_limits,
                "disconnected_threshold": args.disconnected_threshold,
                "segmented": args.segmented,
                "hole_repair_threshold": args.hole_repair_threshold,
                "downscaling": args.downscaling,
                "smooth_iter": args.smooth_iter,
                "smooth_relax": args.smooth_relax,
                "tongues_ratio": args.tongues_ratio,
                "tongues_radius": args.tongues_radius,
            },
            index=[0],
        )

        if os.path.exists(args.settings_file):
            df2 = pd.read_csv(args.settings_file, sep="\t")
            if df2.loc[df2.fname == bname(fname)].shape[0] > 0:
                df2.loc[df2.fname == bname(fname)] = df.values
                df = df2
            else:
                df = pd.concat([df, df2], ignore_index=True)
        df.to_csv(args.settings_file, sep="\t", index=False)

    mesh.save(f"{args.output}/{oname}.vtk")


p = Pool(args.parallel_workers)
p.map(run, files)
p.close()
p.join()

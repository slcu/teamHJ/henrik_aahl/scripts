#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 29 12:08:33 2020

@author: henrikahl
"""

import argparse
import os
from multiprocessing import Pool

from imgmisc import mkdir
from imgmisc import parse_input_files
from imgmisc import parse_image_input
from imgmisc import parse_resolution
from imgmisc import create_cellular_mesh
from imgmisc import bname
from imgmisc import lowmap
from imgmisc import map_replace
from imgmisc import listdir

parser = argparse.ArgumentParser(description="")

# Required positional argument
parser.add_argument("input", type=str, nargs="+",
                    help="Input files or directory")
parser.add_argument(
    "--output", type=str, help="Output directory", default=f"{os.path.curdir}"
)
parser.add_argument("--smooth_iterations", type=int, default=0)
parser.add_argument("--background", type=int, default=0)
parser.add_argument("--resolution", type=float, nargs=3, default=None)
parser.add_argument("--suffix", type=str, default="-cellular_mesh")
parser.add_argument("--verbose", action="store_true")
parser.add_argument("--skip_done", action="store_true")
parser.add_argument("--parallel_workers", type=int, default=1)
args = parser.parse_args()

files = parse_input_files(
    args.input, include=[".tif", ".tiff", ".lsm", ".czi"])
mkdir(args.output)

fname = files[0]
def run(fname):

    basename = bname(fname)
    if basename in ''.join(listdir(args.output)):
        print('Done. Skipping.')
        return
    
    # Read inputs
    seg_img = parse_image_input(fname)
    resolution = parse_resolution(fname, args.resolution)

    # Generate mesh
    mesh = create_cellular_mesh(seg_img, resolution)
    mesh = mesh.smooth(args.smooth_iterations)

    # Label
    mesh['lowmap_id'] = map_replace(mesh['cell_id'], lowmap(seg_img))
    
    # Save
    mesh.save(f'{args.output}/{basename}{args.suffix}.vtk')

p = Pool(args.parallel_workers)
p.map(run, files)
p.close()
p.join()

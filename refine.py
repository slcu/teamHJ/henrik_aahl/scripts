#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:04:59 2020

@author: henrik
"""

import argparse
import os
from multiprocessing import Pool

import numpy as np
import tifffile as tiff
from imgmisc import parse_input_files
from skimage.segmentation import relabel_sequential
from imgmisc import parse_image_input
from imgmisc import parse_resolution
from loguru import logger
from imgmisc import bname

from imgmisc import (
    binary_extract_largest,
    set_background,
    listdir,
    mkdir,
)

script_desc = """Refine a segmented image to remove spurious cells and by ensuring that the background label is set to a specific value (e.g. 0)."""
parser = argparse.ArgumentParser(description=script_desc)

# Required positional argument
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output", type=str, default=f"{os.path.curdir}", help="Output directory. Defaults to the current directory."
)

# Optional argument
parser.add_argument("--background", type=int, default=0, help='The output label for the background. Defaults to 0.')
parser.add_argument("--delete", type=int, nargs="+", default=None, help='Cell labels to manually delete.')
parser.add_argument('--resolution', type=float, nargs=3, default=None, help='The image resolution in ZYX format unless contained in the image metadata. Defaults to retrieve from the image metadata.')
parser.add_argument("--suffix", type=str, default="-refined", help='The output file suffix. Defaults to "-refined"')
parser.add_argument("--verbose", action="store_true", help="Verbose printing. Defaults to False.")
parser.add_argument("--skip_done", action="store_true", help='Whether to skip done files already found in the output directory. Defaults to False.')
parser.add_argument("--parallel_workers", type=int, default=1, help='The number of parallel workers. Defaults to 1.')

args = parser.parse_args()

files = parse_input_files(args.input, incluide=[".tif", ".tiff", ".lsm", ".czi"])

mkdir(args.output)

if not args.verbose:
    logger.disable("")

def run(fname):
    
    done_files = listdir(args.output)
    if args.skip_done and f"{bname(fname)}{args.suffix}.tif" in "--".join(done_files):
        logger.info(f"Skipping {bname(fname)} - already done")
        return
    logger.info(f"Now running {fname}")

    # Load image
    seg_img = parse_image_input(fname)
    resolution = parse_resolution(fname, args.resolution)

    # Process the image
    if args.delete is not None:
        seg_img[np.isin(seg_img, args.delete)] = 0
    seg_img = set_background(seg_img, background=0)
    seg_img = binary_extract_largest(seg_img, background=0)
    seg_img = relabel_sequential(seg_img)[0]
    seg_img = seg_img.astype(np.uint16)

    # Save the output as a tiff-file in ImageJ format
    outname = f"{bname(fname)}{args.suffix}.tif"
    tiff.imwrite(
        f"{args.output}/{outname}",
        seg_img,
        imagej=True,
        metadata={"spacing": resolution[0], "unit": "um"},
        resolution=(1.0 / resolution[1], 1.0 / resolution[2]),
    )

n_cores = int(args.parallel_workers)

logger.info(f"Using {n_cores} cores")
p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()

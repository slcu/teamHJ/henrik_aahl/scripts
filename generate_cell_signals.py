#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:04:59 2020

@author: henrik
"""

import argparse
import os
from multiprocessing import Pool

from imgmisc import listdir
from skimage.measure import regionprops_table
import tifffile as tiff
import pandas as pd
from functools import reduce

import numpy as np
import pyvista as pv
from loguru import logger

from cellygon import internal_angles_mesh
from imgmisc import (
    bname,
    find_neighbors,
    get_layers,
    l1_cell_surface_areas,
    listdir,
    mkdir,
    natural_sort,
    parse_hstr,
    parse_image_input,
    parse_input_files,
    parse_resolution,
    regionprops3D,
)

parser = argparse.ArgumentParser(description="")
parser.add_argument("--seg_input", type=str, nargs="+",
                    help="Input files or directory")
parser.add_argument("--cha_input", type=str, nargs="+",
                    help="Input files or directory")
parser.add_argument(
    "--output", type=str, help="Output directory", default=f"{os.path.curdir}"
)

parser.add_argument("--background", type=int, default=0)
parser.add_argument("--nchannels", type=int, default=None)
parser.add_argument("--resolution", type=float, nargs=3, default=None)
parser.add_argument("--time", action="store_true")

parser.add_argument("--suffix", type=str, default="-cell_data")
parser.add_argument("--verbose", action="store_true")
parser.add_argument("--skip_done", action="store_true")
parser.add_argument("--parallel_workers", type=int, default=1)
args = parser.parse_args()

if not args.verbose:
    logger.disable("")

print(args.seg_input)
seg_files = parse_input_files(args.seg_input, include=[".tiff", ".tif"])
cha_files = parse_input_files(args.cha_input, include=[".tiff", ".tif"])
mkdir(args.output)

nchannels = len(
    cha_files) // len(seg_files) if args.nchannels is None else args.nchannels
cha_files = np.reshape(cha_files, (-1, nchannels))
all_args = [[seg_files[ii]] + list(cha_files[ii])
            for ii in range(len(seg_files))]

fname = seg_files[0]

# seg_files = listdir('/home/henrikahl/projects/seb/data/ABACUS/segmented_refined/')
# cha_files = listdir('/home/henrikahl/projects/seb/data/ABACUS/cropped/')
# args = {'nchannels': 4}
# cha_files = np.reshape(cha_files, (-1, args['nchannels']))


def run(input_args):
    fname = input_args[0]
    logger.info(f"Now running {fname}")

    basename = bname(fname)
    done_files = listdir(args.output, include=".csv")
    if args.skip_done and f"{basename}{args.suffix}" in "-".join(
        [bname(ff) for ff in done_files]
    ):
        logger.info(f"{basename} done. Skipping...")
        return

    # Get infostr
    if args.time:
        dataset, reporters, genotype, plant, time = parse_hstr(fname)
        time = int(time)
    else:
        dataset, reporters, genotype, plant = parse_hstr(fname)
    dataset, plant = int(dataset), int(plant)

    resolution = parse_resolution(fname, args.resolution)
    seg_img = parse_image_input(fname, )

    logger.info(f"Regprops analysis for {basename}")

    tables = []
    for channel in range(len(input_args) - 1):
        intensity_image = tiff.imread(input_args[channel + 1])
        table_multi = regionprops_table(
            seg_img,
            intensity_image,
            properties=['centroid', 'label', 'area', 'image_intensity', 'intensity_max', 'intensity_mean'])
        new_df = pd.DataFrame(table_multi)
        new_df['image_intensity'] = [dd.sum()
                                     for dd in new_df['image_intensity']]
        new_df = new_df.rename({'image_intensity': f'image_intensity-{channel + 1}',
                                'intensity_max': f'intensity_max-{channel + 1}',
                                'intensity_mean': f'intensity_mean-{channel + 1}'}, axis=1)
        tables.append(new_df)

    regprops = reduce(lambda left, right: pd.merge(left, right, on=['centroid-0', 'centroid-1', 'centroid-2', 'label', 'area'],
                                                   how='outer'), tables)
    regprops['volume'] = regprops['area'] * np.product(resolution)

    logger.info(f"Saving {basename}")
    regprops["dataset"] = [dataset] * regprops.shape[0]
    regprops["reporters"] = [reporters] * regprops.shape[0]
    regprops["genotype"] = [genotype] * regprops.shape[0]
    regprops["plant"] = [plant] * regprops.shape[0]
    if args.time:
        regprops["time"] = [time] * regprops.shape[0]

    labels = np.array(regprops.columns)
    infolabels = (
        ["dataset", "reporters", "genotype", "plant", "time", "label"]
        if args.time
        else ["dataset", "reporters", "genotype", "plant", "label"]
    )

    regprops = regprops[
        infolabels + list(natural_sort(labels[~np.isin(labels, infolabels)]))
    ]

    regprops.to_csv(
        f"{args.output}/{bname(fname)}{args.suffix}.csv", index=False, sep="\t"
    )


n_cores = int(args.parallel_workers)
logger.info(f"Using {n_cores} cores")

p = Pool(n_cores)
p.map(run, all_args)
p.close()
p.join()

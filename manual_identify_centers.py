#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 10:37:07 2022

@author: henrikahl
"""

from math import pi
from math import sqrt
from math import acos
import os
import re
import argparse
import numpy as np
import pandas as pd
import mahotas as mh
import czifile as cz
import pyvista as pv
import tifffile as tiff
from skimage.measure import marching_cubes
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import natural_sort
from imgmisc import get_resolution
from imgmisc import parse_image_input
from imgmisc import parse_resolution

parser = argparse.ArgumentParser(description='')

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--masking', type=float, default=1)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-centers')
parser.add_argument('--segmented', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--verbose', action='store_true')

args = parser.parse_args()

if len(args.input) == 1 and os.path.isdir(os.path.abspath(args.input[0])):
    files = listdir(args.input[0], sorting='natural', include=['.tif', '.tiff',
                                                               '.lsm', '.czi'])
else:
    files = args.input
mkdir(args.output)
# print(files)

from imgmisc import bname
from loguru import logger

import gc
from imgmisc import parse_hstr
from tqdm import tqdm
logger.info(files)

def length(v):
    return sqrt(v[0]**2+v[1]**2)

def create_mesh(contour, resolution=[1, 1, 1]):
    v, f, _, value = marching_cubes(
        contour, 0.5, spacing=list(resolution), step_size=1, allow_degenerate=False)
    mesh = pv.PolyData(v, np.hstack(np.c_[np.full(f.shape[0], 3), f]))
    mesh['value'] = value
    return mesh

dfs = []
for fname in tqdm(files, disable=(not args.verbose)):
    logger.info(f'Now running {bname(fname)}')

    done_files = listdir(args.output, include='.csv')
    done_files = np.array([bname(ff) for ff in done_files])

    if args.skip_done and (f'{bname(fname)}{args.suffix}' in '-'.join(done_files)):
        logger.info(f'{bname(fname)} done. Skipping...')
        continue

    if '.vtk' in fname:
        try:
            mesh = pv.read(fname)
        except:
            raise Exception(f'Input file type for {fname} not supported.')
    else:
        data = parse_image_input(fname, None)
        resolution = parse_resolution(fname, args.resolution)
        
        if not args.segmented:
            contour = to_uint8(data, False) > args.masking * \
                mh.otsu(to_uint8(data, False))
        else:
            contour = data
        contour = np.pad(contour, 1)
        contour = contour[1:, 1:-1, 1:-1]
        mesh = create_mesh(contour, resolution)
    del contour, data
    gc.collect()
    
    dataset, reporters, genotype, plant = parse_hstr(fname)
    dataset, plant = int(dataset), int(plant)

    # User input
    p = pv.Plotter()
    # pyvista can't modify variables that aren't lists
    selected_points = np.full(mesh.n_points, -1, dtype='int')
    orientation = []

    def point_callback(mesh, pt_id):
        index = selected_points.max() + 1
        if selected_points[pt_id] == -1:
            selected_points[pt_id] = index
        p.add_point_labels([mesh.points[pt_id], ], [index], name=f'pt{index}')

    def undo_point():
        index = selected_points.max()
        selected_points[selected_points == index] = -1
        p.remove_actor(f'pt{index}')
        p.update()

    def assign_clockwise():
        if len(orientation) > 0:
            orientation.pop()
        orientation.append('cw')

    def assign_counterclockwise():
        if len(orientation) > 0:
            orientation.pop()
        orientation.append('ccw')
    p.add_mesh(mesh, scalars='value' if args.segmented else [
               1] * mesh.n_points, cmap='glasbey_bw', interpolate_before_map=False)
    # p.view_yz()
    p.enable_point_picking(callback=point_callback,
                           show_message=True, pickable_window=True, use_mesh=True)
    p.add_key_event('u', undo_point)
    p.add_key_event('c', assign_clockwise)
    p.add_key_event('v', assign_counterclockwise)
    p.set_background('white')
    p.show()

    if len(orientation) == 0:
        orientation = [np.nan]
        print('Warning! No orientation given')
        #raise Exception('Need to give orientation! Press c for clockwise or v for counter-clockwise.')

    # TODO ugly ugly fix at some pt
    order = selected_points[selected_points > -1]
    centers = mesh.points[selected_points > -1]

    if len(order) == 0:
        continue

    new_pts = np.zeros((len(order), 3))
    for ii, idx in enumerate(order):
        new_pts[idx] = centers[ii]

    # Compute angles
    centers = new_pts.copy()
    apex = centers[0]
    centers = centers[1:]

    # Add information as dataframe entry
    pdf = pd.DataFrame(columns=[
        'dataset',
        'genotype',
        'reporters',
        'plant',
        'apex_coord-0',
        'apex_coord-1',
        'apex_coord-2'])
    pdf = pdf.append({  # 'file': bname(fname),
        'dataset': dataset,
        'genotype': genotype,
        'reporters': reporters,
        'plant': plant,
        'apex_coord-0': apex[0],
        'apex_coord-1': apex[1],
        'apex_coord-2': apex[2]
    }, ignore_index=True)
    outname = f'{bname(fname)}{args.suffix}.csv'
    pdf.to_csv(f'{args.output}/{outname}', index=False, sep='\t')


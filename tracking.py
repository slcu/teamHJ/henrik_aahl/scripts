#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 11:39:26 2020

@author: henrikahl
"""

import argparse
import os
import re

import ants
import mahotas as mh
import numpy as np
import pandas as pd
import tifffile as tiff
from imgmisc import (
    get_layers,
    get_resolution,
    listdir,
    mkdir,
    natural_sort,
    sgroup,
    to_uint8,
)
from skimage.segmentation import relabel_sequential
from skimage.transform import resize
from timagetk.components.spatial_image import SpatialImage
from tissue_analysis.spatial_image_analysis import SpatialImageAnalysis
from loguru import logger
from imgmisc import bname
from imgmisc import parse_input_files

script_desc = """
This script tracks files of subsequent timepoints to each other. It tries several different sampling methods of the input images to generate the transformation matrices.
"""
parser = argparse.ArgumentParser(description="")
parser.add_argument("--seg_input", type=str, nargs="+", help="Input segmentation files or directory")
parser.add_argument("--sig_input", type=str, nargs="+", help="Input signal files or directory")
parser.add_argument("--output", type=str, help="Output directory. Defaults to the current directory.", default=f"{os.path.curdir}")
parser.add_argument('--background', type=int, default=0, help='The background label. Defaults to 0.')
parser.add_argument("--suffix", type=str, default="-tracking}", help='The output file suffix. Default is "-tracking".')
parser.add_argument("--verbose", action="store_true", help='Verbose flag. Defaults to False.')
parser.add_argument("--skip_done", action="store_true", help='Whether to skip already done files in the output directory. Defaults to False.')
parser.add_argument("--parallel_workers", type=int, default=1, help='The number of parallel workers. Defaults to 1.')
args = parser.parse_args()

OUTPUT_DIR = args.output
BACKGROUND = args.background
mkdir(OUTPUT_DIR)

if not args.verbose:
    logger.disable("")

seg_files = parse_input_files(args.seg_input, include=[".tif", ".tiff", ".lsm", ".czi"])
sig_files = parse_input_files(args.sig_input, include=[".tif", ".tiff", ".lsm", ".czi"])

##############################################################
tp_regex = "(\d+)-(\S+)-(\S+)-plant_(\d+)-(\d+)h"
pl_regex = "\d+-\S+-\S+-plant_\d+-"

datasets, reporters, genotype, plants, times = zip(
    *[
        re.findall(tp_regex, bname(ff))[0]
        for ff in sig_files
    ]
)
datasets, plants, times = (
    np.array(datasets, dtype="int"),
    np.array(plants, dtype="int"),
    np.array(times, dtype="int"),
)

sig_file_groups = sgroup(sig_files, pl_regex)
seg_file_groups = sgroup(seg_files, pl_regex)
for ff in range(len(sig_file_groups)):
    sig_file_groups[ff] = natural_sort(sig_file_groups[ff])
    seg_file_groups[ff] = natural_sort(seg_file_groups[ff])

files = []
for gid in range(len(sig_file_groups)):
    rf_group = sig_file_groups[gid]
    sf_group = seg_file_groups[gid]

    if len(rf_group) < 2 or (len(rf_group) != len(sf_group)):
        continue
    for iter_ in range(len(rf_group) - 1):
        files.append(
            [rf_group[iter_], rf_group[iter_ + 1], sf_group[iter_], sf_group[iter_ + 1]]
        )
files = np.array(files)
done_files = listdir(OUTPUT_DIR, include=".csv")

logger.info(files)

def run(arg):
    t0_path, t1_path, seg0_path, seg1_path = arg

    # Read in basic info
    infostr = re.findall(tp_regex, bname(t0_path))[0]
    dataset, reporters, genotype, plant, from_ = infostr
    dataset, plant, from_ = int(dataset), int(plant), int(from_)
    to_ = int(re.findall("-(\d+)h-", t1_path)[0])

    # Check if already done
    basestr = f"{dataset}-{reporters}-{genotype}-plant_{plant}-{from_}h_to_{to_}h"
    if any([basestr in ff for ff in done_files]):
        logger.info(f"{basestr} already done. Skipping...")
        return

    # Read in images and pre-process
    logger.info(f"Retrieving images {bname(t0_path)} and {bname(t1_path)}")
    t0_res = get_resolution(seg0_path)
    t1_res = get_resolution(seg1_path)

    t0 = tiff.imread(t0_path)
    t1 = tiff.imread(t1_path)
    t0_seg = tiff.imread(seg0_path)
    t1_seg = tiff.imread(seg1_path)

    t0 = to_uint8(t0, normalize=False)
    t1 = to_uint8(t1, normalize=False)

    if t1_seg.shape != t1.shape:
        t1_seg = resize(t1_seg, t1.shape, order=0, preserve_range=True).astype("uint16")
    if t0_seg.shape != t0.shape:
        t0_seg = resize(t0_seg, t0.shape, order=0, preserve_range=True).astype("uint16")

    # Get borders in general, and in epidermis
    t0_borders = mh.borders(t0_seg).astype(t0.dtype)
    t1_borders = mh.borders(t1_seg).astype(t1.dtype)
    t0_borders[t0_borders > 0] = t0[t0_borders > 0]
    t1_borders[t1_borders > 0] = t1[t1_borders > 0]

    t0_l1 = get_layers(t0_seg, depth=1)[0]
    t1_l1 = get_layers(t1_seg, depth=1)[0]

    t0_l1_borders = t0_borders.copy()
    t1_l1_borders = t1_borders.copy()
    t0_l1_borders[~np.isin(t0_seg, t0_l1)] = BACKGROUND
    t1_l1_borders[~np.isin(t1_seg, t1_l1)] = BACKGROUND

    def register_ants(
        t0,
        t1,
        t0_seg,
        t1_seg,
        t0_res,
        t1_res,
        background=0,
        non_linear=True,
        verbose=True,
        t0_fine=None,
        t1_fine=None,
    ):
        t0 = ants.from_numpy(t0, spacing=t0_res)
        t1 = ants.from_numpy(t1, spacing=t1_res)

        # initial registration
        result = ants.registration(
            fixed=t1,
            moving=t0,
            type_of_transform="Affine",
            aff_metric="mattes",
            aff_sampling=128,
            grad_step=0.1,
            verbose=True,
            search_factor=20,
        )
        fwdtransforms = result["fwdtransforms"]

        # second registration
        if non_linear:
            if t0_fine is not None:
                t0 = ants.from_numpy(t0_fine, spacing=t0_res)
            if t1_fine is not None:
                t1 = ants.from_numpy(t1_fine, spacing=t1_res)
            result = ants.registration(
                fixed=t1,
                moving=t0,
                initial_transform=result['fwdtransforms'][0],
                type_of_transform="SyNOnly",
            )

            # TODO check if the initial transform is stacked or not so we don't need to do this step
            if len(result["fwdtransforms"]) == 2:
                fwdtransforms = result["fwdtransforms"]
            else:
                fwdtransforms.extend(result["fwdtransforms"])

        # Transform the segmented images
        seg_t0_on_t1 = ants.apply_transforms(
            fixed=ants.from_numpy(t1_seg.astype(float), spacing=t1_res),
            moving=ants.from_numpy(t0_seg.astype(float), spacing=t0_res),
            transformlist=fwdtransforms,
            interpolator="nearestNeighbor",
        )
        seg_t0_on_t1 = seg_t0_on_t1.numpy().astype(np.uint16)
        seg_t0_on_t1[seg_t0_on_t1 == 0] = background

        # Ensure that the labelling is sequential
        # TODO We can rewrite the parts down below so they won't need this step
        logger.info("Relabelling")
        seg_t0_on_t1, t0_fw_map, t0_bw_map = relabel_sequential(np.array(seg_t0_on_t1))
        # should this be resolution for t0 or t1?
        seg_t0_on_t1 = SpatialImage(seg_t0_on_t1, voxelsize=t1_res)
        t1_seg, t1_fw_map, t1_bw_map = relabel_sequential(np.array(t1_seg))
        t1_seg = SpatialImage(t1_seg, voxelsize=t1_res)

        # Extract information of the cell positions etc
        # TODO rewrite this to not need timagetk
        logger.info("Extract cell information")
        t0_maxlabel = int(seg_t0_on_t1.max())
        t1_maxlabel = int(t1_seg.max())
        an0 = SpatialImageAnalysis(seg_t0_on_t1, background=None)
        an1 = SpatialImageAnalysis(t1_seg, background=None)
        t0_volumes = an0.volume()
        t1_volumes = an1.volume()
        t0_labels_in_bb = an0.boundingbox()
        del an0, an1

        # Compute overlaps between timepoints and corresponding mapping score
        def dice(t1_cell_voxels, lab, t0_vol):
            return float(
                2
                * sum(t1_cell_voxels == lab)
                * np.product(t1_res)
                / (t0_vol + t1_volumes[lab])
            )

        logger.info("Compute scores")
        scores = np.zeros((t0_maxlabel + 1, t1_maxlabel + 1), dtype=float)
        for t0_cell, bb in t0_labels_in_bb.items():
            t1_cell_voxels = t1_seg[bb][seg_t0_on_t1[bb] == t0_cell]  # t0 in t1
            t1_labels = np.unique(t1_cell_voxels)
            t0_vol = t0_volumes[t0_cell]

            # calculate dice score for the individual labels and input them in the score matrix
            mapping_scores = dict()
            for lab in t1_labels:
                if lab != background:
                    mapping_scores[lab] = dice(t1_cell_voxels, lab, t0_vol)
                else:
                    mapping_scores[lab] = float(
                        sum(t1_cell_voxels == lab) * np.product(t1_res) / t0_vol
                    )
            for mapping_cell, mapping_score in mapping_scores.items():
                scores[t0_cell, mapping_cell] = mapping_score

        logger.info("Find matches")
        matching = []
        matching_scores = []
        for ii in range(background + 1, t1_maxlabel + 1):
            score = np.max(scores[:, ii])
            if score > 0:
                matching.append((t0_bw_map[np.argmax(scores[:, ii])], t1_bw_map[ii]))
                matching_scores.append(score)
            else:
                matching.append((background, t1_bw_map[ii]))
                matching_scores.append(0)
        matching = np.array(matching)
        matching_scores = np.array(matching_scores)

        # Create dict of the lineages
        logger.info("Establish lineages")
        mothers_daughters = np.c_[
            [plant] * len(matching),
            [from_] * len(matching),
            [to_] * len(matching),
            matching,
            matching_scores,
        ]

        # Add the unmatched t0 cells -- these now match with the background
        t0_unmatched = np.unique(t0_seg)
        t0_unmatched = t0_unmatched[~np.isin(t0_unmatched, matching[:, 0])]
        to_add = np.c_[
            [plant] * len(t0_unmatched),
            [from_] * len(t0_unmatched),
            [to_] * len(t0_unmatched),
            np.c_[np.array(t0_unmatched), [0] * len(t0_unmatched)],
            [1] * len(t0_unmatched),
        ]
        mothers_daughters = np.vstack([mothers_daughters, to_add])

        return mothers_daughters

    # 2 replicas of each, because registration can be iffy at times
    mds = []
    for _ in range(2):
        mds.append(
            register_ants(
                t0_borders,
                t1_borders,
                t0_seg,
                t1_seg,
                t0_res,
                t1_res,
                background=BACKGROUND,
                non_linear=True,
                verbose=args.verbose,
                t0_fine=t0_borders,
                t1_fine=t1_borders
            )
        )
    for _ in range(2):
        mds.append(
            register_ants(
                t0_l1_borders,
                t1_l1_borders,
                t0_seg,
                t1_seg,
                t0_res,
                t1_res,
                background=BACKGROUND,
                non_linear=True,
                verbose=args.verbose,
                t0_fine=t0_borders,
                t1_fine=t1_borders
            )
        )
    for _ in range(2):
        mds.append(
            register_ants(
                t0,
                t1,
                t0_seg,
                t1_seg,
                t0_res,
                t1_res,
                background=BACKGROUND,
                non_linear=True,
                verbose=args.verbose,
                t0_fine=t0,
                t1_fine=t1
            )
        )

    # Check which registration got the best score and choose that one
    def score(x):
        return np.quantile(x[(x > 0.1) & (x < 1)], 0.9)

    scores = [score(x[:, -1]) for x in mds]
    md = mds[np.argmax(scores)]

    if args.verbose:
        logger.info(f"Save {basestr}")
    oname = f"{basestr}{args.suffix}.csv"
    df = pd.DataFrame(md, columns=["plant", "t1", "t2", "cell_t1", "cell_t2", "dice_score"])
    df["dataset"] = [dataset] * len(df)
    df["reporters"] = [reporters] * len(df)
    df["genotype"] = [genotype] * len(df)
    df["t1"] = [from_] * len(df)
    df["t2"] = [to_] * len(df)
    df = df.convert_dtypes()
    df = df.sort_values(
        ["dataset", "reporters", "genotype", "plant", "t1", "t2", "cell_t1", "cell_t2"]
    )
    df.to_csv(f"{OUTPUT_DIR}/{oname}", index=False, sep="\t")


from multiprocessing import Pool
logger.info(f"Running with {args.parallel_workers} cores")
p = Pool(args.parallel_workers)
p.map(run, files)
p.close()
p.join()

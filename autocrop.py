#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 21:58:18 2021

@author: henrikahl

"""


import os
import argparse
import numpy as np
import tifffile as tiff
import mahotas as mh
from imgmisc import cut
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import autocrop
from pystackreg import StackReg
from imgmisc import parse_image_input
from imgmisc import parse_resolution
from imgmisc import parse_input_files
from loguru import logger
from imgmisc import bname

script_descr = """This script allows the user to automatically crop 3D image stacks by setting a
threshold intensity value/count that needs to be surpassed. The algorithm then
crops the image to a bounding box defined by that constraint.
"""

parser = argparse.ArgumentParser(description=script_descr)

# Required positional argument
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory. Required argument.')
parser.add_argument('--output', type=str,
                    help='Output directory. Defaults to the current directory.', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--background', type=int, default=0, help='The background label. Defaults to 0.')
parser.add_argument('--offset', type=int, default=[10, 5, 5], nargs='*', help='The offset in ZYX direction for cropping. If offset[0] is 5, and the beginning of the object is identified at slice 14, the image will be cropped at slice 9. Defaults to [10, 5, 5]')
parser.add_argument('--factor', type=float, default=.9, help='The factor used for the Otsu mask if so used, and if _binary_ is set to True. Defaults to 0.9.')
parser.add_argument('--n', type=int, default=10, help='The number of voxels exceeding the threshold criteria that have to been met for cropping to initiate. Defaults to 10.')
parser.add_argument('--channel', type=int, default=None, help='Which image channel to use, if using a multi-channeled image. Defaults to all channels.')
parser.add_argument('--resolution', type=float, nargs=3, default=None, help='The image resolution in ZYX format unless contained in the image metadata. Defaults to retrieve from the image metadata.')
parser.add_argument('--suffix', type=str, default='-cropped', help='Output filename suffix. Defaults to "-cropped"')
parser.add_argument('--binary', action='store_true', help='Whether the file is binary (i.e. segmented) or not.')
parser.add_argument('--verbose', action='store_true', help='Verbose printing. Defaults to False.')
parser.add_argument('--skip_done', action='store_true', help='Skip already done files if found in the output directory. Defaults to False.')
parser.add_argument('--split_channels', action='store_true', help='Whether to split channels into separate files or not. Defaults to False.')
parser.add_argument('--wiener', action='store_true', help='Whether to use wiener deconvolution or not. Defaults to False.')
parser.add_argument('--stackreg', action='store_true', help='Whether to regularise the image stack or not. Defaults to False.')
parser.add_argument('--parallel_workers', type=int, default=1, help='The number of parallel workers. Defaults to 1.')

args = parser.parse_args()


files = parse_input_files(args.inputs, include=['.tif', '.tiff', '.lsm', '.czi', '.lif'])
mkdir(args.output)

if not args.verbose:
    logger.disable("")

logger.info(files)
def run(fname):
    if args.verbose:
        logger.info(f'Now running {fname}')
    
    done_files = listdir(args.output)
    o1 = f'{args.output}/{bname(fname)}' + '-C1' + f'{args.suffix}.tif'
    o2 = f'{args.output}/{bname(fname)}' + f'{args.suffix}.tif'
    if args.skip_done and (any(np.isin(done_files, o1)) or any(np.isin(done_files, o2))):
        logger.info(f'File {bname(fname)} done. Skipping...')
        return
    
    data = parse_image_input(fname, args.channel)
    resolution = parse_resolution(fname, args.resolution)

    if data.ndim > 3 and '.czi' in fname:
        logger.info('CZI-file detected. Swapping axes 0 and 1.')
        data = np.swapaxes(data, 0, 1)

    # Define an appropriate offset
    offset = args.offset
    if len(offset) == 1:
        offset = np.full((3, 2), offset)
    elif len(offset) == 3:
        offset = np.array([[offset[0]] * 2, [offset[1]] * 2, [offset[2]] * 2])
    elif len(offset) == 6:
        offset = np.reshape(offset, (-1, 2))
    else:
        raise Exception("`offset` must be of length 1, 3 or 6")

    # Run StackReg to correct for moving stage / moving meristems
    if args.stackreg:
        pretype = data.dtype
        data = data.astype(float)
        logger.info(f'Running StackReg for file {fname} with shape {data.shape}')
        sr = StackReg(StackReg.RIGID_BODY)
        if data.ndim > 3:
            trsf_mat = sr.register_stack(np.max(data, 1))
            for ii in range(data.shape[1]):
                data[:, ii] = sr.transform_stack(data[:, ii], tmats=trsf_mat)
        else:
            trsf_mat = sr.register_stack(data)
            data = sr.transform_stack(data, tmats=trsf_mat)
        data[data < 0] = 0
        if pretype not in [float, np.float64]:
            data[data > np.iinfo(pretype).max] = np.iinfo(pretype).max
        data = data.astype(pretype)
    

    # Compute the mask
    cfact = args.factor
    if not args.binary:
        threshold_mask = cfact * mh.otsu(np.max(data, 1))
    else:
        threshold_mask = 0

    # Crop
    logger.info(f'Preshape: {data.shape}')
    if data.ndim > 3:
        _, cuts = autocrop(np.max(data, 1), threshold_mask, n=args.n, 
                           offset=offset, return_cuts=True)
        data = cut(data, cuts)
    else:
        data, cuts = autocrop(data, threshold_mask, n=args.n, offset=offset, return_cuts=True)
    logger.info(f'Postshape: {data.shape}')

    if args.wiener:
        from scipy.signal import wiener
        pretype = data.dtype
        data = data.astype(float)
        if data.ndim < 4:
            data = wiener(data)
        else:
            for cc in range(data.shape[1]):
                data[:, cc] = wiener(data[:, cc])
        data = data.astype(pretype)

    # Save output    
    if args.split_channels:
        if data.ndim < 3:
            raise Exception(f'Trying to split channels, but input is of shape {data.shape}.' + 
                            ' Channel splitting requires input in format ZCYX.')
        elif data.ndim == 3: 
            data = np.expand_dims(data, 1)
        for cc in range(data.shape[1]):
            iname = os.path.basename(os.path.splitext(fname)[0]) + f'-C{cc+1}' + f'{args.suffix}.tif'
            iname = f'{args.output}/{iname}'
            tiff.imwrite(iname, data=np.expand_dims(data[:, cc], 1), resolution=1/resolution[1:], imagej=True, 
                metadata={'spacing':resolution[0], 'unit':'um'})
    else:
        iname = os.path.basename(os.path.splitext(fname)[0]) + f'{args.suffix}.tif'
        iname = f'{args.output}/{iname}'
        tiff.imwrite(iname, data=np.expand_dims(data, 1) if data.ndim == 3 else data, resolution=1/resolution[1:], imagej=True, 
                    metadata={'spacing':resolution[0], 'unit':'um'})
    del data
            
from multiprocessing import Pool
p = Pool(args.parallel_workers)
output = p.map(run, files)
p.close()
p.join()

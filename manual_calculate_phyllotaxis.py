#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 30 10:37:07 2022

@author: henrikahl
"""

from math import pi
from math import sqrt
from math import acos
import os
import re
import argparse
import numpy as np
import pandas as pd
import mahotas as mh
import czifile as cz
import pyvista as pv
import tifffile as tiff
from skimage.measure import marching_cubes
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import get_resolution
from imgmisc import parse_input_files
from imgmisc import bname
from loguru import logger

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--font_size', type=int, default=None)
parser.add_argument('--masking', type=float, default=1)
parser.add_argument('--resolution', type=float, nargs=3, default=None)
parser.add_argument('--suffix', type=str, default='-phyllotaxis_data')
parser.add_argument('--segmented', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--verbose', action='store_true')
args = parser.parse_args()

files = parse_input_files(args.input, include=['.tif', '.tiff', '.lsm', '.czi'])
mkdir(args.output)

logger.info(files)
dfs = []

def length(v):
    return sqrt(v[0]**2+v[1]**2)

def dot_product(v, w):
    return v[0]*w[0]+v[1]*w[1]

def determinant(v, w):
    return v[0]*w[1]-v[1]*w[0]

def inner_angle(v, w):
    cosx = dot_product(v, w)/(length(v)*length(w))
    rad = acos(cosx)  # in radians
    return rad*180/pi  # returns degrees

def angle_clockwise(A, B):
    inner = inner_angle(A, B)
    det = determinant(A, B)
    if det < 0:  # this is a property of the det. If the det < 0 then B is clockwise of A
        return inner
    else:  # if the det > 0 then A is immediately clockwise of B
        return 360-inner

def create_mesh(contour, resolution=[1, 1, 1]):
    v, f, _, value = marching_cubes(
        contour, 0.5, spacing=list(resolution), step_size=1, allow_degenerate=False)
    mesh = pv.PolyData(v, np.hstack(np.c_[np.full(f.shape[0], 3), f]))
    mesh['value'] = value
    return mesh

fname = '/media/henrikahl/My Passport1/data/220706-PI-Col0_and_csi1-osmotic_treatments/220706-PI-Col0-plant_1-0.2M-cropped_predictions_multicut-refined.tif'
fname = '/home/henrikahl/220621/contours/220621-PIN1GFP_MYRYFP-WT-plant_1-0.02cyt_lambda_reverse-C2-cropped_predictions_multicut-refined_contour.tif'
from tqdm import tqdm
for fname in tqdm(files):
    if args.verbose:
        logger.info(f'Now running {bname(fname)}')

    done_files = listdir(args.output)
    done_files = np.array([bname(ff) for ff in done_files])

    if args.skip_done and np.any(done_files == f'{bname(fname)}{args.suffix}'):
        logger.info(f'{bname(fname)} done. Skipping...')
        continue

    if '.vtk' in fname:
        try:
            mesh = pv.read(fname)
            if np.any(np.array(mesh.array_names) == 'domains'):
                mesh.set_active_scalars('domains')
        except:
            raise Exception(f'Input file type for {fname} not supported.')
    else:
        try:
            data = tiff.imread(fname)
        except:
            try:
                data = cz.czifile(fname)
            except:
                raise Exception(f'Input file type for {fname} not supported.')
        data = np.squeeze(data)
        if data.ndim < 3:
            raise Exception('Too few channels')
        if data.ndim == 4 and args.channel is not None:
            data = data[:, args.channel]
        elif data.ndim > 3 and args.channel is None:
            raise Exception('Too many channels!')

        if args.resolution is None:
            resolution = np.asarray(get_resolution(fname))
            if resolution[0] < 1e-3:
                resolution = np.array(resolution) * 1e6
        else:
            resolution = args.resolution

        if not args.segmented:
            contour = to_uint8(data, False) > args.masking * \
                mh.otsu(to_uint8(data, False))
        else:
            contour = data
        contour = np.pad(contour, 1)
        contour = contour[1:, 1:-1, 1:-1]
        mesh = create_mesh(contour, resolution)

    dataset, reporters, genotype, plant = re.findall(
        '(\S+)-(\S+)-(\S+)-plant_(\d+)', bname(fname))[0]
    dataset, plant = int(dataset), int(plant)
    # dataset = int(dataset)

    # User input
    p = pv.Plotter()
    # pyvista can't modify variables that aren't lists
    selected_points = np.full(mesh.n_points, -1, dtype='int')
    orientation = []

    def point_callback(mesh, pt_id):
        index = selected_points.max() + 1
        if selected_points[pt_id] == -1:
            selected_points[pt_id] = index
        p.add_point_labels([mesh.points[pt_id], ], [index], name=f'pt{index}',
                font_size=args.font_size)

    def undo_point():
        index = selected_points.max()
        selected_points[selected_points == index] = -1
        p.remove_actor(f'pt{index}')
        p.update()

    def assign_clockwise():
        if len(orientation) > 0:
            orientation.pop()
        orientation.append('cw')

    def assign_counterclockwise():
        if len(orientation) > 0:
            orientation.pop()
        orientation.append('ccw')
        
    scalars = 'value' if args.segmented else [1] * mesh.n_points
    scalars = 'domains' if np.any(np.array(mesh.array_names) == 'domains') else scalars
    
    p.add_mesh(mesh, scalars=scalars, cmap='glasbey_bw', interpolate_before_map=False)
    # p.view_yz()
    p.enable_point_picking(callback=point_callback,
                           show_message=True, pickable_window=True, use_mesh=True)
    p.add_key_event('u', undo_point)
    p.add_key_event('c', assign_clockwise)
    p.add_key_event('v', assign_counterclockwise)
    p.set_background('white')
    p.show()

    if len(orientation) == 0:
        orientation = [np.nan]
        logger.warning('Warning! No orientation given')
        #raise Exception('Need to give orientation! Press c for clockwise or v for counter-clockwise.')

    # TODO ugly ugly fix at some pt
    order = selected_points[selected_points > -1]
    centers = mesh.points[selected_points > -1]

    if len(order) == 0:
        continue

    new_pts = np.zeros((len(order), 3))
    for ii, idx in enumerate(order):
        new_pts[idx] = centers[ii]

    # Compute angles
    centers = new_pts.copy()
    apex = centers[0]
    centers = centers[1:]
    angles = [angle_clockwise(centers[ii, 1:] - apex[1:], [0, 1])
              for ii in range(len(centers))]
    angles = np.array(angles)
    if args.verbose:
        logger.info('\tAngles: ' + ' '.join([f"{x:.2}" for x in angles]))

    #angles = (np.arctan2(centers[:,1] - apex[1], centers[:, 2] - apex[2]) * 360 / (2 * np.pi)) % 360

    def divang(angles): return ((np.diff(angles) + 180) % 360 - 180)
    # divang_cw(angles) if orientation[0] == 'cw' else divang_ccw(angles)
    divang = divang(angles)
    divang = np.abs(divang)

    organ_distance = np.sum((centers - apex)**2, axis=1)**.5

    # Add information as dataframe entry
    pdf = pd.DataFrame(columns=[
        'dataset',
        'genotype',
        'reporters',
        'plant',
        'organ_index',
        'apex_coord-0',
        'apex_coord-1',
        'apex_coord-2',
        'coord-0',
        'coord-1',
        'coord-2',
        'distance',
        'angle',
        'divang',
        'plant_divang_mean',
        'plant_divang_median',
        'plant_divang_std',
        'n_domains',
        'orientation'])
    for ii, angle in enumerate(angles):
        pdf = pdf.append({  # 'file': bname(fname),
            'dataset': dataset,
            'reporters': reporters,
            'genotype': genotype,
            'plant': plant,
            'organ_index': ii,
            'apex_coord-0': apex[0],
            'apex_coord-1': apex[1],
            'apex_coord-2': apex[2],
            'coord-0': centers[ii][0],
            'coord-1': centers[ii][1],
            'coord-2': centers[ii][2],
            'distance': organ_distance[ii],
            'angle': angle,
            'divang': np.nan if ii == len(divang) else divang[ii],
            'plant_divang_mean': np.mean(divang),
            'plant_divang_median': np.median(divang),
            'plant_divang_std': np.std(divang),
            'n_domains': len(angles),
            'orientation': orientation[0]
        }, ignore_index=True)
    outname = f'{bname(fname)}{args.suffix}.csv'
    pdf.to_csv(f'{args.output}/{outname}', index=False, sep='\t')
    # dfs.append(pdf)
# 
# df = pd.concat(dfs, ignore_index=True)


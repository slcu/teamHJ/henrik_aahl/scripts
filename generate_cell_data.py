#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:04:59 2020

@author: henrik
"""

import argparse
import os
from multiprocessing import Pool

import numpy as np
import pyvista as pv
from loguru import logger

from cellygon import internal_angles_mesh
from imgmisc import (
    bname,
    find_neighbors,
    get_layers,
    l1_cell_surface_areas,
    listdir,
    mkdir,
    natural_sort,
    parse_hstr,
    parse_image_input,
    parse_input_files,
    parse_resolution,
    regionprops3D,
)

script_desc = \
"""Generate cell geometry data for a segmented 3D image. Computes the following metrics for each cell:
    "label" : the cell label
    "bbox_area" : the surface area of the bounding box
    "bbox_volume" : the volume of the bounding box
    "obbox_sides" : the length of the oriented bounding box sides
    "obbox_volume" : the length of the oriented bounding box sides
    "obbox_area" : the surface area of the oriented bounding box
    "flatness" : the ratio between smallest and largest "obbox_sides"
    "ellipsoidity" : the ellipsoidity
    "fractional_anisotropy" : fractional anisotropy as calculated from the obbox sides
    "cuboidness" : the cuboidness
    "centroid" : the cell centroid coordinates, separated into "centroid-0", "centroid-1" and "centroid-2"
    "convex_volume" : the volume of the convex hull of the cell
    "equivalent_diameter" : the diameter of the corresponding sphere with the same volume as the cell
    "max_inscribed_sphere_radius" : the radius of the biggest sphere which can fit in the cell
    "pca_major_axis_length" : the major explained variance axis from a PCA of the cell voxel point cloud
    "pca_medial_axis_length" : the medial explained variance axis from a PCA of the cell voxel point cloud
    "pca_minor_axis_length" : the minor explained variance axis from a PCA of the cell voxel point cloud
    "solidity" : See skimage.regionprops
    "sphericity" : See skimage.regionprops
    "surface_area" : The cell surface area
    "volume" : The cell volume
"""

parser = argparse.ArgumentParser(description=script_desc)
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output", type=str, default=f"{os.path.curdir}", help="Output directory. Defaults to the current directory."
)

parser.add_argument("--cellygon", type=str, nargs="+", default=None, help="Optional cellygonised files which enable L1 cell junction angle quantification.s")
parser.add_argument("--background", type=int, default=0, help='Background label. Defaults to 0.')
parser.add_argument("--channel", type=int, default=None, help="The channels to use, if applicable.")
parser.add_argument("--resolution", type=float, nargs=3, default=None, help="The image resolution, if provided manually. If not given, attempts to retrieve this from the image metadata.")
parser.add_argument("--time", action="store_true", help='Whether the filename has a time-flag, e.g. "[...]-plant_3-0h". Defaults to False.')

parser.add_argument("--suffix", type=str, default="-cell_data", help='The output file suffix. Defaults to "-cell_data".')
parser.add_argument("--verbose", action="store_true", help='Verbosity flag. Defaults to False.')
parser.add_argument("--skip_done", action="store_true", help='Skip done files already in the output folder. Defaults to False.')
parser.add_argument("--parallel_workers", type=int, default=1, help='Number of parallel workers. Defaults to 1')
args = parser.parse_args()

if not args.verbose:
    logger.disable("")

logger.info(args.input)
files = parse_input_files(args.input, include=[".tiff", ".tif"])
mkdir(args.output)

# fname = files[0]
# fname = '/home/henrikahl/projects/svmeristem/data/221114-pUBQ10-WT-Timelapse_4h/segmented_refined/221122-pUBQ10-WT-plant_6-8h-cropped-deconvolved_12_predictions_multicut-refined.tif'
# cell_fname = '/home/henrikahl/projects/svmeristem/data/221114-pUBQ10-WT-Timelapse_4h/cellygon_meshes/221122-pUBQ10-WT-plant_6-8h-cropped-deconvolved_12_predictions_multicut-refined-cellygon_mesh.vtk'
def run(fname):
    logger.info(f"Now running {fname}")
    
    basename = bname(fname)
    done_files = listdir(args.output, include=".csv")
    if args.skip_done and f"{basename}{args.suffix}" in "-".join(
        [bname(ff) for ff in done_files]
    ):
        logger.info(f"{basename} done. Skipping...")
        return

    # Get infostr
    if args.time:
        dataset, reporters, genotype, plant, time = parse_hstr(fname)
        time = int(time)
    else:
        dataset, reporters, genotype, plant = parse_hstr(fname)
    dataset, plant = int(dataset), int(plant)

    resolution = parse_resolution(fname, args.resolution)
    seg_img = parse_image_input(fname, args.channel)

    logger.info(f"Regprops analysis for {basename}")
    regprops = regionprops3D(
        seg_img,
        resolution=resolution,
        properties=[
            "label",
            "bbox_area",
            "bbox_volume",
            "obbox_sides",
            "obbox_volume",
            "obbox_area",
            "flatness",
            "ellipsoidity",
            "fractional_anisotropy",
            "cuboidness",
            "centroid",
            "convex_volume",
            "equivalent_diameter",
            "max_inscribed_sphere_radius",
            "pca_major_axis_length",
            "pca_medial_axis_length",
            "pca_minor_axis_length",
            "solidity",
            "sphericity",
            "surface_area",
            "volume",
        ],
    )

    logger.info(f"Computing L1 surface areas {basename}")
    labels, l1_surface_areas = l1_cell_surface_areas(seg_img)
    l1_surface_areas = {labels[ii]: l1_surface_areas[ii] for ii in range(len(labels))}
    regprops["l1_surface_areas"] = [
        l1_surface_areas[ll] if ll in l1_surface_areas else np.nan
        for ll in regprops["label"]
    ]

    logger.info(f"Computing layers {basename}")
    l1, l2, l3 = get_layers(seg_img, depth=3)
    regprops["layer"] = [
        "L1" if ll in l1 else "L2" if ll in l2 else "L3" if ll in l3 else "L4+"
        for ll in regprops["label"]
    ]

    logger.info(f"Calculating neighbourhood metrics for {basename}")
    neighs = find_neighbors(seg_img, background=args.background, connectivity=3)
    regprops["n_neighs"] = [len(neighs[lab]) for lab in regprops["label"]]
    regprops["n_l1neighs"] = [
        len(neighs[lab][np.isin(neighs[lab], l1)]) for lab in regprops["label"]
    ]
    regprops["n_l2neighs"] = [
        len(neighs[lab][np.isin(neighs[lab], l2)]) for lab in regprops["label"]
    ]
    regprops["n_l3neighs"] = [
        len(neighs[lab][np.isin(neighs[lab], l3)]) for lab in regprops["label"]
    ]


    if args.cellygon is not None:
        logger.info(f"Cellygon analysis for {basename}")

        cellygon_files = parse_input_files(args.cellygon)
        try:
            cellygon_mesh = pv.read(
                [
                    ff
                    for ff in cellygon_files
                    if f"{dataset}-{genotype}-{reporters}-plant_{plant}-{time}h" in ff
                ][0]
            )
            angles = internal_angles_mesh(cellygon_mesh)
            regprops["angle_internal_min_1"] = np.asarray(
                [
                    np.sort(angles[angles[:, 0] == lab, 2])[0]
                    if len(angles[angles[:, 0] == lab, 0]) > 0
                    else np.nan
                    for lab in regprops["label"]
                ]
            )
            regprops["angle_internal_min_2"] = np.asarray(
                [
                    np.sort(angles[angles[:, 0] == lab, 2])[1]
                    if len(angles[angles[:, 0] == lab, 0]) > 1
                    else np.nan
                    for lab in regprops["label"]
                ]
            )
            regprops["angle_internal_min_3"] = np.asarray(
                [
                    np.sort(angles[angles[:, 0] == lab, 2])[2]
                    if len(angles[angles[:, 0] == lab, 0]) > 2
                    else np.nan
                    for lab in regprops["label"]
                ]
            )
            regprops["angle_internal_mean"] = np.asarray(
                [
                    np.mean(angles[angles[:, 0] == lab, 2])
                    if len(angles[angles[:, 0] == lab, 0]) > 0
                    else np.nan
                    for lab in regprops["label"]
                ]
            )
            regprops["angle_internal_std"] = np.asarray(
                [
                    np.std(angles[angles[:, 0] == lab, 2])
                    if len(angles[angles[:, 0] == lab, 0]) > 0
                    else np.nan
                    for lab in regprops["label"]
                ]
            )
        except:
            regprops["angle_internal_min_1"] = [np.nan for lab in regprops["label"]]
            regprops["angle_internal_min_2"] = [np.nan for lab in regprops["label"]]
            regprops["angle_internal_min_3"] = [np.nan for lab in regprops["label"]]
            regprops["angle_internal_mean"] = [np.nan for lab in regprops["label"]]
            regprops["angle_internal_std"] = [np.nan for lab in regprops["label"]]

    logger.info(f"Saving {basename}")
    regprops["dataset"] = [dataset] * regprops.shape[0]
    regprops["reporters"] = [reporters] * regprops.shape[0]
    regprops["genotype"] = [genotype] * regprops.shape[0]
    regprops["plant"] = [plant] * regprops.shape[0]
    if args.time:
        regprops["time"] = [time] * regprops.shape[0]

    labels = np.array(regprops.columns)
    infolabels = (
        ["dataset", "reporters", "genotype" , "plant", "time", "label"]
        if args.time
        else ["dataset", "reporters", "genotype", "plant", "label"]
    )

    regprops = regprops[
        infolabels + list(natural_sort(labels[~np.isin(labels, infolabels)]))
    ]

    regprops.to_csv(
        f"{args.output}/{bname(fname)}{args.suffix}.csv", index=False, sep="\t"
    )


n_cores = int(args.parallel_workers)
logger.info(f"Using {n_cores} cores")

p = Pool(n_cores)
p.map(run, files)
p.close()
p.join()

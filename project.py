#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 11:29:32 2020

@author: henrikahl
"""

import os
import argparse
import numpy as np
import tifffile as tiff
# from multiprocessing import Pool, cpu_count
from imgmisc import mkdir
from imgmisc import listdir
from imgmisc import to_uint8
from imgmisc import parse_image_input
import matplotlib
import matplotlib.pyplot as plt
from imgmisc import parse_input_files
from imgmisc import bname
from imgmisc import parse_resolution
from xvfbwrapper import Xvfb

cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", ['black', "red"])

vdisplay = Xvfb(width=1920, height=1080)
vdisplay.start()

parser = argparse.ArgumentParser(description='')
parser.add_argument('input', type=str, nargs='+',
                    help='Input files or directory')
parser.add_argument('--output', type=str,
                    help='Output directory', default=f'{os.path.curdir}')

# Optional argument
parser.add_argument('--threshold', type=float, default=.99)
parser.add_argument('--channel', type=int, default=None)
parser.add_argument('--segmented', action='store_true')
parser.add_argument('--background', type=int, default=0)
parser.add_argument('--resolution', type=float, nargs=3, default=[1,1,1])
parser.add_argument('--sides', type=int, nargs=3, default=[1,0,0])
parser.add_argument('--fct', type=str, default='sum')
parser.add_argument('--verbose', action='store_true')
parser.add_argument('--skip_done', action='store_true')
parser.add_argument('--parallel_workers', type=int, default=1)
args = parser.parse_args()

files = parse_input_files(args.input)
mkdir(args.output)


def save_plots(oname, data, resolution=[1, 1, 1], sides=[1, 1, 1], fct='sum'):
    if fct == 'sum':
        fct = np.sum
    elif fct == 'max':
        fct = np.max
    elif fct == 'mean':
        fct = np.mean

    if sides[0] != 0:
        sum_data = fct(data, 0)
        sum_data = to_uint8(sum_data, normalize=True)
        plt.imsave(f'{args.output}/{oname}-top_sum.png', sum_data, 
                   cmap=cmap, vmin=0, 
                   vmax=np.quantile(sum_data, args.threshold))

    if sides[1] != 0:
        #data_1 = data[::-1, :, data.shape[2] // 2]    
        data_1 = fct(data, 1)
        data_1 = to_uint8(data_1, normalize=True)
        plt.imsave(f'{args.output}/{oname}-side_1.png', data_1, cmap=cmap, vmin=0, vmax=np.quantile(data_1, args.threshold))
    
    if sides[2] != 0:
        #data_2 = data[::-1, data.shape[1] // 2, :]    
        data_2 = fct(data, 2)
        data_2 = to_uint8(data_2, normalize=True)
        plt.imsave(f'{args.output}/{oname}-side_2.png', data_2, cmap=cmap, vmin=0, vmax=np.quantile(data_2, args.threshold))

def project(fname):
    oname = bname(fname)

    if args.skip_done and oname in ''.join(listdir(args.output, include='.png')): 
        print(f'{oname} done. Skipping...')
        return
    else:
        print(f'Running {oname}')

    data = parse_image_input(fname, args.channel)
    resolution = parse_resolution(fname, args.resolution)
    
    if args.segmented:
        data = (data != args.background).astype(float)
    
    if data.ndim > 3:
        for ch in range(data.shape[1]):
            save_plots(f'{oname}-C{ch + 1}', data[:, ch], resolution, sides=args.sides)
    else:
        save_plots(f'{oname}', data, resolution, args.sides)

from multiprocessing import Pool
p = Pool(args.parallel_workers)
p.map(project, files)
p.close()
p.join()

import argparse
import os
from multiprocessing import Pool

import numpy as np
import pandas as pd
import pyvista as pv
from loguru import logger
from scipy.spatial import cKDTree
from tqdm import tqdm

import phenotastic.mesh as mp
from imgmisc import (
    binary_extract_largest,
    bname,
    find_neighbors,
    get_layers,
    get_resolution,
    listdir,
    mkdir,
    parse_hstr,
    parse_input_files,
    to_uint8,
)
from phenotastic.mesh import (
    create_mesh,
    fill_contour,
    remesh,
    remove_normals,
    remove_tongues,
    repair_small,
    smooth_boundary,
)

script_desc = """ """
parser = argparse.ArgumentParser(description=script_desc)
parser.add_argument("input", type=str, nargs="+", help="Input files or directory")
parser.add_argument(
    "--output",
    type=str,
    help="Output name",
    default=f"{os.path.curdir}/cell_age_data.csv",
)
parser.add_argument(
    "--threshold",
    type=float,
    default=35,
    help="Threshold distance in um. Points larger than this from the center are removed. Defaults to 35.",
)
parser.add_argument(
    "--initial_params",
    type=float,
    nargs=8,
    default=None,
    help="Initial guess for the paraboloid parameters. Tries a few different guesses if not provided.",
)

parser.add_argument(
    "--suffix", type=str, help="Output file suffix.", default="-paraboloid_fit"
)
parser.add_argument(
    "--verbose", action="store_true", help="Verbose flag. Defaults to False."
)
parser.add_argument(
    "--segmented",
    action="store_true",
    help="Whether input is segmented. Defaults to False.",
)
parser.add_argument(
    "--skip_done",
    action="store_true",
    help="Whether to skip already done files in the output directory. Defaults to False.",
)
parser.add_argument(
    "--parallel_workers",
    type=int,
    default=1,
    help="The number of parallel workers. Defaults to 1.",
)
args = parser.parse_args()
if not args.verbose:
    logger.disable("")
    
files = parse_input_files(args.input, include=".vtk")
logger.info(files)
mkdir(args.output)

for fname in tqdm(files, disable=(not args.verbose)):
    dataset, reporters, genotype, plant = parse_hstr(fname)
    dataset, plant = int(dataset), int(plant)
    logger.info(f"Running {fname}")

    # Select center
    mesh = pv.read(fname)

    try:
        p = pv.Plotter()
        # pyvista can't modify variables that aren't lists
        selected_points = np.full(mesh.n_points, -1, dtype="int")

        def point_callback(mesh, pt_id):
            index = selected_points.max() + 1
            if selected_points[pt_id] == -1:
                selected_points[pt_id] = index
            p.add_point_labels(
                [
                    mesh.points[pt_id],
                ],
                [index],
                name=f"pt{index}",
            )

        def undo_point():
            index = selected_points.max()
            selected_points[selected_points == index] = -1
            p.remove_actor(f"pt{index}")
            p.update()

        p.add_mesh(
            mesh,
            scalars="value" if args.segmented else [1] * mesh.n_points,
            cmap="glasbey_bw",
            interpolate_before_map=False,
        )
        p.enable_point_picking(
            callback=point_callback,
            show_message=True,
            pickable_window=True,
            use_mesh=True,
        )
        p.add_key_event("u", undo_point)
        p.set_background("white")
        p.show()
        center = mesh.points[selected_points > -1]
        center = np.mean(center, axis=0)
        # logger.info(center)

        mesh = mesh.remove_points(
            np.sum((mesh.points - center) ** 2, axis=1) ** 0.5 > args.threshold
        )[0]

        initial_params = (
            [
                [-1, -1, 0, 0, 0, 0, 0, 0],
                [-0.1, -0.1, 0, 0, 0, 0, 0, 0],
                [-0.01, -0.01, 0, 0, 0, 0, 0, 0],
                [-0, -0, 0, 0, 0, 0, 0, 0],
            ]
            if args.initial_params is None
            else [args.initial_params]
        )

        for init in initial_params:
            para, para_success = mp.fit_paraboloid(
                mesh.points, init=init, return_success=True
            )
            if para_success:
                break

        para_mesh = mp.paraboloid(para, bounds=mesh.bounds)
        para_apex = mp.paraboloid_apex(para)
        com_apex = mesh.center_of_mass()
        para_dir = (para_apex - para_mesh.center) / np.linalg.norm(
            para_apex - para_mesh.center
        )

        # Project the points that are not on the mesh onto it, using the paraboloid as
        # reference
        factor = 999  # generic big number
        para_apex_projected = mesh.ray_trace(
            origin=para_apex, end_point=para_apex - factor * para_dir, first_point=True
        )[
            0
        ]  # para often overshooting
        com_apex_projected = mesh.ray_trace(
            origin=com_apex, end_point=com_apex + factor * para_dir, first_point=True
        )[0]
        if len(para_apex_projected) == 0:
            para_apex_projected = mesh.ray_trace(
                origin=para_apex,
                end_point=para_apex + factor * para_dir,
                first_point=True,
            )[0]
        if len(com_apex_projected) == 0:
            com_apex_projected = mesh.ray_trace(
                origin=com_apex,
                end_point=com_apex - factor * para_dir,
                first_point=True,
            )[0]

        tree = cKDTree(para_mesh.points)
        closest_pts_distance, closest_pts_idx = tree.query(mesh.points)
        rmse = np.mean(closest_pts_distance)

        df = pd.DataFrame(
            {
                "dataset": dataset,
                "genotype": genotype,
                "reporters": reporters,
                "plant": plant,
                "t": np.nan,
                "para_success": para_success,
                "para_apex-0": para_apex_projected[0],
                "para_apex-1": para_apex_projected[1],
                "para_apex-2": para_apex_projected[2],
                "com_apex-0": com_apex_projected[0],
                "com_apex-1": com_apex_projected[1],
                "com_apex-2": com_apex_projected[2],
                "area": mesh.area,
                "p0": para[0],
                "p1": para[1],
                "rmse": rmse,
                "success": True,
            },
            index=[0],
        )
        df.to_csv(
            f"{args.output}/{bname(fname)}{args.suffix}.csv", index=False, sep="\t"
        )
    except:
        logger.info(f"File {bname(fname)} failed.")


p = Pool(args.parallel_workers)
p.map(run, files)
p.close()
p.join()
